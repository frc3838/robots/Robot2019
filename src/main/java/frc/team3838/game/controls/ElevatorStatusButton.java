package frc.team3838.game.controls;

import javax.annotation.Nullable;

import edu.wpi.first.wpilibj.buttons.Button;
import frc.team3838.game.subsystems.ElevatorSubsystem;
import frc.team3838.game.subsystems.ElevatorSubsystem.Position;
import frc.team3838.game.subsystems.ElevatorSubsystem.RelativePosition;



public class ElevatorStatusButton extends Button
{
    private static final Position activationLevel = Position.MiddleHatch;
    /**
     * Returns whether or not the trigger is active.
     *
     * <p>This method will be called repeatedly a command is linked to the Trigger.
     *
     * @return whether or not the trigger condition is active.
     */
    @Override
    public boolean get()
    {
        final ElevatorSubsystem elevatorSubsystem = ElevatorSubsystem.getInstance();
        //noinspection ConstantConditions
        if ((elevatorSubsystem == null) || !elevatorSubsystem.isEnabled())
        {
            return false;
        }
        else
        {
            @Nullable
            final Position currentPosition = elevatorSubsystem.getPosition();
            if (currentPosition == null)
            {
                return false;
            }
            return (currentPosition == activationLevel) ||
                   (currentPosition.relativePositionTo(activationLevel) == RelativePosition.AboveTarget) ||
                   (currentPosition.relativePositionTo(activationLevel) == RelativePosition.AtTarget)
                ;
        }
    }
}
