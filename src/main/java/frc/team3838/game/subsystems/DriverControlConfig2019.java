package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.controls.AxisPair;
import frc.team3838.core.controls.NoOpAxisPair;
import frc.team3838.core.controls.NoOpTrigger;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.core.subsystems.drive.DriveTrainControlConfigImpl;



public class DriverControlConfig2019 extends DriveTrainControlConfigImpl
{

    private static DriverControlConfig2019 noOpInstance;

    public DriverControlConfig2019(@Nonnull String configurationName,
                                   @Nonnull DriveControlMode driveControlMode,
                                   @Nonnull AxisPair axisPair,
                                   @Nullable Trigger reverseModeTrigger,
                                   @Nullable Trigger fineControlTrigger)
    {
        super(configurationName, driveControlMode, axisPair, reverseModeTrigger, fineControlTrigger);
    }

    @API
    public static DriverControlConfig2019 getDriverControlConfig2017NoOpInstance()
    {
        if (noOpInstance == null)
        {
            noOpInstance = new DriverControlConfig2019("No Op DriverControlConfig2019",
                                                       DriveControlMode.Arcade,
                                                       new NoOpAxisPair(),
                                                       new NoOpTrigger(),
                                                       new NoOpTrigger()
            );
        }
        return noOpInstance;
    }
    

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("configurationName", getConfigurationName())
            .append("driveControlMode", getDriveControlMode())
            .append("axisPair", getAxisPair())
            .append("reverseModeTrigger", getReverseModeTrigger())
            .append("fineControlTrigger", getFineControlTrigger())
            .toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if ((o == null) || (!getClass().equals(o.getClass()))) return false;

        DriverControlConfig2019 other = (DriverControlConfig2019) o;

        return new EqualsBuilder()
            .appendSuper(super.equals(o))
            .isEquals();
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
            .appendSuper(super.hashCode())
            .toHashCode();
    }
}
