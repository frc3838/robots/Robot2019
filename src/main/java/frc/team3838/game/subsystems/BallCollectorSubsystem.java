package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.BaseMotorController;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.RobotProperties;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.game.RobotMap.CANs;
import frc.team3838.game.RobotMap.DIOs;
import frc.team3838.game.commands.ball.MonitorBallStatusCommand;



@SuppressWarnings("Duplicates")
public class BallCollectorSubsystem extends Abstract3838Subsystem
{

    private BaseMotorController motorController;

    private DigitalInput ballSensorDigitalInput;

    private static final double intakeFullSpeed = 0.60;
    private static final double intakePartialSpeed = 0.10;
    private static final double ejectSpeed = 1.0;
    private boolean isEjectingBall = false;

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private BallCollectorSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();


        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @SuppressWarnings("RedundantThrows")

    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem
        if (RobotProperties.getRobotName().toLowerCase().contains("practice"))
        {
            getLogger().debug("Practice robot detected. Initializing ball motor controller as a TalonSRX");
            motorController = new WPI_TalonSRX(CANs.BALL_COLLECTOR_MOTOR);
        }
        else
        {
            getLogger().debug("Competition robot detected. Initializing ball motor controller as a VictorSPX");
            motorController = new WPI_VictorSPX(CANs.BALL_COLLECTOR_MOTOR);
        }
        ballSensorDigitalInput = new DigitalInput(DIOs.BALL_SENSOR);
    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here by calling
        // setDefaultCommand()with the default command.
        // A default command runs without any driver action
    }


    public boolean haveBall()
    {
        return !ballSensorDigitalInput.get();
    }

    public void startMotorIntake()
    {
        isEjectingBall = false;
        motorController.set(ControlMode.PercentOutput, intakeFullSpeed);
    }
    public void startMotorIntakePartialSpeed()
    {
        isEjectingBall = false;
        motorController.set(ControlMode.PercentOutput, intakePartialSpeed);
    }

    public void startMotorEject()
    {
        isEjectingBall = true;
        motorController.set(ControlMode.PercentOutput, -ejectSpeed);
    }


    public void stopMotor()
    {
        isEjectingBall = false;
        motorController.set(ControlMode.PercentOutput, 0.0);
    }


    @Override
    protected void initDefaultCommand()
    {
        if (isEnabled())
        {
            setDefaultCommand(new MonitorBallStatusCommand());
        }
    }


    public boolean isEjectingBall()
    {
        return isEjectingBall;
    }


    public void setEjectingBall(boolean ejectingBall)
    {
        isEjectingBall = ejectingBall;
    }


    public void updateStatus()
    {
        SmartDashboard.putBoolean("Have Ball", haveBall());
        SmartDashboard.putBoolean("Ejecting", isEjectingBall);
    }



    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final BallCollectorSubsystem singleton = new BallCollectorSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static BallCollectorSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

