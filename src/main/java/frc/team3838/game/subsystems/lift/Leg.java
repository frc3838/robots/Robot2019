package frc.team3838.game.subsystems.lift;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.BaseMotorController;

import edu.wpi.first.wpilibj.Counter;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.RobotMap.DIOs;

import static frc.team3838.game.subsystems.lift.LegLocation.*;



public class Leg<C extends BaseMotorController>
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(Leg.class);

    /* TODO - need to determine what a good default speed is for the raising and lowering of the legs*/
    @API
    public static final double EXTENDING_DEFAULT_SPEED = 0.7;
    @API
    public static final double RETRACTING_DEFAULT_SPEED = 0.7;
    @API
    public static final double EXTENDING_MAX_SPEED = 1.0;
    @API
    public static final double RETRACTING_MAX_SPEED = 1.0;
    @API
    public static final double EXTENDING_MIN_SPEED = 0.2;
    @API
    public static final double RETRACTING_MIN_SPEED = 0.2;
    @API
    public static final double INCHES_PER_ROTATION = 1.0 / 11.0;

    private static final ControlMode controlMode = ControlMode.PercentOutput;
    private final C motorController;
    private final LegLocation location;
    private final Counter rotationCounter;
    private LegMovementDirection legMovementState = LegMovementDirection.Stopped;


    Leg(C motorController, LegLocation location)
    {
        this.motorController = motorController;
        this.location = location;
        motorController.set(controlMode, 0.0);
        motorController.setNeutralMode(NeutralMode.Brake);

        switch(location)
        {
            case FrontLeft:
                rotationCounter = initCounter(DIOs.LEG_FRONT_LEFT_ROTATION_SENSOR, FrontLeft);
                break;

            case FrontRight:
                rotationCounter = initCounter(DIOs.LEG_FRONT_RIGHT_ROTATION_SENSOR, FrontRight);
                break;

            case RearLeft:
                rotationCounter = initCounter(DIOs.LEG_REAR_LEFT_ROTATION_SENSOR, RearLeft);
                break;

            case RearRight:
                rotationCounter = initCounter(DIOs.LEG_REAR_RIGHT_ROTATION_SENSOR, RearRight);
                break;

            default:
                throw new IllegalStateException("Unknown leg location of '" + location + "'. Cannot initialize Leg object");
        }
    }


    /**
     * Returns the default absolute speed for the specified Leg Movement direction.
     *
     * @param legMovementDirection the direction to get the default speed for
     *
     * @return the default absolute speed for the specified Leg Movement direction
     */
    @SuppressWarnings("Duplicates")
    @API
    public static double getDefaultSpeed(LegMovementDirection legMovementDirection)
    {
        switch (legMovementDirection)
        {
            case Extending:
                return EXTENDING_DEFAULT_SPEED;
            case Retracting:
                return RETRACTING_DEFAULT_SPEED;
            case Stopped:
            default:
               return  0.0;
        }
    }

    /**
     * Returns the maximum absolute speed for the specified Leg Movement direction.
     *
     * @param legMovementDirection the direction to get the maximum speed for
     *
     * @return the maximum absolute speed for the specified Leg Movement direction
     */
    @SuppressWarnings("Duplicates")
    @API
    public static double getMaxSpeed(LegMovementDirection legMovementDirection)
    {
        switch (legMovementDirection)
        {
            case Extending:
                return EXTENDING_MAX_SPEED;
            case Retracting:
                return RETRACTING_MAX_SPEED;
            case Stopped:
            default:
               return  0.0;
        }
    }
    /**
     * Returns the minimum absolute speed for the specified Leg Movement direction (to prevent stalling).
     *
     * @param legMovementDirection the direction to get the minimum speed for
     *
     * @return the minimum absolute speed for the specified Leg Movement direction
     */
    @SuppressWarnings("Duplicates")
    @API
    public static double getMinSpeed(LegMovementDirection legMovementDirection)
    {
        switch (legMovementDirection)
        {
            case Extending:
                return EXTENDING_MIN_SPEED;
            case Retracting:
                return RETRACTING_MIN_SPEED;
            case Stopped:
            default:
               return  0.0;
        }
    }

    private static Counter initCounter(int channel, LegLocation legLocation)
    {
        // first.wpi.edu/FRC/roborio/release/docs/java/edu/wpi/first/wpilibj/Counter.html
        final Counter counter = new Counter(channel);
        counter.setDistancePerPulse(INCHES_PER_ROTATION);
        counter.setMaxPeriod(0.1 /*seconds*/); // It's not clear what the default value is. Note that the Ultrasonic class sets it to 1.0
        counter.setSemiPeriodMode(false);
        // counter.setReverseDirection(true); - do we need this since we only hav an up channel?
        // counter.setSamplesToAverage(64);
        counter.setName(legLocation.name() + "Leg");
        counter.reset();

        return counter;
    }



    // TODO: We want to configure the inversion/direction so a positive value is lowering the legs

    @API
    public void startExtendingLeg() { moveLeg(LegMovementDirection.Extending, EXTENDING_DEFAULT_SPEED); }
    @API
    public void startExtendingLeg(double absoluteSpeed)
    {
        final double normalizedSpeed = MathUtils.constrainBetweenZeroAndOne(MathUtils.makePositive(absoluteSpeed));
        logger.debug("startExtendingLeg() at speed {}", normalizedSpeed);
        legMovementState = LegMovementDirection.Extending;
        motorController.set(controlMode, normalizedSpeed);
    }
    @API
    public void startRetractingLeg() { moveLeg(LegMovementDirection.Retracting, RETRACTING_DEFAULT_SPEED); }
    @API
    public void startRetractingLeg(double absoluteSpeed)
    {
        final double normalizedSpeed = MathUtils.constrainBetweenNegOneAndZero(MathUtils.makeNegative(absoluteSpeed));
        logger.debug("startRetractingLeg() at speed {}", normalizedSpeed);
        legMovementState = LegMovementDirection.Retracting;
        motorController.set(controlMode, Math.abs(normalizedSpeed));
    }

    @API
    public void moveLeg(LegMovementDirection legMovement)
    {
        switch (legMovement)
        {
            case Extending:
                moveLeg(legMovement, EXTENDING_DEFAULT_SPEED);
                break;
            case Retracting:
                moveLeg(legMovement, RETRACTING_DEFAULT_SPEED);
                break;
            case Stopped:
                stopLeg();
        }
    }

    @API
    public void moveLeg(LegMovementDirection legMovementDirection, double absoluteSpeed)
    {
        switch (legMovementDirection)
        {
            case Extending:
                motorController.set(controlMode, MathUtils.makePositive(absoluteSpeed));
                legMovementState = LegMovementDirection.Extending;
                break;
            case Retracting:
                motorController.set(controlMode, MathUtils.makeNegative(absoluteSpeed));
                legMovementState = LegMovementDirection.Retracting;
                break;
            case Stopped:
                stopLeg();
        }
    }

    @API
    public void stopLeg()
    {
        motorController.set(controlMode, 0.0);
        legMovementState = LegMovementDirection.Stopped;
        logger.debug("stopLeg() called");
    }


    @API
    public C getMotorController() { return motorController; }


    @API
    public LegLocation getLocation() { return location; }


//    @API
//    public Counter getRotationCounter() { return rotationCounter; }


    @API
    public void resetRotationCount() { rotationCounter.reset(); }


    @API
    public double getLegExtensionDistance() { return rotationCounter.getDistance(); }


    @API
    public LegMovementDirection getLegMovementState() { return legMovementState; }


    @Override
    public String toString()
    {
        return location + "Leg";
    }
}
