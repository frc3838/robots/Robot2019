package frc.team3838.game.subsystems.lift;

import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.RobotMap.CANs;



public class LiftSubsystem extends Abstract3838Subsystem
{
    private Leg<WPI_TalonSRX> frontLeftLeg;
    private Leg<WPI_TalonSRX> frontRightLeg;
    private Leg<WPI_TalonSRX> rearLeftLeg;
    private Leg<WPI_TalonSRX> rearRightLeg;

    private ImmutableMap<LegLocation, Leg<?>> legs;


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private LiftSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem


        // TODO: Need to find out what actual controller types are being used.
        frontLeftLeg =  new Leg<>(new WPI_TalonSRX(CANs.LEG_MOTOR_LEFT_FRONT), LegLocation.FrontLeft);
        frontRightLeg = new Leg<>(new WPI_TalonSRX(CANs.LEG_MOTOR_RIGHT_FRONT), LegLocation.FrontRight);
        rearRightLeg =  new Leg<>(new WPI_TalonSRX(CANs.LEG_MOTOR_RIGHT_REAR), LegLocation.RearRight);
        rearLeftLeg =   new Leg<>(new WPI_TalonSRX(CANs.LEG_MOTOR_LEFT_REAR), LegLocation.RearLeft);

        legs = ImmutableMap.of(frontLeftLeg.getLocation(), frontLeftLeg,
                               frontRightLeg.getLocation(), frontRightLeg,
                               rearLeftLeg.getLocation(), rearLeftLeg,
                               rearRightLeg.getLocation(), rearRightLeg);

        // TODO: We want to configure the inversion/direction so a positive value is lowering the legs
        frontLeftLeg.getMotorController().setInverted(false);
        frontRightLeg.getMotorController().setInverted(false);
        rearLeftLeg.getMotorController().setInverted(false);
        rearRightLeg.getMotorController().setInverted(false);

    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // We may have to instead move the polling out to a separate scheduledExecutorService.scheduleAtFixedRate
        setDefaultCommand(new Abstract3838Command() {
            // @formatter:off
            @Nonnull
            @Override protected Set<I3838Subsystem> getRequiredSubsystems() { return ImmutableSet.of(getInstance()); }
            @Override protected void initializeImpl() throws Exception { pollLegRotationSensors(); }
            @Override protected void executeImpl() throws Exception { pollLegRotationSensors(); }
            @Override protected boolean isFinishedImpl() throws Exception { return false; }
            @Override protected void endImpl() throws Exception { /* No op */ }
            // @formatter:on
        });
    }


    private void pollLegRotationSensors()
    {
        for (Entry<LegLocation, Leg<?>> entry : legs.entrySet())
        {
            final double legExtensionDistance = entry.getValue().getLegExtensionDistance();
            SmartDashboard.putString( entry.getKey() + " Leg (inches)", MathUtils.formatNumber(legExtensionDistance, 3));
        }
    }

    @API
    public ImmutableMap<LegLocation, Leg<?>> getLegs() { return legs; }

    @API
    public Leg<?> getLeg(LegLocation legLocation) { return legs.get(legLocation); }



    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final LiftSubsystem singleton = new LiftSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static LiftSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}
