package frc.team3838.game.subsystems.lift;

public enum LegLocation
{ FrontLeft, FrontRight, RearLeft, RearRight }
