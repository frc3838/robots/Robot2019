package frc.team3838.game.subsystems.lift;

public enum LegMovementDirection 
{
    // We use extending and retracing rather than lowering and raising to prevent
    // confusion since lowering the leg raises the robot and visa-versa. 
    
    Extending,
    Retracting,
    Stopped
}
