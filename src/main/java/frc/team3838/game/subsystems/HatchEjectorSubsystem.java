package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.team3838.core.subsystems.Abstract3838CompressorSubsystem;
import frc.team3838.game.RobotMap;



public class HatchEjectorSubsystem extends Abstract3838CompressorSubsystem
{

    /*
        TODO:
        Jocelyn
            From my understanding, the Hatch game piece will be put onto the cargo ships
            and rockets via an pneumatic controlled system. It will have 3 Solenoids (i.e.
            the electronically controlled valves that actuate the Pneumatic Cylinders).
            There will be an in (that we'll call 'Retracted') and out state (that we'll
            call 'Extended').

            Take a look at the class level Javadoc of the 'Abstract3838CompressorSubsystem'
            for some more information on creating the DoubleSolenoid objects you will need.
            We need to wait to find out what PWM channels the Solenoids will be on. For now
            assume 1 through 6. (Each DoubleSolenoid has a forwardChannel and a ReverseChannel)
            You can put PWM values those in the RobotMap.PWM class. Please add a  T O D O  comment
            to verify them with Scott.

            In addition to implementing this Subsystem class, we'll need (to start with)
            a 'HatchEjectorExtendCommand' and 'HatchEjectorRetractCommand'. I created
            skeletons for them (since I have a custom new class template for doing such)
            in the package frc.team3838.game.commands.hatchEjector

            For an example, you can look at the following:
                Robot2018's ArmAndClawSubsystem
                    https://gitlab.com/FRC3838/Robot2018/blob/master/src/frc/team3838/game/subsystems/ArmAndClawSubsystem.java
                That's the robot we were using last week to test the drivetrain. It has a big claw that is controlled by
                Pneumatic Cylinders. It had one cylinder that raised and lowered the arm. And another one that opened and
                closed the claw.
                It also had motors that were used to "suck" in and to add in ejecting the cube we had
                to pickup and move. This year there are no motors.
                It also had a 'servo' that was used to set high positions. Again nothing like that this year.
                Then take a look at the commands that were used in the package
                    https://gitlab.com/FRC3838/Robot2018/tree/master/src/frc/team3838/game/commands/arm
                You will mostly be interested in the OpenClawCommand and the CloseClawCommand. The
                Extend and Retract commands for this subsystem will be similar.

              Let me know if you have any questions.
     */

    private static final Logger logger = LoggerFactory.getLogger(HatchEjectorSubsystem.class);

    private static final DoubleSolenoid.Value extendValue = Value.kForward;
    private static final DoubleSolenoid.Value retractValue = Value.kReverse;

    private DoubleSolenoid retractExtendSolenoid;


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private HatchEjectorSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @SuppressWarnings({"RedundantThrows"})
    @Override
    protected void initSubsystem() throws Exception
    {
        super.initSubsystem(); // The Compressor is initialized in the super class.

        //TODO: Implement this method. Initialize any Solenoid or DoubleSolenoid fields here.
        //      Most often DoubleSolenoid objects are used.
        //      Also init any limit switches or other sensors here
        //      See the Javadoc of the super class for information on creating and using Solenoid and DoubleSolenoid objects

        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem
        // This method implementation must call super.initSubsystem() as its first statement.

        retractExtendSolenoid= new DoubleSolenoid(getPcmCanId(), RobotMap.PCM.HATCH_EJECTOR_SolenoidForwardChannel, RobotMap.PCM.HATCH_EJECTOR_SolenoidReverseChannel);
    }


    /**
     * The CAN ID the Pneumatic Control Module (PCM) -- i.e. the compressor --  compressor is
     * assigned via the Cross the Road Electronics (CTRE) 'Phoenix' tool.
     * That CAN ID should be set to a constant in the RobotMap (so that all CAN IDs can be
     * referenced and managed in a single location). This method then simply returns that constant.
     *
     * @return CAN ID the PCM is assigned
     */
    @Override
    public int getPcmCanId()
    {
        return RobotMap.CANs.PCM;
    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for CompressorSubsystems
        // Other than perhaps a sensor reading being updated on the
        // Smartdashboard or such.
    }

    public void retractArm()
    {
        retractExtendSolenoid.set(retractValue);
    }

    public void extendArm()
    {
        retractExtendSolenoid.set(extendValue);
    }


    /**
     * Turns off the solenoid (used mostly for testing).
     */
    public void solenoidOff()
    {
        retractExtendSolenoid.set(Value.kOff);
    }










    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final HatchEjectorSubsystem singleton = new HatchEjectorSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static HatchEjectorSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

