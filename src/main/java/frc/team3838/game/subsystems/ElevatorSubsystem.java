package frc.team3838.game.subsystems;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.jetbrains.annotations.Contract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.PWMSpeedController;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.dashboard.DashboardManager;
import frc.team3838.core.hardware.LimitSwitch;
import frc.team3838.core.logging.PeriodicLogger;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.RobotMap.DIOs;
import frc.team3838.game.RobotMap.PWMs;
import frc.team3838.game.commands.elevator.FullyResetElevatorCommand;



public class ElevatorSubsystem extends Abstract3838Subsystem
{
    private static final Logger logger = LoggerFactory.getLogger(ElevatorSubsystem.class);

    public static final double MAX_RAISE_SPEED = -0.70;
    private static final double HOLD_IN_PLACE_SPEED = -0.17;
    public static final double MAX_LOWER_SPEED = 0.30;

    private PeriodicLogger countLogger = new PeriodicLogger(logger, new TimeSetting(500, TimeUnit.MILLISECONDS));
    private PeriodicLogger speedLogger = new PeriodicLogger(logger, new TimeSetting(1, TimeUnit.SECONDS));
    private PeriodicLogger badPositionStatePeriodicLogger = new PeriodicLogger(logger, new TimeSetting(5, TimeUnit.SECONDS));
    private Counter counter;
    private PWMSpeedController speedController;
    private LimitSwitch bottomLimitSwitch = LimitSwitch.createInverted(DIOs.ELEVATOR_BOTTOM_LIMIT_SWITCH);
    private boolean isElevatorResetInProgress = false;
    private DigitalInput upDio;
    private DigitalInput downDio;

//    private MotorOps motorOps;

    // UP IS NEGATIVE VALUE
    // 10-12% holds
    // 50-60% up is max speed
    // 15% down works well, might be able to go to 20%


    public enum Position
    {
        LowerHatch(0,1),
        HatchLoadLevel(2, 3),
        CargoLoadLevel(4, 5),
        MiddleHatch(6, 7),
        IgnoredLevel(8, 9),
        UpperHatch(10, 11);

        private final int firstCountPosition;
        private final int secondCountPosition;

        private static Map<Integer, Position> map;
        private static ImmutableList<Position> list;
        Position(int firstCountPosition, int secondCountPosition)
        {
            this.firstCountPosition = firstCountPosition;
            this.secondCountPosition = secondCountPosition;
        }



        public int getFirstCountPosition()
        {
            return firstCountPosition;
        }


        public int getSecondCountPosition()
        {
            return secondCountPosition;
        }


        public RelativePosition relativePositionTo(@Nonnull Position targetPosition)
        {
            if (targetPosition.firstCountPosition == this.firstCountPosition)
            {
                return RelativePosition.AtTarget;
            }
            else if (targetPosition.firstCountPosition > this.firstCountPosition)
            {
                return RelativePosition.BelowTarget;
            }
            else
            {
                return RelativePosition.AboveTarget;
            }
        }


        @Nullable
        @Contract("null -> null")
        public static Position nextPositionDown(@Nullable Position currentPosition)
        {
            try
            {
                if (currentPosition == null) {return null; }
                final Position nextPosition = (currentPosition == Iterables.getFirst(list, null)) ? currentPosition : list.get(list.indexOf(currentPosition) - 1);
                return ((nextPosition == Position.IgnoredLevel) && (currentPosition != IgnoredLevel)) ? nextPositionDown(nextPosition) : nextPosition;

            }
            catch (Exception e)
            {
                logger.warn("An exception occurred when calculating nextPositionDown for current position of '{}'. Cause Summary: {}", currentPosition, e.toString(), e);
                return currentPosition;
            }
        }

        @Nullable
        @Contract("null -> null")
        public static Position nextPositionUp(@Nullable Position currentPosition)
        {
            try
            {
                if (currentPosition == null) {return null; }
                final Position nextPosition = (currentPosition == Iterables.getLast(list)) ? currentPosition : list.get(list.indexOf(currentPosition) + 1);
                return ((nextPosition == Position.IgnoredLevel) && (currentPosition != IgnoredLevel)) ? nextPositionUp(nextPosition) : nextPosition;
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred when calculating nextPositionUp for current position of '{}'. Cause Summary: {}", currentPosition, e.toString(), e);
                return currentPosition;
            }
        }


        static
        {
            final Builder<Integer, Position> builder = ImmutableMap.builder();

            for (Position position : Position.values())
            {
                builder.put(position.firstCountPosition, position);
                builder.put(position.secondCountPosition, position);
            }
            map = builder.build();

            list = ImmutableList.copyOf(values());
        }
    }

    public enum RelativePosition
    {
        AboveTarget, BelowTarget, AtTarget
    }

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private ElevatorSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem
        upDio = new DigitalInput(DIOs.ELEVATOR_ENCODER_UP);
        upDio.setName(getSubsystem(), "Elevator_DI_up [4]");
        downDio = new DigitalInput(DIOs.ELEVATOR_ENCODER_DOWN);
        upDio.setName(getSubsystem(), "Elevator_DI_down [5]");
        counter = new Counter(EncodingType.k2X, upDio, downDio, true );
        counter.setName(ElevatorSubsystem.getInstance().getSubsystem(), "ElevatorCounter");
        speedController = new VictorSP(PWMs.ELEVATOR_MOTOR);
        // If you need to invert, only invert the controller, not the motor ops
        speedController.setInverted(false);
//        motorOps = new MotorOps(speedController, false);

        try
        {
            updateStatus();
        }
        catch (Exception ignore) {}
    }


    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for most subsystems
        setDefaultCommand(new Abstract3838Command()
        {
            // @formatter:off
            @Nonnull
            @Override protected Set<I3838Subsystem> getRequiredSubsystems() { return ImmutableSet.of(getInstance()); }
            @Override protected void initializeImpl() throws Exception { updateStatus(); }
            @Override protected void executeImpl() throws Exception { updateStatus(); }
            @Override protected boolean isFinishedImpl() throws Exception { return false; }
            @Override protected void endImpl() throws Exception { /* No op */ }
            // @formatter:on
        });
    }


    @API
    public int getCount()
    {
        // may be null temporarily during robot initialization
        return (counter == null) ? 0 : counter.get();
    }

    @API
    @Nullable
    public Position getPosition()
    {
        final int count = getCount();
        final Position position = Position.map.get(count);
        if (position == null)
        {
            badPositionStatePeriodicLogger.warn("Could not determine position for count of {}. This is likely do robot initializing in a bad state (i.e. the elevator not all the way down).", count);
        }

        return position;
    }

    @API
    public RelativePosition getRelativePosition(Position targetPosition)
    {
        final Position currentPosition = getPosition();
        if ((targetPosition == null) || (currentPosition == null))
        {
            logger.warn("Cannot calculate relativePosition due to a null value. targetPosition = {}  currentPosition = {}", targetPosition, currentPosition);
            return RelativePosition.AtTarget;
        }
        else
        {
            return currentPosition.relativePositionTo(targetPosition);
        }
    }

    @API
    public void updateStatus()
    {
        SmartDashboard.putBoolean("Elev Full Down", bottomLimitSwitch.isActivated());
//        SmartDashboard.putBoolean("Up DIO", upDio.get());
//        SmartDashboard.putBoolean("Down DIO", downDio.get());
        DashboardManager.elevatorUpDioEntry.setBoolean(upDio.get());
        DashboardManager.elevatorDownDioEntry.setBoolean(downDio.get());
        final int count = getCount();
        if (bottomLimitSwitch.isActivated())
        {
            resetCounter();
        }

        final String countString = Integer.toString(count);
        SmartDashboard.putString("Elevator Count", countString);
        DashboardManager.elevatorCountEntry.setString(countString);
        countLogger.debug("Elevator Count: {}", countString);
        if (((count < 0) || (count > 12)) && !isElevatorResetInProgress)
        {
            logger.warn("elevator count is out of range at {}. Automatically resetting the elevator.", count);
            final FullyResetElevatorCommand resetElevatorCommand = new FullyResetElevatorCommand();
            resetElevatorCommand.start();
        }
    }

    @API
    public void lowerElevator()
    {
        lowerElevator(MAX_LOWER_SPEED);
    }

    @API
    public void lowerElevator(double absoluteSpeed)
    {
        final double speed = MathUtils.constrainBetweenZeroAndOne(MathUtils.makePositive(absoluteSpeed));
        speedLogger.debug("lowerElevator() setting speed to {} given absoluteSpeed of {}", speed, absoluteSpeed);
        speedController.set(speed);
    }

    @API
    public void raiseElevator()
    {
        raiseElevator(MAX_RAISE_SPEED);
    }

    @API
    public void raiseElevator(double absoluteSpeed)
    {
        if (!isElevatorResetInProgress)
        {
            final double speed = MathUtils.constrainBetweenNegOneAndZero(MathUtils.makeNegative(absoluteSpeed));
            speedLogger.debug("raiseElevator() setting speed to {} given absoluteSpeed of {}", speed, absoluteSpeed);
            speedController.set(speed);
        }
    }

    @API
    public void moveElevator(double directionalSpeed)
    {
        speedLogger.debug("moveElevator setting speed: {}", directionalSpeed);
        speedController.set(MathUtils.constrainBetweenNegOneAndOne(directionalSpeed));
    }

    @API
    public void holdElevator()
    {
        if (getPosition() == Position.map.get(0))
        {
            stopElevator();
        }
        else
        {
            raiseElevator(HOLD_IN_PLACE_SPEED);
        }
    }

    @API
    public void stopElevator()
    {
        speedController.stopMotor();
    }

    @API
    public void initializeReset()
    {
        isElevatorResetInProgress = true;
    }
    @API
    public void resetCounter()
    {
        counter.reset();
        isElevatorResetInProgress = false;
    }

    @API
    public boolean isFullyDown() { return bottomLimitSwitch.isActivated(); }


    public boolean isElevatorResetInProgress() { return isElevatorResetInProgress; }









    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final ElevatorSubsystem singleton = new ElevatorSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static ElevatorSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

