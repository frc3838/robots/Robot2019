package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import frc.team3838.core.RobotProperties;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;



public class NavxSubsystem extends Abstract3838NavxSubsystem
{

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private NavxSubsystem()
    {
        //The super constructor checks if the subsystem is enabled. If so, it calls initSubsystem();
        super(RobotProperties.getNavxAdjustmentAngle());
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if, and only if, the subsystem is enabled
        // The super constructor will safely handle this init method throwing an exception by disabling the subsystem

    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        /* Initialize the default command, if any, here.    */
        /* A default command runs without any driver action. */
    }


    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final NavxSubsystem singleton = new NavxSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static NavxSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

