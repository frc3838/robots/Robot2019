package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import frc.team3838.core.subsystems.Abstract3838TwoUsbCameraSubsystem;
import frc.team3838.core.subsystems.CameraConfig;
import frc.team3838.game.commands.camera.SwitchCameraCommand;



public class UsbCameraSubsystem extends Abstract3838TwoUsbCameraSubsystem
{

    private UsbCameraSubsystem()
    {
        super( new CameraConfig(0, "Front_Camera_0"), new CameraConfig(1, "Rear_Camera_1"));
    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        setDefaultCommand(new SwitchCameraCommand());
    }
















    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final UsbCameraSubsystem singleton = new UsbCameraSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static UsbCameraSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */

}
