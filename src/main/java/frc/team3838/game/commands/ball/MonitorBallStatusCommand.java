package frc.team3838.game.commands.ball;

import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.subsystems.BallCollectorSubsystem;



public class MonitorBallStatusCommand extends Abstract3838Command
{

    private final BallCollectorSubsystem ballCollectorSubsystem = BallCollectorSubsystem.getInstance();


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return ImmutableSet.of(BallCollectorSubsystem.getInstance());
    }


    @Override
    protected void initializeImpl() throws Exception
    {
        ballCollectorSubsystem.updateStatus();
    }


    @Override
    protected void executeImpl() throws Exception
    {
        ballCollectorSubsystem.updateStatus();
    }


    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return false;
    }


    @Override
    protected void endImpl() throws Exception
    {

    }
}
