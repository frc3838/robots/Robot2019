package frc.team3838.game.commands;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.dashboard.DashboardManager;
import frc.team3838.core.logging.PeriodicLogger;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.commands.ball.EjectBallCommand;
import frc.team3838.game.commands.ball.EjectBallWithAutoShutoffCommand;
import frc.team3838.game.commands.ball.IntakeBallCommand;
import frc.team3838.game.commands.ball.IntakeBallWithAutoShutoffCommand;
import frc.team3838.game.commands.ball.StopBallCollectorCommand;
import frc.team3838.game.commands.elevator.FullyResetElevatorCommand;
import frc.team3838.game.commands.elevator.HoldElevatorCommand;
import frc.team3838.game.commands.elevator.MoveElevatorDownOnePositionCommand;
import frc.team3838.game.commands.elevator.MoveElevatorToPositionCommand;
import frc.team3838.game.commands.elevator.MoveElevatorUpOnePositionCommand;
import frc.team3838.game.commands.elevator.StopElevatorCommand;
import frc.team3838.game.commands.hatchEjector.EjectHatchCommandGroup;
import frc.team3838.game.commands.hatchEjector.HatchEjectorExtendCommand;
import frc.team3838.game.commands.hatchEjector.HatchEjectorRetractCommand;
import frc.team3838.game.commands.hatchEjector.HatchEjectorSolenoidOffCommand;
import frc.team3838.game.commands.lift.StartExtendingAllLegsAtDashboardSpeed;
import frc.team3838.game.commands.lift.StartExtendingLegAtDashboardSpeedCommand;
import frc.team3838.game.commands.lift.StartRetractingAllLegsAtDashboardSpeed;
import frc.team3838.game.commands.lift.StartRetractingLegAtDashboardSpeedCommand;
import frc.team3838.game.commands.lift.StopAllLegsCommand;
import frc.team3838.game.commands.lift.StopLegCommand;
import frc.team3838.game.subsystems.ElevatorSubsystem;
import frc.team3838.game.subsystems.ElevatorSubsystem.Position;
import frc.team3838.game.subsystems.lift.LegLocation;



public class CommandDashboardButtons
{
    private static final Logger logger = LoggerFactory.getLogger(CommandDashboardButtons.class);

    /*
        EXAMPLE FROM https://wpilib.screenstepslive.com/s/currentCS/m/shuffleboard/l/1021980-organizing-widgets
        ShuffleboardLayout elevatorCommands = Shuffleboard.getTab("Commands")
          .getLayout("Elevator", BuiltInLayouts.kList)
          .withSize(2, 2)
          .withProperties(Map.of("Label position", "HIDDEN")); // hide labels for commands
        elevatorCommands.add(new ElevatorDownCommand());
        elevatorCommands.add(new ElevatorUpCommand());

     */


    public static void addAllCommands()
    {
        addElevatorCommands();
        addLegCommands();
        addHatchEjectorCommands();
        addBallCollectorCommands();
    }


    private static void addElevatorCommands()
    {
        PeriodicLogger periodicLogger = new PeriodicLogger(logger, new TimeSetting(2, TimeUnit.SECONDS));

        final ShuffleboardLayout layout = createLayout("Elevator", 2, 8);

        layout.add(new FullyResetElevatorCommand());
        layout.add(new StopElevatorCommand());
        layout.add(new HoldElevatorCommand());
        layout.add("Up One Level", new MoveElevatorUpOnePositionCommand());
        layout.add("Down One Level", new MoveElevatorDownOnePositionCommand());
        for (Position position : Position.values())
        {
            layout.add(new MoveElevatorToPositionCommand(position));
        }


        final NetworkTableEntry speedEntry =
            DashboardManager.commandsTab.add("Elevator Speed", 0.0)
                                        .withProperties(Map.of("Min", -1.0, "Max", 1.0, "Block increment", 0.05))
                                        .getEntry();

        DashboardManager.elevatorUpDioEntry.setBoolean(false);
        DashboardManager.elevatorUpDioEntry.setBoolean(false);

        layout.add(new Abstract3838Command("Move Elevator") {
            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems()
            {
                return ImmutableSet.of(ElevatorSubsystem.getInstance());
            }


            @Override
            protected void initializeImpl() throws Exception
            {

            }


            @Override
            protected void executeImpl() throws Exception
            {
                final double speed = speedEntry.getDouble(0.0);
                periodicLogger.info("Elevator speed from dashboard: {}", speed);
                ElevatorSubsystem.getInstance().moveElevator(speed);
            }


            @Override
            protected boolean isFinishedImpl() throws Exception
            {
                return false;
            }


            @Override
            protected void endImpl() throws Exception
            {

            }
        });


        // RESET COUNTER COMMAND

        layout.add(new Abstract3838Command("Reset Counter")
        {
            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems()
            {
                return ImmutableSet.of(ElevatorSubsystem.getInstance());
            }


            @Override
            protected void initializeImpl() throws Exception
            {
                ElevatorSubsystem.getInstance().resetCounter();
            }


            @Override
            protected void executeImpl() throws Exception
            {

            }


            @Override
            protected boolean isFinishedImpl() throws Exception
            {
                return false;
            }


            @Override
            protected void endImpl() throws Exception
            {

            }
        });
    }


    private static void addLegCommands()
    {
        final ShuffleboardLayout layout = createLayout("Lift Legs", 3, 8);

        layout.add(new StopAllLegsCommand());

        layout.add(new StartExtendingAllLegsAtDashboardSpeed());

        layout.add(new StartRetractingAllLegsAtDashboardSpeed());

        for (LegLocation legLocation : LegLocation.values())
        {
            layout.add(new StartRetractingLegAtDashboardSpeedCommand(legLocation));
        }

        for (LegLocation legLocation : LegLocation.values())
        {
            layout.add(new StartExtendingLegAtDashboardSpeedCommand(legLocation));
        }

        for (LegLocation legLocation : LegLocation.values())
        {
            layout.add(new StopLegCommand(legLocation));
        }
    }


    private static void addHatchEjectorCommands()
    {
        final ShuffleboardLayout layout = createLayout("Hatch Ejector", 2, 3);

        layout.add(new EjectHatchCommandGroup());
        layout.add(new HatchEjectorExtendCommand());
        layout.add(new HatchEjectorRetractCommand());
        layout.add(new HatchEjectorSolenoidOffCommand());
    }

    private static void addBallCollectorCommands()
    {
        final ShuffleboardLayout layout = createLayout("Ball Collector", 2, 3);

        layout.add(new IntakeBallCommand());
        layout.add(new EjectBallCommand());
        layout.add(new StopBallCollectorCommand());
        layout.add(new IntakeBallWithAutoShutoffCommand());
        layout.add(new EjectBallWithAutoShutoffCommand());
    }

    @Nonnull
    private static ShuffleboardLayout createLayout(String title, int width, int height)
    {
        return DashboardManager.commandsTab.getLayout(title, BuiltInLayouts.kList)
                                           .withSize(width, height)
                                           .withProperties(Map.of("Label position", "HIDDEN"));  // hide labels for commands
    }
}
