package frc.team3838.game.commands.lift;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.commands.Abstract3838CommandGroup;
import frc.team3838.game.subsystems.lift.LegLocation;



public class StopAllLegsCommand extends Abstract3838CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    protected final Logger logger = LoggerFactory.getLogger(getClass());


    public StopAllLegsCommand()
    {
        super(StopAllLegsCommand.class.getSimpleName());
        for (LegLocation legLocation : LegLocation.values())
        {
            logger.debug("Adding {} Leg StopLegCommand to parallel run", legLocation);
            addParallel(new StopLegCommand(legLocation));
        }
    }
}
