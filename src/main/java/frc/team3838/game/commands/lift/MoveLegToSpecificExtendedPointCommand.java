package frc.team3838.game.commands.lift;

import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.subsystems.lift.Leg;
import frc.team3838.game.subsystems.lift.LegLocation;
import frc.team3838.game.subsystems.lift.LegMovementDirection;
import frc.team3838.game.subsystems.lift.LiftSubsystem;


@API
public class MoveLegToSpecificExtendedPointCommand extends Abstract3838Command
{
    private final Leg<?> leg;
    private final double targetExtensionInInches;
    private LegMovementDirection legMovementDirectionNeeded = LegMovementDirection.Stopped; // Default to stopped to prevent NPE, and set in initialize
    
    
    public MoveLegToSpecificExtendedPointCommand(LegLocation legLocation, double targetExtensionInInches)
    {
        super(MoveLegToSpecificExtendedPointCommand.class.getName() + '-' + legLocation + '-' + targetExtensionInInches + "_inches");
        this.targetExtensionInInches = targetExtensionInInches;
        leg = LiftSubsystem.getInstance().getLeg(legLocation);
    }
    
    
    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems() { return ImmutableSet.of(LiftSubsystem.getInstance()); }
    
    
    /**
     * The initialize() method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute()
     * method is called repeatedly until isFinished() returns true,
     * or interrupted() is called by the command scheduler/runner.
     * After isFinished() returns true, end() is called one time
     * in order to do any cleanup (such as stopping a motor) or 
     * to set any post run values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        final double currentExtensionInInches = leg.getLegExtensionDistance();
        if (isAtTarget())
        {
            legMovementDirectionNeeded = LegMovementDirection.Stopped;
        }
        else if (currentExtensionInInches < targetExtensionInInches)
        {
            legMovementDirectionNeeded = LegMovementDirection.Extending;
        }
        else 
        {
            legMovementDirectionNeeded = LegMovementDirection.Retracting;
        }
        logger.info("    {} with a TARGET extension of    {} inches", getName(), targetExtensionInInches);
        logger.info("    {} with an starting extension of {} inches", getName(), currentExtensionInInches);
        logger.info("    {} with an needed direction of   {} ", getName(), legMovementDirectionNeeded);
    }
    
    
    private boolean isAtTarget()
    {
        return MathUtils.isNear(leg.getLegExtensionDistance(), targetExtensionInInches, 0.05 );
    }
    
    
    private double calculateRemainingDistance()
    {
        return Math.abs(targetExtensionInInches - leg.getLegExtensionDistance());
    }
    
    /**
     * The execute() method is called repeatedly when this  
     * Command is scheduled to run (such as by a button being
     * pushed or some code, such as in autonomous mode, running
     * the command). It is called repeatedly until either
     * isFinish() returns true, interrupted() is called,
     * or the command is canceled. Note that the initialize()
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @Override
    protected void executeImpl() throws Exception
    {
        final double maxSpeed = Leg.getMaxSpeed(legMovementDirectionNeeded);
        final double minSpeed =Leg.getMinSpeed(legMovementDirectionNeeded);
        double speed = maxSpeed;
        final double remainingDistance = calculateRemainingDistance();
        
        // Slow Down as we approach our target to prevent overshoot
        
        if (remainingDistance < 1.0)
        {
            speed = maxSpeed / 2;
        }
        else if (remainingDistance <= 0.25)
        {
            speed = minSpeed;
        }
    
        // prevent stalling
        if ((speed > 0) && (speed < minSpeed))
        {
            speed = minSpeed;
        }
        
        leg.moveLeg(legMovementDirectionNeeded, speed);
    }
    
    
    /**
     * <p>
     * Returns whether this command is finished. If it is, then the command will be removed 
     * from the command scheduler and {@link #end()} will be called.
     * </p><p>
     * It may be useful for a team to reference the {@link #isTimedOut()}
     * method for time-sensitive commands.
     * </p><p>
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning (hard-coded) true will 
     * result in the command executing once and finishing immediately. It is recommended to use
     * {@link edu.wpi.first.wpilibj.command.InstantCommand} (added in 2017) for this.
     * </p>
     *
     * @return whether this command is finished.
     *
     * @see #isTimedOut() isTimedOut()
     */
    @SuppressWarnings("RedundantThrows")
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return isAtTarget() || (legMovementDirectionNeeded == LegMovementDirection.Stopped);
    }
    
    
    /**
     * Called once when the command ended peacefully; that is it is called once
     * after {@link #isFinished()} returns true. This is where you may want to
     * wrap up loose ends, like shutting off a motor that was being used in the
     * command or to set any post run values.
     */
    @SuppressWarnings("RedundantThrows")
    protected void endImpl() throws Exception
    {
        leg.stopLeg();
    }
    
    
    /**
     * <p>
     * Called when the command ends because somebody called {@link #cancel()} or
     * another command shared the same requirements as this one, and booted it out. For example,
     * it is called when another command which requires one or more of the same
     * subsystems is scheduled to run.
     * </p><p>
     * This is where you may want to wrap up loose ends, like shutting off a motor that was being
     * used in the command.
     * </p><p>
     * Generally, it is useful to simply call the {@link #end()} method within this
     * method, as done in the default implementation.
     * </p>
     */
    @SuppressWarnings({"RedundantThrows", "RedundantMethodOverride"})
    protected void interruptedImpl() throws Exception
    {
        end();
    }
}
