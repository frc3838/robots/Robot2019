package frc.team3838.game.commands.elevator;

import frc.team3838.game.subsystems.ElevatorSubsystem;
import frc.team3838.game.subsystems.ElevatorSubsystem.Position;



public class MoveElevatorUpOnePositionCommand extends MoveElevatorToPositionCommand
{
    public MoveElevatorUpOnePositionCommand()
    {
        super();
    }


    /**
     * The initialize() method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute()
     * method is called repeatedly until isFinished() returns true,
     * or interrupted() is called by the command scheduler/runner.
     * After isFinished() returns true, end() is called one time
     * in order to do any cleanup (such as stopping a motor) or
     * to set any post run values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        targetPosition = Position.nextPositionUp(ElevatorSubsystem.getInstance().getPosition());
        super.initializeImpl();
    }
}
