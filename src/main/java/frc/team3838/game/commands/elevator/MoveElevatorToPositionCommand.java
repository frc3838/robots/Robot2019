package frc.team3838.game.commands.elevator;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableSet;

import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.logging.PeriodicLogger;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.subsystems.ElevatorSubsystem;
import frc.team3838.game.subsystems.ElevatorSubsystem.Position;
import frc.team3838.game.subsystems.ElevatorSubsystem.RelativePosition;



public class MoveElevatorToPositionCommand extends Abstract3838Command
{

    private final PeriodicLogger executionLogger = new PeriodicLogger(logger, new TimeSetting(1, TimeUnit.SECONDS));
    private final ElevatorSubsystem elevatorSubsystem = ElevatorSubsystem.getInstance();

    @Nullable
    ElevatorSubsystem.Position targetPosition;
    @Nullable
    private ElevatorSubsystem.Position startingPosition;
    private int startingCount;
    private int targetCount;

    private ElevatorSubsystem.RelativePosition relativePosition;


    MoveElevatorToPositionCommand()
    {

    }


    @API
    public MoveElevatorToPositionCommand(Position targetPosition)
    {
        super("Elevator to " + targetPosition);
        this.targetPosition = targetPosition;
    }


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return ImmutableSet.of(
            ElevatorSubsystem.getInstance());
    }


    /**
     * The initialize() method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute()
     * method is called repeatedly until isFinished() returns true,
     * or interrupted() is called by the command scheduler/runner.
     * After isFinished() returns true, end() is called one time
     * in order to do any cleanup (such as stopping a motor) or
     * to set any post run values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        if (!elevatorSubsystem.isElevatorResetInProgress())
        {
            startingPosition = elevatorSubsystem.getPosition();

            if ((targetPosition == null) || (startingPosition == null))
            {
                logger.warn("targetPosition ('{}') was not set or startingPosition ('{}') was null. Elevator will not move", targetPosition, startingPosition);
                relativePosition = RelativePosition.AtTarget;
                return;
            }

            startingCount = elevatorSubsystem.getCount();

            relativePosition = elevatorSubsystem.getRelativePosition(targetPosition);
            if (relativePosition == RelativePosition.AboveTarget)
            {
                targetCount = targetPosition.getFirstCountPosition();
            }
            else
            {
                targetCount = targetPosition.getSecondCountPosition();
            }
            logger.debug("Move Elevator initialized>> startingPosition = {} targetPosition = {} RelativePosition = {} targetCount = {}. startingCount = {}",
                         startingPosition,
                         targetPosition,
                         relativePosition,
                         targetCount,
                         startingCount
            );
        }
        else
        {
            logger.debug("Elevator reset is in progress. Will not initialize or execute the move command");
        }
    }


    /**
     * The execute() method is called repeatedly when this
     * Command is scheduled to run (such as by a button being
     * pushed or some code, such as in autonomous mode, running
     * the command). It is called repeatedly until either
     * isFinish() returns true, interrupted() is called,
     * or the command is canceled. Note that the initialize()
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @Override
    protected void executeImpl() throws Exception
    {
        if (!elevatorSubsystem.isElevatorResetInProgress())
        {
            final int currentCount = elevatorSubsystem.getCount();
            String calling = "Not Set";
            if (relativePosition == RelativePosition.AboveTarget)
            {
                calling = "lowerElevator()";
                elevatorSubsystem.lowerElevator();
            }
            else if (relativePosition == RelativePosition.BelowTarget)
            {
                calling = "raiseElevator()";
                elevatorSubsystem.raiseElevator();
            }

            executionLogger.debug("ExecuteImpl: calling {} : currentCount = {} startingPosition = {} targetPosition = {} RelativePosition = {} targetCount = {}. startingCount = {}",
                                  calling,
                                  currentCount,
                                  startingPosition,
                                  targetPosition,
                                  relativePosition,
                                  targetCount,
                                  startingCount
            );
        }
        else
        {
            executionLogger.debug("Elevator reset is in progress. Will not execute the move command");
        }
    }


    /**
     * <p>
     * Returns whether this command is finished. If it is, then the command will be removed
     * from the command scheduler and {@link #end()} will be called.
     * </p><p>
     * It may be useful for a team to reference the {@link #isTimedOut()}
     * method for time-sensitive commands.
     * </p><p>
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning (hard-coded) true will
     * result in the command executing once and finishing immediately. It is recommended to use
     * {@link edu.wpi.first.wpilibj.command.InstantCommand} (added in 2017) for this.
     * </p>
     *
     * @return whether this command is finished.
     *
     * @see #isTimedOut() isTimedOut()
     */
    @SuppressWarnings("RedundantThrows")
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        final int currentCount = elevatorSubsystem.getCount();

        if (relativePosition == RelativePosition.AtTarget) return true;

        final boolean isFinishedFlag = currentCount == targetCount;
        if (isFinishedFlag)
        {
            logger.debug("isFinishedSetToTrue: currentCount = {} : startingPosition = {} targetPosition = {} RelativePosition = {} targetCount = {}. startingCount = {}",
                         currentCount,
                         startingPosition,
                         targetPosition,
                         relativePosition,
                         targetCount,
                         startingCount
            );
        }
        return isFinishedFlag;

//
//        final int targetGoalCount =
//
//        if (relativePosition == RelativePosition.AboveTarget)
//       {
//
//       }
//       (elevatorSubsystem.getPosition() == targetPosition)
//       )
//        );
    }


    /**
     * Called once when the command ended peacefully; that is it is called once
     * after {@link #isFinished()} returns true. This is where you may want to
     * wrap up loose ends, like shutting off a motor that was being used in the
     * command or to set any post run values.
     */
    @SuppressWarnings("RedundantThrows")
    protected void endImpl() throws Exception
    {
        elevatorSubsystem.holdElevator();
    }


    /**
     * <p>
     * Called when the command ends because somebody called {@link #cancel()} or
     * another command shared the same requirements as this one, and booted it out. For example,
     * it is called when another command which requires one or more of the same
     * subsystems is scheduled to run.
     * </p><p>
     * This is where you may want to wrap up loose ends, like shutting off a motor that was being
     * used in the command.
     * </p><p>
     * Generally, it is useful to simply call the {@link #end()} method within this
     * method, as done in the default implementation.
     * </p>
     */
    @SuppressWarnings({"RedundantThrows", "RedundantMethodOverride"})
    protected void interruptedImpl() throws Exception
    {
        end();
    }
}
