package frc.team3838.game.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.core.commands.drive.StopDrivingCommand;
import frc.team3838.game.commands.ball.StopBallCollectorCommand;
import frc.team3838.game.commands.elevator.StopElevatorCommand;



public class StopAllMotorsCommandGroup extends CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    
    public StopAllMotorsCommandGroup()
    {
        // See http://wpilib.screenstepslive.com/s/4485/m/13810/l/241903?data-resolve-url=true&data-manual-id=13810
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.
        
        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.
        
        // A command group will require all of the subsystems that each member would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the arm.
        
        addSequential(new StopElevatorCommand());
        addSequential(new StopBallCollectorCommand());
        addSequential(new StopDrivingCommand());
    }
}
