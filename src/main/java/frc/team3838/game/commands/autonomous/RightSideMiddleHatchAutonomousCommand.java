package frc.team3838.game.commands.autonomous;

public class RightSideMiddleHatchAutonomousCommand extends AbstractMiddleHatchAutonomousCommand
{
    @Override
    protected boolean isFirstTurnNegative() { return false; }


//    @Override
//    protected int getSecondTurnAbsoluteAngle() { return 103; }


    @Override
    protected double getMiddleLegLength() { return 169.5; }


    @Override
    protected double getFinalLegDistance() { return 30; }
}
