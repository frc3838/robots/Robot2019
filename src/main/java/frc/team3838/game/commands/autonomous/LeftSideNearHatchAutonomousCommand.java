package frc.team3838.game.commands.autonomous;

public class LeftSideNearHatchAutonomousCommand extends AbstractNearHatchAutonomousCommand
{
    @Override
    protected boolean isFirstTurnNegative() { return true; }
}
