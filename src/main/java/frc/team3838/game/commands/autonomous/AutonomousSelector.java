package frc.team3838.game.commands.autonomous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.autonomous.AutonomousCommandConfiguration;
import frc.team3838.core.commands.autonomous.AutonomousCommandConfigurationImpl;
import frc.team3838.core.commands.common.NoOpCommand;
import frc.team3838.core.commands.drive.DrivePidRotateCommand;
import frc.team3838.core.commands.drive.DriveStraightSetDistanceCommand;
import frc.team3838.core.dashboard.SortedSendableChooser;
import frc.team3838.core.dashboard.SortedSendableChooser.Order;



public class AutonomousSelector
{
    private static final Logger logger = LoggerFactory.getLogger(AutonomousSelector.class);

    public static final boolean INCLUDE_TEST_OPTIONS = false;
    public static final boolean ENABLE_DASHBOARD_TUNING = false;



    private static final String DELAY_DASHBOARD_KEY = "Autonomous Drive Straight Delay in Seconds";

    public static final SortedSendableChooser<AutonomousCommandConfiguration> chooser = new SortedSendableChooser<>(Order.Insertion);





    // 2019 Commands ============

    private static AutonomousCommandConfiguration frontHatch = new AutonomousCommandConfigurationImpl("Front Hatch Full Autonomous", new FrontHatchAutonomousCommand());
    private static AutonomousCommandConfiguration frontHatchDriverFinish = new AutonomousCommandConfigurationImpl("Front Hatch with Driver Assist", new FrontHatchWithDriverAssistAutonomousCommand());

    private static AutonomousCommandConfiguration leftSideNearHatch = new AutonomousCommandConfigurationImpl("Left Side Near Hatch Full Autonomous", new LeftSideNearHatchAutonomousCommand());
    private static AutonomousCommandConfiguration rightSideNearHatch = new AutonomousCommandConfigurationImpl("Right Side Near Hatch Full Autonomous", new RightSideNearHatchAutonomousCommand());


    private static AutonomousCommandConfiguration leftSideMiddleHatch = new AutonomousCommandConfigurationImpl("Left Side Middle Hatch Full Autonomous", new LeftSideMiddleHatchAutonomousCommand());
    private static AutonomousCommandConfiguration rightSideMiddleHatch = new AutonomousCommandConfigurationImpl("Right Side Middle Hatch Full Autonomous", new RightSideMiddleHatchAutonomousCommand());


//    private static AutonomousCommandConfiguration angleAdjustTesting = new AutonomousCommandConfigurationImpl("AngleAdjustTesting", new AngleAdjustTestingCommand());
//    private static AutonomousCommandConfiguration justExit = new AutonomousCommandConfigurationImpl("JustExitPlatformTesting", new ExitPlatformAutonomousCommand());





    // TEST COMMANDS
    private static AutonomousCommandConfiguration rotateNinetyCW =  new AutonomousCommandConfigurationImpl("TEST: Rotate 90 degrees CW", new DrivePidRotateCommand(90));
    private static AutonomousCommandConfiguration rotateNinetyCCW = new AutonomousCommandConfigurationImpl("TEST: Rotate -90 degrees CCW", new DrivePidRotateCommand(-90));
    private static AutonomousCommandConfiguration rotateFortyFive = new AutonomousCommandConfigurationImpl("TEST: Rotate 45 degrees", new DrivePidRotateCommand(45));
    private static AutonomousCommandConfiguration rotateSixtyCW =   new AutonomousCommandConfigurationImpl("TEST: Rotate  60 degrees CW", new DrivePidRotateCommand(60));
    private static AutonomousCommandConfiguration rotateSixtyCCW =  new AutonomousCommandConfigurationImpl("TEST: Rotate -60 degrees CCW", new DrivePidRotateCommand(-60));
//    private static AutonomousCommandConfiguration rotateVariable =  new AutonomousCommandConfigurationImpl("TEST: Rotate Variable as Set on Dashboard", new DrivePidRotateVariableSettingCommand());
    private static AutonomousCommandConfiguration driveStraight36 =   new AutonomousCommandConfigurationImpl("TEST: Drive Straight 36 inches", new DriveStraightSetDistanceCommand(36, ENABLE_DASHBOARD_TUNING));
    private static AutonomousCommandConfiguration driveStraight60 =   new AutonomousCommandConfigurationImpl("TEST: Drive Straight 60 inches", new DriveStraightSetDistanceCommand(60, ENABLE_DASHBOARD_TUNING));
    private static AutonomousCommandConfiguration driveStraight100 =  new AutonomousCommandConfigurationImpl("TEST: Drive Straight 100 inches", new DriveStraightSetDistanceCommand(100, ENABLE_DASHBOARD_TUNING));
    private static AutonomousCommandConfiguration driveStraight120 =  new AutonomousCommandConfigurationImpl("TEST: Drive Straight 120 inches", new DriveStraightSetDistanceCommand(120, ENABLE_DASHBOARD_TUNING));
    private static AutonomousCommandConfiguration driveStraight200 =  new AutonomousCommandConfigurationImpl("TEST: Drive Straight 200 inches", new DriveStraightSetDistanceCommand(200, ENABLE_DASHBOARD_TUNING));


    public static void initializeChooser()
    {
        try
        {
            logger.info("Setting up autonomous mode chooser...");

            addChoice(frontHatch);
            addChoice(frontHatchDriverFinish);
            addChoice(leftSideNearHatch);
            addChoice(rightSideNearHatch);
            addChoice(leftSideMiddleHatch);
            addChoice(rightSideMiddleHatch);

//            addChoice(angleAdjustTesting);
//            addChoice(justExit);



            // DEFAULT
            addDefault(new AutonomousCommandConfigurationImpl("DEFAULT: Do Nothing / No Autonomous Mode", NoOpCommand.getInstance()));


            if (INCLUDE_TEST_OPTIONS)
            {
                addChoice(rotateNinetyCW);
                addChoice(rotateNinetyCCW);
                addChoice(rotateFortyFive);
                addChoice(rotateSixtyCW);
                addChoice(rotateSixtyCCW);
                addChoice(driveStraight36);
                addChoice(driveStraight60);
                addChoice(driveStraight100);
                addChoice(driveStraight120);
                addChoice(driveStraight200);
            }


            SmartDashboard.putData("Autonomous Modes Selection...", chooser);
            logger.info("Autonomous mode chooser set up completed.");

        }
        catch (Throwable e)
        {
            final String msg = String.format("An error occurred when setting up Autonomous Chooser. Summary: %s", e.toString());
            DriverStation.reportError(msg, false);
            logger.error(msg, e);
        }
    }


    public static Command getSelectedCommand()
    {
        try
        {
            return chooser.getSelected().getCommand();
        }
        catch (Exception e)
        {
            final String msg = String.format("An error occurred selecting Autonomous Mode from chooser. Using NoOpCommand. Summary: %s", e.toString());
            DriverStation.reportError(msg, false);
            logger.error(msg, e);
            return NoOpCommand.getInstance();
        }
    }


    protected static void addChoice(AutonomousCommandConfiguration commandConfiguration)
    {
        chooser.addOption(commandConfiguration.getDescription(), commandConfiguration);
    }


    protected static void addDefault(AutonomousCommandConfiguration commandConfiguration)
    {
        chooser.setDefaultOption(commandConfiguration.getDescription(), commandConfiguration);
    }
}
