package frc.team3838.game.commands.autonomous;

public abstract class AbstractNearHatchAutonomousCommand extends AbstractSideHatchAutonomousCommand
{
    @Override
    protected int getFirstTurnAbsoluteAngle() { return 12; }

    @Override
    protected double getMiddleLegLength() { return 139.0; }

//    @Override
//    protected int getSecondTurnAbsoluteAngle() { return 107; }

    @Override
    protected double getFinalLegDistance() { return 36.5; }

}
