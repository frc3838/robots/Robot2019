package frc.team3838.game.commands.autonomous;

public class LeftSideMiddleHatchAutonomousCommand extends AbstractMiddleHatchAutonomousCommand
{
    @Override
    protected boolean isFirstTurnNegative() { return true; }
}
