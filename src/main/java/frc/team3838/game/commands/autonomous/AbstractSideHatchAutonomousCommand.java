package frc.team3838.game.commands.autonomous;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.commands.autonomous.Abstract3838AutonomousCommandGroup;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.commands.common.SleepCommand.PartialSeconds;
import frc.team3838.core.commands.drive.DrivePidRotateNoResetCommand;
import frc.team3838.core.commands.drive.DriveStraightSetDistanceCommand;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.game.commands.hatchEjector.EjectHatchCommandGroup;



public abstract class AbstractSideHatchAutonomousCommand extends Abstract3838AutonomousCommandGroup
{
    private final Logger logger = LoggerFactory.getLogger(getClass());


    protected abstract boolean isFirstTurnNegative();


    /**
     * Add commands in this method:
     * <pre>
     *      Run in Sequence
     *          e.g. addSequential(new Command1());
     *               addSequential(new Command2());
     *      these will run in order.
     *
     *      To run multiple commands at the same time, use addParallel()
     *          e.g. addParallel(new Command1());
     *               addSequential(new Command2());
     *      Command1 and Command2 will run in parallel.
     *
     *      A command group will require all of the subsystems that each member would require.
     *          e.g. if Command1 requires chassis, and Command2 requires arm,
     *          a CommandGroup containing them would require both the chassis and the arm.
     * </pre>
     */
    @Override
    protected void addCommands()
    {
        //addSequential(new RestNavxCommand());
        addSequential(new ExitPlatformAutonomousCommand());
        addSequential(SleepCommand.createSeconds(PartialSeconds.OneSixteenthSecond));
        int factor = isFirstTurnNegative() ? -1 : 1;
        addSequential(new DrivePidRotateNoResetCommand(getFirstTurnAbsoluteAngle() * factor, new TimeSetting(3, TimeUnit.SECONDS)));
        final DriveStraightSetDistanceCommand middleLegDrive = new DriveStraightSetDistanceCommand(getMiddleLegLength());
        middleLegDrive.setShouldResetNavX(false);
        addSequential(middleLegDrive);
//        addSequential(new DrivePidRotateCommand(getSecondTurnAbsoluteAngle() * factor * -1));
        addSequential(new DrivePidRotateNoResetCommand(89.5 * factor * -1));
        final DriveStraightSetDistanceCommand finalLeg = new DriveStraightSetDistanceCommand(getFinalLegDistance(), new TimeSetting(2, TimeUnit.SECONDS));
        finalLeg.setShouldResetNavX(false);
        addSequential(finalLeg);
        addSequential(new EjectHatchCommandGroup());

    }


    protected abstract double getFinalLegDistance();

//    protected abstract int getSecondTurnAbsoluteAngle();

    protected abstract double getMiddleLegLength();

    protected abstract int getFirstTurnAbsoluteAngle();
}
