package frc.team3838.game.commands.autonomous;

import frc.team3838.core.commands.autonomous.Abstract3838AutonomousCommandGroup;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.commands.common.SleepCommand.PartialSeconds;
import frc.team3838.core.commands.drive.DriveStraightSetDistanceCommand;
import frc.team3838.game.commands.hatchEjector.EjectHatchCommandGroup;



public class FrontHatchAutonomousCommand extends Abstract3838AutonomousCommandGroup
{
    /**
     * Add commands in this method:
     * <pre>
     *      Run in Sequence
     *          e.g. addSequential(new Command1());
     *               addSequential(new Command2());
     *      these will run in order.
     *
     *      To run multiple commands at the same time, use addParallel()
     *          e.g. addParallel(new Command1());
     *               addSequential(new Command2());
     *      Command1 and Command2 will run in parallel.
     *
     *      A command group will require all of the subsystems that each member would require.
     *          e.g. if Command1 requires chassis, and Command2 requires arm,
     *          a CommandGroup containing them would require both the chassis and the arm.
     * </pre>
     */
    @Override
    protected void addCommands()
    {
        addSequential(new ExitPlatformAutonomousCommand());
        addSequential(new DriveStraightSetDistanceCommand(135 - ExitPlatformAutonomousCommand.EXIT_DISTANCE));
        addSequential(SleepCommand.createSeconds(PartialSeconds.OneEighthSecond));
        addSequential(new EjectHatchCommandGroup());
    }
}
