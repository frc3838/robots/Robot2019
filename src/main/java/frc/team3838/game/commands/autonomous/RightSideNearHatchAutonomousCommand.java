package frc.team3838.game.commands.autonomous;

@SuppressWarnings("MethodDoesntCallSuperMethod")
public class RightSideNearHatchAutonomousCommand extends AbstractNearHatchAutonomousCommand
{
    @Override
    protected boolean isFirstTurnNegative() { return false; }


//    @Override
//    protected int getSecondTurnAbsoluteAngle() { return 103; }


    @Override
    protected double getFinalLegDistance() { return 30; }
}
