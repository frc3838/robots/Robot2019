package frc.team3838.game.commands.autonomous;

public abstract class AbstractMiddleHatchAutonomousCommand extends AbstractSideHatchAutonomousCommand
{
    @Override
    protected int getFirstTurnAbsoluteAngle() { return 12; }

    @Override
    protected double getMiddleLegLength() { return 170.5; }

//    @Override
//    protected int getSecondTurnAbsoluteAngle() { return 107; }

    @Override
    protected double getFinalLegDistance() { return 42; }
}
