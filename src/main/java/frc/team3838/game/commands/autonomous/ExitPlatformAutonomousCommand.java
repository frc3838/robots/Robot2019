package frc.team3838.game.commands.autonomous;

import frc.team3838.core.commands.Abstract3838CommandGroup;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.commands.common.SleepCommand.PartialSeconds;
import frc.team3838.core.commands.drive.DriveStraightSetDistanceCommand;



public class ExitPlatformAutonomousCommand extends Abstract3838CommandGroup
{
    public static final int EXIT_DISTANCE = 50;


    public ExitPlatformAutonomousCommand()
    {
        addSequential(new DriveStraightSetDistanceCommand(EXIT_DISTANCE, 0.28));
        addSequential(SleepCommand.createSeconds(PartialSeconds.HalfSecond));
    }
}
