package frc.team3838.game;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.config.AbstractIOAssignments;
import frc.team3838.core.controls.ButtonAction;
import frc.team3838.core.controls.DualComboButton;
import frc.team3838.core.controls.HidBasedAxisPair;
import frc.team3838.core.controls.OrButton;
import frc.team3838.core.dashboard.SortedSendableChooser;
import frc.team3838.core.dashboard.SortedSendableChooser.Order;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.commands.ball.EjectBallWithAutoShutoffCommand;
import frc.team3838.game.commands.ball.IntakeBallContinuousCommand;
import frc.team3838.game.commands.ball.StopBallCollectorCommand;
import frc.team3838.game.commands.elevator.FullyResetElevatorCommand;
import frc.team3838.game.commands.elevator.MoveElevatorDownOnePositionCommand;
import frc.team3838.game.commands.elevator.MoveElevatorUpOnePositionCommand;
import frc.team3838.game.commands.elevator.StopElevatorCommand;
import frc.team3838.game.commands.hatchEjector.EjectHatchCommandGroup;
import frc.team3838.game.controls.ElevatorStatusButton;
import frc.team3838.game.subsystems.BallCollectorSubsystem;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.DriverControlConfig2019;
import frc.team3838.game.subsystems.ElevatorSubsystem;
import frc.team3838.game.subsystems.HatchEjectorSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;
import frc.team3838.game.subsystems.UsbCameraSubsystem;
import frc.team3838.game.subsystems.lift.LiftSubsystem;



/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap
{
    /**
     * Factor added to AIO & DIO port and PWM Channel numbers on the MXP (myRIO Expansion Port) 'More Board'.
     * Thus DIO port 2 on the MXP More board is DIO 12 (2 + ADD_FACTOR) to the roboRIO.
     * And  AIO port 5 on the MXP More board is DIO 15 (5 + ADD_FACTOR) to the roboRIO.
     * <br/>
     * See <a href='www.revrobotics.com/product/more-board/'>www.revrobotics.com/product/more-board/</a> <br/>
     * See <a href='http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering'>http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering</a><br/>
     */
    @SuppressWarnings("unused")
    public static final int MXP_ADD_FACTOR = 10;

    @SuppressWarnings("WeakerAccess")
    @Nonnull
    public static final ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap = initSubsystemsMap();

    public static final boolean isDriveTrainEnabled = true;

    private static ImmutableMap<Class<? extends I3838Subsystem>, Boolean> initSubsystemsMap()
    {
        final ImmutableMap.Builder<Class<? extends I3838Subsystem>, Boolean> builder = ImmutableMap.builder();

        builder.put(DriveTrainSubsystem.class, isDriveTrainEnabled);
        builder.put(NavxSubsystem.class, true);
        builder.put(LiftSubsystem.class, false);
        builder.put(ElevatorSubsystem.class, true);
        builder.put(HatchEjectorSubsystem.class, true);
        builder.put(BallCollectorSubsystem.class, true);
        builder.put(UsbCameraSubsystem.class, true);

        return builder.build();
    }


    public static class DIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please

        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_A = 0;
        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_B = 1;

        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_A = 2;
        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_B = 3;

        public static final int ELEVATOR_ENCODER_UP = 4;
        public static final int ELEVATOR_ENCODER_DOWN = 5;

        public static final int ELEVATOR_BOTTOM_LIMIT_SWITCH = 6;

        public static final int BALL_SENSOR = 9;


        // TODO These values are placeholders. We need to verify where they are actually connected
        public static final int LEG_FRONT_LEFT_ROTATION_SENSOR = 18;
        public static final int LEG_FRONT_RIGHT_ROTATION_SENSOR = 19;
        public static final int LEG_REAR_LEFT_ROTATION_SENSOR = 20;
        public static final int LEG_REAR_RIGHT_ROTATION_SENSOR = 13;


        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_14 = 14;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_15 = 15;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_16 = 16;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_17 = 17;

        // Keep assignments in numerical order please
    
        
        private DIOs() {/* Static use only. */}
    }

    public static class AIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
    
    
        private AIOs() {/* Static use only. */}
    }

    public static class PWMs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        public static final int ELEVATOR_MOTOR = 0;
    
    
        private PWMs() {/* Static use only. */}
    }

    /**
     * Port assignments for the Pneumatic Control Module (PCM).
     * [Not to be confused with the PWMs (Pulse Width Modulation) channel assignment class.]
     */
    public static class PCM extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        public static final int HATCH_EJECTOR_SolenoidForwardChannel = 1;
        public static final int HATCH_EJECTOR_SolenoidReverseChannel = 0;
    
    
        private PCM() {/* Static use only. */}
    }

    public static class Relays extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
    
    
        private Relays() {/* Static use only. */}
    }

    public static class CANs extends AbstractIOAssignments
    {
        /*
        CAN IDs are assigned in the roboRIO UI -- http://172.22.11.2 -- **ALWAYS** assign as ID as they default to Zero
        So keep Zero "Free". See Section 2.2. "Common ID Talons" of the "Talon SRX Software Reference Manual.pdf"
        in C:\Users\Developer\Downloads\FRC\Hardware Components\Talon SRX or download from
        http://www.ctr-electronics.com/talon-srx.html#product_tabs_technical_resources
        From that Section:

            TIP: Since the default device ID of an "out of the box" Talon SRX is device ID '0', when you setup your
                 robot for the first time, start assigning device IDs at '1'. That way you can, at any time, add
                 another default Talon to your bus and easily identify it.
        */

        // Keep assignments in numerical order please
        @SuppressWarnings("unused")
        public static final int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;

        public static final int DRIVE_LEFT_SIDE_REAR = 1;      // a.k.a. Driver's side
        public static final int DRIVE_LEFT_SIDE_FRONT = 2;
        public static final int DRIVE_RIGHT_SIDE_REAR = 3;     // a.k.a. Passenger's side
        public static final int DRIVE_RIGHT_SIDE_FRONT = 4;

        public static final int BALL_COLLECTOR_MOTOR = 5;

        public static final int LEG_MOTOR_RIGHT_REAR = 11; // placeholder value
        public static final int LEG_MOTOR_RIGHT_FRONT = 6;
        public static final int LEG_MOTOR_LEFT_REAR = 7;
        public static final int LEG_MOTOR_LEFT_FRONT = 8;



        /**
         * The CAN Bus ID for the Power Distribution Panel (PDP). We usually do not need to access
         * this at all. But we do document its CAN ID here to prevent conflicts.
         */
        @SuppressWarnings("unused")
        public static final int PDP = 9;

        /** The CAN ID for the Pneumatic Control Module (i.e. the 'Compressor'). */
        @SuppressWarnings("unused")
        public static final int PCM = 10;
    
    
        private CANs() {/* Static use only. */}
    }



    public static class UI
    {
        @SuppressWarnings("unused")
        private static final Logger logger = LoggerFactory.getLogger(UI.class);

        @API
        public static final ImmutableList<DriverControlConfig2019> configs;
        private static final SortedSendableChooser<DriverControlConfig2019> configChooser;


        public static DriverControlConfig2019 getDriveTrainControlConfiguration()
        {
            return configChooser.getSelected();
        }


        static
        {
            int loopCount = 0;
            while (!Subsystems.haveSubsystemsBeenInitialized() && (loopCount < 20))
            {
                loopCount++;
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}
            }

            final Builder<DriverControlConfig2019> listBuilder = ImmutableList.builder();
            final Logger logger = LoggerFactory.getLogger(UI.class);

            logger.info("Creating Joystick DriveConfigurations");

            final Joystick primaryStick = new Joystick(1);

            final Button reverseModeButtonA = new JoystickButton(primaryStick, 8);
            final Button reverseModeButtonB = new JoystickButton(primaryStick, 9);
            final Button reverseModeButton = new OrButton(reverseModeButtonA, reverseModeButtonB);

            final Button autoFineControlButton = new ElevatorStatusButton();

            final DriverControlConfig2019 joystickArcade =
                    new DriverControlConfig2019("Joystick Arcade Control on port 1",
                                                DriveControlMode.Arcade,
                                                new HidBasedAxisPair(primaryStick),
                                                reverseModeButton,
                                                autoFineControlButton
                    );

            createAndAssignButton(primaryStick, 1, ButtonAction.Button_WhenPressed, new EjectHatchCommandGroup());
            createAndAssignButton(primaryStick, 2, ButtonAction.Button_WhenPressed, new IntakeBallContinuousCommand() /*new IntakeBallWithAutoShutoffCommand()*/);
            createAndAssignButton(primaryStick, 3, ButtonAction.Button_WhenPressed, new EjectBallWithAutoShutoffCommand());
            createAndAssignButton(primaryStick, 6, ButtonAction.Button_WhenPressed, new StopBallCollectorCommand());



            createAndAssignButton(primaryStick, 4, ButtonAction.Button_WhenPressed, new MoveElevatorDownOnePositionCommand());
            createAndAssignButton(primaryStick, 5, ButtonAction.Button_WhenPressed, new MoveElevatorUpOnePositionCommand());

//            createAndAssignButton(primaryStick, 4, ButtonAction.Button_WhileHeld, new MoveElevatorDownAndHoldCommand());
//            createAndAssignButton(primaryStick, 4, ButtonAction.Button_WhenReleased, new HoldElevatorCommand());
//            createAndAssignButton(primaryStick, 5, ButtonAction.Button_WhileHeld, new MoveElevatorUpAndHoldCommand());
//            createAndAssignButton(primaryStick, 5, ButtonAction.Button_WhenReleased, new HoldElevatorCommand());




            createAndAssignButton(primaryStick, 11, ButtonAction.Button_WhenPressed, new StopElevatorCommand());

            final DualComboButton resetElevatorButton = new DualComboButton(new JoystickButton(primaryStick, 7), new JoystickButton(primaryStick, 10));
            assignButton(resetElevatorButton, ButtonAction.Button_WhenPressed, new FullyResetElevatorCommand());

            listBuilder.add(joystickArcade);

            configs = listBuilder.build();
            configChooser = new SortedSendableChooser<>(Order.Insertion);

            if (configs.isEmpty())
            {
                logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
                final DriverControlConfig2019 noOpConfig = DriverControlConfig2019.getDriverControlConfig2017NoOpInstance();
                configChooser.setDefaultOption("No operation - no joystick/gamepad is connected.", noOpConfig);
            }
            else if (configs.size() == 1)
            {
                final DriverControlConfig2019 config = configs.get(0);
                configChooser.setDefaultOption(config.getConfigurationName(), config);
            }
            else
            {
                for (int i = 0; i < configs.size(); i++)
                {
                    final DriverControlConfig2019 config = configs.get(i);
                    final String prefix = "# " + (i + 1) + ") ";
                    if (i == 0)
                    {
                        configChooser.setDefaultOption(prefix + config.getConfigurationName(), config);
                    }
                    else
                    {
                        configChooser.addOption(prefix + config.getConfigurationName(), config);
                    }
                }
            }


            SmartDashboard.putData("==UI Configuration Chooser==", configChooser);
        }

//        public static void previousStaticInitializer()
//        {
//
//            final Builder<DriverControlConfig2019> listBuilder = ImmutableList.builder();
//            final Logger logger = LoggerFactory.getLogger(UI.class);
//
//
//
//
//
//                /*
//                  ___                              _
//                 / __|__ _ _ __  ___ _ __  __ _ __| |
//                | (_ / _` | '  \/ -_) '_ \/ _` / _` |
//                 \___\__,_|_|_|_\___| .__/\__,_\__,_|
//                                    |_|
//                 */
//            final int gamePadReverseModeButton = 5;
//            final int gamePadFineControlButtonNum = 6;
//
////            if (!HidUtils.isHidConnected(0) || !HidUtils.isGamepad(0))
////            {
////                logger.info("No gamepad detected on port 0. Will not create gamepad drive configuration options");
////            }
////            else
//            {
//                logger.info("Creating Gamepad DriveConfigurations");
//
//
//
//                final XboxController gamepad = new XboxController(0);
//
//                final Button reverseModeTrigger = new JoystickButton(gamepad, gamePadReverseModeButton);
//
//                final Button fineControlTrigger = new JoystickButton(gamepad, gamePadFineControlButtonNum);
//                //final Trigger fineControlTrigger =  new MultiPovButton(gamepad, PovDirection.N_UP, PovDirection.NW, PovDirection.NE);
//
//                final Trigger allowReverseClimbTrigger = new NoOpTrigger();//new JoystickButton(gamepad, 7);
//
//
//
//
//                final DriverControlConfig2019 gamepadArcade = new DriverControlConfig2019("Gamepad Arcade Control",
//                                                                                          DriveControlMode.Arcade,
//                                                                                          new HidBasedAxisPair(gamepad, 4, 5),
//                                                                                          reverseModeTrigger,
//                                                                                          fineControlTrigger
//                );
//
//                listBuilder.add(gamepadArcade);
//
//
//                final DriverControlConfig2019 gamepadTank = new DriverControlConfig2019("Gamepad Tank Control",
//                                                                                        DriveControlMode.Tank,
//                                                                                        new GamepadTankAxisPair(gamepad),
//                                                                                        reverseModeTrigger,
//                                                                                        fineControlTrigger
//                );
//                listBuilder.add(gamepadTank);
//
//            }
//
//
//
//
//
//
//            /*
//                _             _   _    _
//             _ | |___ _  _ __| |_(_)__| |__ ___
//            | || / _ \ || (_-<  _| / _| / /(_-<
//             \__/\___/\_, /__/\__|_\__|_\_\/__/
//                      |__/
//             */
//
//            final int joystickReverseModeButtonNumber = 3;
//            final int joystickFineControlButtonNumber = 1;
//
//            final int joystickAllowReverseClimbNumberA = 8;
//            final int joystickAllowReverseClimbNumberB = 9;
//
////            if (!HidUtils.isHidConnected(1))
////            {
////                logger.info("No joystick detected on port 1. Will not create joystick drive configuration options");
////            }
////            else
//            {
//
//                logger.info("Creating Joystick DriveConfigurations");
//
//                final Joystick primaryStick = new Joystick(1);
//
//                final Button arcadeReverseModeTrigger = new JoystickButton(primaryStick, joystickReverseModeButtonNumber);
//                final Trigger arcadeFineControlTrigger = new JoystickButton(primaryStick, joystickFineControlButtonNumber);
//
//                final Trigger arcadeAllowReverseClimbTrigger = new DualComboButton(new JoystickButton(primaryStick, joystickAllowReverseClimbNumberA),
//                                                                                   new JoystickButton(primaryStick, joystickAllowReverseClimbNumberB));
//
//
//
//
//                final DriverControlConfig2019 joystickArcade =
//                    new DriverControlConfig2019("Joystick Arcade Control",
//                                                DriveControlMode.Arcade,
//                                                new HidBasedAxisPair(primaryStick),
//                                                arcadeReverseModeTrigger,
//                                                arcadeFineControlTrigger
//                    );
//                listBuilder.add(joystickArcade);
//
//
//            }
//
//            // HAD TO COMMENT OUT IN THIS PLACEHOLDER METHOD SINCE WE CANNOT ASSIGN TO A FINAL HERE
////            configs = listBuilder.build();
////            configChooser = new SortedSendableChooser<>(Order.Insertion);
//
//
//            if (configs.isEmpty())
//            {
//                logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
//                final DriverControlConfig2019 noOpConfig = DriverControlConfig2019.getDriverControlConfig2017NoOpInstance();
//                configChooser.setDefaultOption("No operation - no joystick/gamepad is connected.", noOpConfig);
//            }
//            else
//            {
//                for (int i = 0; i < configs.size(); i++)
//                {
//                    final DriverControlConfig2019 config = configs.get(i);
//                    final String prefix = "# " + (i + 1) + ") ";
//                    if (i == 0)
//                    {
//                        configChooser.setDefaultOption(prefix + config.getConfigurationName(), config);
//                    }
//                    else
//                    {
//                        configChooser.addOption(prefix + config.getConfigurationName(), config);
//                    }
//                }
//            }
//
//
//
//
//            SmartDashboard.putData("==UI Configuration Chooser==", configChooser);
//
//        }


        @API
        public static Button createAndAssignButton(final GenericHID hid,
                                                   final int buttonNumber,
                                                   final ButtonAction action,
                                                   final Command command) throws IllegalArgumentException
        {
            final JoystickButton button = new JoystickButton(hid, buttonNumber);
            return assignButton(button, action, command);
        }


        @API
        public static Button assignButton(Button button, ButtonAction action, Command command)
        {
            action.assign(button, command);
            return button;
        }
    
    
        private UI() {/* Static use only. */}
    }
}
