package frc.team3838.core.dashboard;

import java.util.Map;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;



public class DashboardManager
{
    //private static final Logger logger = LoggerFactory.getLogger(DashboardManager.class);

    public static final ShuffleboardTab commandsTab = Shuffleboard.getTab("Commands");
    public static final ShuffleboardTab driveTab = Shuffleboard.getTab("Drive");
    public static final ShuffleboardTab electricalTab = Shuffleboard.getTab("Electrical");

    public static final ShuffleboardLayout axisPairLayout = DashboardManager.driveTab
        .getLayout("AdjustedAxisPairReading", BuiltInLayouts.kList)
        .withProperties(Map.of("Label position", "LEFT"))
        .withSize(2, 2);

    public static final NetworkTableEntry elevatorCountEntry = DashboardManager.commandsTab.add("Elevator Count", 0.0)
                                                                                           .getEntry();
    public static final NetworkTableEntry elevatorUpDioEntry = DashboardManager.commandsTab.add("Up DIO", Boolean.FALSE).getEntry();
    public static final NetworkTableEntry elevatorDownDioEntry = DashboardManager.commandsTab.add("Down DIO", Boolean.FALSE).getEntry();

    public static final NetworkTableEntry legSpeedEntry =
        DashboardManager.commandsTab.add("Leg Speed", 0.0)
                                    .withProperties(Map.of("Min", -1.0, "Max", 1.0, "Block increment", 0.05))
                                    .getEntry();

    private DashboardManager() {}
}
