/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008-2017. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team3838.core.dashboard;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.Nonnull;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.meta.API;

import static java.util.Objects.requireNonNull;



/**
 * The {@link SortedSendableChooser} class is a useful tool for presenting a selection of options to the
 * {@link SmartDashboard}.
 *
 * <p>For instance, you may wish to be able to select between multiple autonomous modes. You can do
 * this by putting every possible {@link Command} you want to run as an autonomous into a {@link
 * SortedSendableChooser} and then put it into the {@link SmartDashboard} to have a list of options appear
 * on the laptop. Once autonomous starts, simply ask the {@link SortedSendableChooser} what the selected
 * value is.
 *
 * @param <V> The type of the values to be stored
 */
public class SortedSendableChooser<V> implements Sendable,
                                                 AutoCloseable {

  private static final AtomicInteger s_instances = new AtomicInteger();



  /**
   * The key for the default value.
   */
  private static final String DEFAULT = "default";
  /**
   * The key for the selected option.
   */
  private static final String SELECTED = "selected";
  /**
   * The key for the active option.
   */
  private static final String ACTIVE = "active";
  /**
   * The key for the option array.
   */
  private static final String OPTIONS = "options";
  /**
   * The key for the instance number.
   */
  private static final String INSTANCE = ".instance";
  private static final String[] EMPTY_STRING_ARRAY = new String[0];

  private String m_name = "";
  private String m_subsystem = "Ungrouped";
  /**
   * A map linking strings to the objects the represent.
   */
  @SuppressWarnings("PMD.LooseCoupling")
  private final Map<String, V> m_map;
  private String m_defaultChoice = "";
  private final int m_instance = s_instances.getAndIncrement();




  public enum Order{Sorted, Insertion}

  /**
   * Instantiates a {@link SortedSendableChooser}.
   */
  @API
  public SortedSendableChooser() {
    this(Order.Insertion);
  }


  public SortedSendableChooser(Order order)
  {
    switch (order)
    {
      case Insertion:
        m_map = new LinkedHashMap<>();
        break;
      case Sorted:
        m_map = new TreeMap<>();
        break;
      default:
        // Will only happen if a partial code update occurs.
        throw new IllegalArgumentException("Unknown order type of " + order);
    }
  }

  @API
  public SortedSendableChooser(Comparator<String> nameComparator)
  {
    m_map = new TreeMap<>(nameComparator);
  }


  /**
   * Adds the given object to the list of options. On the {@link SmartDashboard} on the desktop, the
   * object will appear as the given name.
   *
   * @param name   the name of the option
   * @param object the option
   */
  @API
  public void addOption(String name, V object)
  {
    m_map.put(name, object);
  }


  /**
   * Adds the given object to the list of options.
   *
   * @param name   the name of the option
   * @param object the option
   *
   * @deprecated Use {@link #addOption(String, Object)} instead.
   */
  @Deprecated
  public void addObject(String name, V object)
  {
    addOption(name, object);
  }


  /**
   * Adds the given object to the list of options and marks it as the default. Functionally, this is
   * very close to {@link #addOption(String, Object)} except that it will use this as the default
   * option if none other is explicitly selected.
   *
   * @param name   the name of the option
   * @param object the option
   */
  @API
  public void setDefaultOption(@Nonnull String name, V object)
  {
    requireNonNull(name, "Provided name was null");

    m_defaultChoice = name;
    addOption(name, object);
  }


  /**
   * Adds the given object to the list of options and marks it as the default.
   *
   * @param name   the name of the option
   * @param object the option
   *
   * @deprecated Use {@link #setDefaultOption(String, Object)} instead.
   */
  @Deprecated
  public void addDefault(String name, V object)
  {
    setDefaultOption(name, object);
  }


  /**
   * Returns the selected option. If there is none selected, it will return the default. If there is
   * none selected and no default, then it will return {@code null}.
   *
   * @return the option selected
   */
  public V getSelected()
  {
    m_mutex.lock();
    try
    {
      return (m_selected != null) ? m_map.get(m_selected) : m_map.get(m_defaultChoice);
    }
    finally
    {
      m_mutex.unlock();
    }
  }


  private String m_selected;
  private final List<NetworkTableEntry> m_activeEntries = new ArrayList<>();
  private final ReentrantLock m_mutex = new ReentrantLock();


  @Override
  public void initSendable(SendableBuilder builder)
  {
    builder.setSmartDashboardType("String Chooser");
    builder.getEntry(INSTANCE).setDouble(m_instance);
    builder.addStringProperty(DEFAULT, () -> m_defaultChoice, null);
    builder.addStringArrayProperty(OPTIONS, () -> m_map.keySet().toArray(EMPTY_STRING_ARRAY), null);
    builder.addStringProperty(ACTIVE, () -> {
      m_mutex.lock();
      try
      {
        if (m_selected != null)
        {
          return m_selected;
        }
        else
        {
          return m_defaultChoice;
        }
      }
      finally
      {
        m_mutex.unlock();
      }
    }, null);
    m_mutex.lock();
    try
    {
      m_activeEntries.add(builder.getEntry(ACTIVE));
    }
    finally
    {
      m_mutex.unlock();
    }
    builder.addStringProperty(SELECTED, null, val -> {
      m_mutex.lock();
      try
      {
        m_selected = val;
        for (NetworkTableEntry entry : m_activeEntries)
        {
          entry.setString(val);
        }
      }
      finally
      {
        m_mutex.unlock();
      }
    });
  }

  @Deprecated
  public void free()
  {
    close();
  }


  @Override
  public void close()
  {
    LiveWindow.remove(this);
  }


  @Override
  public final synchronized String getName()
  {
    return m_name;
  }


  @Override
  public final synchronized void setName(String name)
  {
    m_name = name;
  }


  /**
   * Sets the name of the sensor with a channel number.
   *
   * @param moduleType A string that defines the module name in the label for the value
   * @param channel    The channel number the device is plugged into
   */
  @API
  protected final void setName(String moduleType, int channel)
  {
    setName(moduleType + "[" + channel + "]");
  }


  /**
   * Sets the name of the sensor with a module and channel number.
   *
   * @param moduleType   A string that defines the module name in the label for the value
   * @param moduleNumber The number of the particular module type
   * @param channel      The channel number the device is plugged into (usually PWM)
   */
  @API
  protected final void setName(String moduleType, int moduleNumber, int channel)
  {
    setName(moduleType + "[" + moduleNumber + "," + channel + "]");
  }


  @Override
  public final synchronized String getSubsystem()
  {
    return m_subsystem;
  }


  @Override
  public final synchronized void setSubsystem(String subsystem)
  {
    m_subsystem = subsystem;
  }


  /**
   * Add a child component.
   *
   * @param child child component
   */
  @API
  protected final void addChild(Object child)
  {
    LiveWindow.addChild(this, child);
  }

}
