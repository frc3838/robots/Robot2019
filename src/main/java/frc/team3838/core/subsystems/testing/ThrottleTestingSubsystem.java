package frc.team3838.core.subsystems.testing;

import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.controls.BoostSpeedThrottle;
import frc.team3838.core.controls.Throttle;
import frc.team3838.core.controls.ThrottleBuilder;
import frc.team3838.core.controls.ThrottleBuilder.Directionality;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;



@API
public class ThrottleTestingSubsystem extends Abstract3838Subsystem
{

    Throttle throttle;
    XboxController gamepad;
    private int rawAxisNumber;

    public static final int INITIAL_THROTTLE_AXIS = 2;
    public static final int BOOST_THROTTLE_AXIS = 3;
    private Throttle initialThrottle;
    private Throttle boostThrottle;


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private ThrottleTestingSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem


        gamepad = new XboxController(0);
//        throttle = new GamePadDualTriggerThrottle(joystick, GamepadSide.Right);
//          throttle =   ThrottleBuilder.getBuilder()
//                                  .using(joystick)
//                                  .usingZAxis()
//                                  .usingBiDirectionality()
//                                  .usingNonInvertedOutput()
//                                  .withDeadZoneThreshold(0.1)
//                                  .withAbsoluteScaling(0, 1.0)
//                                  .withWhileHeldButton(7, Directionality.Negative)
//                                  .build();

//        throttle = new ReversingThrottle(ThrottleBuilder.getBuilder()
//                                                        .using(joystick)
//                                                        .usingRawAxis(2)
//                                                        .usingBiDirectionality()
//                                                        .usingNonInvertedOutput()
//                                                        .withoutDeadZone()
//                                                        .withoutScaling()
//                                                        .withoutActivationButton()
//                                                        .build(), new JoystickButton(joystick, 7));
//
//
//        rawAxisNumber = 3;

        // For the climber - a negative value is climb (i.e. forward)
        initialThrottle = ThrottleBuilder.getBuilder()
                                         .using(gamepad)
                                         .usingRawAxis(INITIAL_THROTTLE_AXIS)
                                         .usingPositiveDirection()
                                         .outputPositiveValue()
                                         .withoutDeadZone()
                                         .withoutScaling()
                                         .withoutActivationButton()
                                         .build();
        boostThrottle = ThrottleBuilder.getBuilder()
                                       .using(gamepad)
                                       .usingRawAxis(BOOST_THROTTLE_AXIS)
                                       .usingPositiveDirection()
                                       .outputPositiveValue()
                                       .withoutDeadZone()
                                       .withoutScaling()
                                       .withoutActivationButton()
                                       .build();

        // For the climber - a negative value is climb (i.e. forward)
        throttle = new BoostSpeedThrottle(initialThrottle, boostThrottle, 0.1, 0.5, Directionality.Negative);

    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        setDefaultCommand(new Abstract3838Command()
        {
            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems()
            {
                return ImmutableSet.of(ThrottleTestingSubsystem.getInstance());
            }


            @Override
            protected void initializeImpl() throws Exception
            {

            }


            @Override
            protected void executeImpl() throws Exception
            {
                SmartDashboard.putString("Combo Throttle ", MathUtils.formatNumber(throttle.get(), 3));
                SmartDashboard.putString("Initial Throttle", MathUtils.formatNumber(initialThrottle.get(), 3));
                SmartDashboard.putString("Boost Throttle", MathUtils.formatNumber(boostThrottle.get(), 3));
            }


            @Override
            protected boolean isFinishedImpl() throws Exception
            {
                return false;
            }


            @Override
            protected void endImpl() throws Exception
            {

            }
        });
    }
















    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final ThrottleTestingSubsystem singleton = new ThrottleTestingSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static ThrottleTestingSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

