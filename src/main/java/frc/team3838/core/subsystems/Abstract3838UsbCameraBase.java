package frc.team3838.core.subsystems;

import java.util.Collection;
import java.util.Properties;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.UsbCameraInfo;
import edu.wpi.first.cameraserver.CameraServer;
import frc.team3838.core.RobotProperties;



abstract class Abstract3838UsbCameraBase extends Abstract3838Subsystem
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final ImmutableList<CameraConfig> cameraConfigsList;


    /**
     * Creates a camera on port 0.
     */
    protected Abstract3838UsbCameraBase()
    {
        this(ImmutableList.of(new CameraConfig(0)));
    }


    protected Abstract3838UsbCameraBase(Collection<CameraConfig> cameraConfigs)
    {
        this.cameraConfigsList = ImmutableList.copyOf(cameraConfigs);
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        initCameras();
    }


    protected ImmutableList<UsbCamera> cameras;
    private ImmutableList<Thread> cameraThreads;


    protected int getResolutionWidthSetting()
    {
        final Properties properties = RobotProperties.getRobotProperties();
        final Integer width = Integer.valueOf(properties.getProperty(RobotProperties.CAMERA_ResolutionWidthSetting,
                                                                     Integer.toString(RobotProperties.CAMERA_ResolutionWidthSetting_DEFAULT)));
        logger.info("Camera Resolution width set to {}", width);
        return width;

    }


    protected int getResolutionHeightSetting()
    {
        final Properties properties = RobotProperties.getRobotProperties();
        final Integer height = Integer.valueOf(properties.getProperty(RobotProperties.CAMERA_ResolutionHeightSetting,
                                                                      Integer.toString(RobotProperties.CAMERA_ResolutionHeightSetting_DEFAULT)));
        logger.info("Camera Resolution height set to {}", height);
        return height;
    }


    protected int getFpsSetting()
    {
        final Properties properties = RobotProperties.getRobotProperties();
        final Integer fps = Integer.valueOf(properties.getProperty(RobotProperties.CAMERA_FpsSetting,
                                                                   Integer.toString(RobotProperties.CAMERA_FpsSetting_DEFAULT)));
        logger.info("Camera FPS set to {}", fps);
        return fps;
    }



    protected void initCameras()
    {
        final Builder<UsbCamera> camListBuilder = ImmutableList.builder();
        final Builder<Thread> threadListBuilder = ImmutableList.builder();

        for (CameraConfig config : cameraConfigsList)
        {
            // I believe we are doing this on a separate thread to prevent blocking
            final Thread thread = new Thread(() ->
                                             {
                                                 logger.info("Creating & initializing Camera: {}", config);
                                                 UsbCamera camera = (config.name != null)
                                                                    ? createCamera(config.deviceNumber, config.name)
                                                                    : createCamera(config.deviceNumber);

                                                 final UsbCameraInfo cameraInfo = camera.getInfo();
                                                 logger.info("Created & initializing Camera: deviceNum={}  name='{}'  path='{}'", cameraInfo.dev, cameraInfo.name, cameraInfo.path);
                                                 camListBuilder.add(camera);

                                             });
            thread.start();
            threadListBuilder.add(thread);
        }

        cameras = camListBuilder.build();
        cameraThreads = threadListBuilder.build();
    }


    /**
     * Creates the next camera in terms of device number. The {@code CameraServer startAutomaticCapture()}
     * method increments the device number.
     *
     * @return the configured camera
     */
    @NotNull
    protected UsbCamera createNextCamera()
    {
        final UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
        return configureUsbCamera(camera);
    }


    @NotNull
    protected UsbCamera createCamera(int deviceNumber)
    {
        final UsbCamera camera = CameraServer.getInstance().startAutomaticCapture(deviceNumber);
        return configureUsbCamera(camera);
    }


    @NotNull
    protected UsbCamera createCamera(int deviceNumber, String name)
    {
        final UsbCamera camera = CameraServer.getInstance().startAutomaticCapture(name, deviceNumber);
        return configureUsbCamera(camera);
    }


    @NotNull
    private UsbCamera configureUsbCamera(UsbCamera camera)
    {
        final int width = getResolutionWidthSetting();
        final int height = getResolutionHeightSetting();
        final int fps = getFpsSetting();
        logger.info("Setting camera to {}Wx{}H @ {} FPS", width, height, fps);
        camera.setResolution(width, height);
        camera.setFPS(fps);
        return camera;
    }
}
