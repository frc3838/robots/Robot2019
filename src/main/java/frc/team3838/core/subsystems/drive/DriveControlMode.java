package frc.team3838.core.subsystems.drive;

import frc.team3838.core.meta.API;



@API
public enum DriveControlMode
{@API Arcade, @API Tank}
