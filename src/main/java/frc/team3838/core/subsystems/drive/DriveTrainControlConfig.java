package frc.team3838.core.subsystems.drive;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.controls.AxisPair;



public interface DriveTrainControlConfig
{
    @Nonnull
    String getConfigurationName();

    @Nonnull
    DriveControlMode getDriveControlMode();

    @Nonnull
    AxisPair getAxisPair();

    @Nullable
    default Trigger getReverseModeTrigger() {return null;}

    @Nullable
    default Trigger getFineControlTrigger()
    {
        return null;
    }
}
