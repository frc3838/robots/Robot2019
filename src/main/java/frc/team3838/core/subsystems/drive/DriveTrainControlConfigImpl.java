package frc.team3838.core.subsystems.drive;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.controls.AxisPair;
import frc.team3838.core.controls.NoOpAxisPair;
import frc.team3838.core.controls.NoOpTrigger;
import frc.team3838.core.meta.API;



@API
public class DriveTrainControlConfigImpl implements DriveTrainControlConfig
{
    @Nonnull
    private final String configurationName;

    @Nonnull
    private final DriveControlMode driveControlMode;

    @Nonnull
    private final AxisPair axisPair;

    @Nullable
    private final Trigger reverseModeTrigger;


    @Nullable
    private final Trigger fineControlTrigger;

    public static DriveTrainControlConfig noOpInstance;

    public DriveTrainControlConfigImpl(@Nonnull String configurationName,
                                       @Nonnull DriveControlMode driveControlMode,
                                       @Nonnull AxisPair axisPair,
                                       @Nullable Trigger reverseModeTrigger,
                                       @Nullable Trigger fineControlTrigger)
    {
        this.configurationName = configurationName;
        this.driveControlMode = driveControlMode;
        this.axisPair = axisPair;
        this.reverseModeTrigger = reverseModeTrigger;
        this.fineControlTrigger = fineControlTrigger;
    }


    public static DriveTrainControlConfig getNoOpDriveTrainControlConfig()
    {
        if (noOpInstance == null)
        {
            noOpInstance = new DriveTrainControlConfigImpl("No Op DriveTrainControlConfig",
                                                           DriveControlMode.Arcade,
                                                           new NoOpAxisPair(),
                                                           new NoOpTrigger(),
                                                           new NoOpTrigger());
        }
        return noOpInstance;
    }

    @Override
    @Nonnull
    public DriveControlMode getDriveControlMode() { return driveControlMode; }


    @Override
    @Nonnull
    public AxisPair getAxisPair() { return axisPair; }


    @Nonnull
    @Override
    public String getConfigurationName() { return configurationName; }


    @Nullable
    @Override
    public Trigger getReverseModeTrigger() { return reverseModeTrigger; }


    @Nullable
    @Override
    public Trigger getFineControlTrigger() { return fineControlTrigger; }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("configurationName", configurationName)
            .append("driveControlMode", driveControlMode)
            .append("axisPair", axisPair)
            .append("reverseModeTrigger", reverseModeTrigger)
            .append("fineControlTrigger", fineControlTrigger)
            .toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DriveTrainControlConfigImpl that = (DriveTrainControlConfigImpl) o;

        return new EqualsBuilder()
            .append(configurationName, that.configurationName)
            .append(driveControlMode, that.driveControlMode)
            .append(axisPair, that.axisPair)
            .append(reverseModeTrigger, that.reverseModeTrigger)
            .append(fineControlTrigger, that.fineControlTrigger)
            .isEquals();
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
            .append(configurationName)
            .append(driveControlMode)
            .append(axisPair)
            .append(reverseModeTrigger)
            .append(fineControlTrigger)
            .toHashCode();
    }
}
