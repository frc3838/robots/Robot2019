package frc.team3838.core.subsystems;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * While it is generally preferred to properly implement a Subsystem and corresponding Commands to
 * use the subsystem, this class provides an alternative. It allows for a more 'procedural' 
 * implementation of a subsystem, such as that in the iterative robot, that less experienced developers 
 * might be more familiar with. 
 */
public abstract class AbstractPeriodicallyExecutedSubsystem extends Abstract3838Subsystem
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    
    
    public long getPeriodicExecutionIntervalDuration() {return 50; }
    
    public TimeUnit getPeriodicExecutionIntervalTimeUnit() {return TimeUnit.MILLISECONDS; }


    /**
     * Method that will be periodically called via a scheduler.
     * It must be implemented by subclasses. 
     * This method will be periodically called by the thread
     * executor in the Robot class <em>when in teleop mode</em>.
     * The frequency of calling is determined by the return values
     * of the {@link #getPeriodicExecutionIntervalDuration()}
     * and {@link #getPeriodicExecutionIntervalTimeUnit()} methods.
     * In most cases, the default values are sufficient.
     */
    public abstract void periodicExecute();
}
