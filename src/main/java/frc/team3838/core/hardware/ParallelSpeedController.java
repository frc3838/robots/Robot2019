package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



/**
 * A {@link SpeedController} implementation that wraps two or more {@code SpeedControllers} in parallel such that
 * an operation on an instance of this {@code ParallelSpeedController} is called on all the wrapped controllers.
 * For example, is a drivetrain has two motors driving a single wheel, the two speed controllers for those two
 * motors can be wrapped in this class so that operations can be called a single time.
 */
public class ParallelSpeedController implements SpeedController
{

    private final ImmutableList<SpeedController> speedControllers;
    private SpeedController refSpeedController;


    @API
    public ParallelSpeedController(@Nonnull SpeedController speedController, @Nonnull SpeedController speedController2)
    {
        //noinspection ZeroLengthArrayAllocation
        this(speedController, speedController2, new SpeedController[0]);
    }


    @API
    public ParallelSpeedController(Iterable<SpeedController> speedControllers)
    {
        this.speedControllers = ImmutableList.copyOf(speedControllers);
        construct();
    }

    @SuppressWarnings("WeakerAccess")
    @API
    public ParallelSpeedController(@Nonnull SpeedController speedController, @Nonnull SpeedController speedController2, SpeedController... additionalSpeedControllers)
    {
        final Builder<SpeedController> collectionBuilder = ImmutableList.builder();
        collectionBuilder.add(speedController);
        collectionBuilder.add(speedController2);
        for (SpeedController controller : additionalSpeedControllers)
        {
            collectionBuilder.add(controller);
        }
        this.speedControllers = collectionBuilder.build();
        construct();
    }



    private void construct()
    {
        this.refSpeedController = this.speedControllers.get(0);
        inversionCheck();
    }

    private void inversionCheck()
    {
        boolean allHaveSameInversion = true;

        boolean invertState = refSpeedController.getInverted();
        for (SpeedController speedController : speedControllers)
        {
            if (speedController.getInverted() != invertState)
            {
                allHaveSameInversion = false;
                break;
            }
        }
        if (!allHaveSameInversion)
        {
            throw new IllegalStateException("Not all speed controllers added to the ParallelSpeedController have the same invert state. This "
                                            + "class cannot handle such a scenario at this time.");
        }
    }

    @Override
    public double get()
    {
        return refSpeedController.get();
    }


    @Override
    public void set(double speed)
    {
        for (SpeedController speedController : speedControllers)
        {
            speedController.set(speed);
        }
    }


    @Override
    public void setInverted(boolean isInverted)
    {
        for (SpeedController speedController : speedControllers)
        {
            speedController.setInverted(isInverted);
        }
    }


    @Override
    public boolean getInverted()
    {
        return refSpeedController.getInverted();
    }


    @Override
    public void disable()
    {
        for (SpeedController speedController : speedControllers)
        {
            speedController.disable();
        }
    }


    @Override
    public void stopMotor()
    {
        for (SpeedController speedController : speedControllers)
        {
            speedController.stopMotor();
        }
    }


    @Override
    public void pidWrite(double output)
    {
        for (SpeedController speedController : speedControllers)
        {
            speedController.pidWrite(output);
        }
    }
}
