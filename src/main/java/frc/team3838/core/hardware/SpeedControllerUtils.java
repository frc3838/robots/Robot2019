package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.meta.API;


@API
public final class SpeedControllerUtils
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(SpeedControllerUtils.class);


    private SpeedControllerUtils() { }



}
