package frc.team3838.core.hardware;

import edu.wpi.first.wpilibj.DigitalInput;
import frc.team3838.core.meta.API;



@API
public class LimitSwitch
{

    private final DigitalInput digitalInput;
    private final boolean inverse;


    @API
    public static LimitSwitch createNormal(int dio)
    {
        return new LimitSwitch(dio, false);
    }

    @API
    public static LimitSwitch createInverted(int dio)
    {
        return new LimitSwitch(dio, true);
    }

    private LimitSwitch(int dio, boolean inverse)
    {
        if (dio < 0)
        {
            throw new IllegalArgumentException("dio specified is invalid as it is less than zero. It was: " + dio);
        }
        this.digitalInput = new DigitalInput(dio);
        this.inverse = inverse;
    }


    @API
    public boolean isActivated()
    {
        return inverse != digitalInput.get();
    }
}
