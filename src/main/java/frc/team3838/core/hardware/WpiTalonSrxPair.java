package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



@API
public class WpiTalonSrxPair extends AbstractCanBusMotorPair<WPI_TalonSRX> implements SpeedController
{
    public WpiTalonSrxPair(int masterDeviceNumber,
                           int followerDeviceNumber,
                           boolean invertMaster,
                           @Nonnull InvertType followerInvertType)
    {
        super(masterDeviceNumber, followerDeviceNumber, invertMaster, followerInvertType);
    }
    
    @Nonnull
    @Override
    protected WPI_TalonSRX createCanController(int deviceNumber)
    {
        return new WPI_TalonSRX(deviceNumber);
    }
}
