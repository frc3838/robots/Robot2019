package frc.team3838.core.hardware;

import javax.annotation.concurrent.Immutable;

import frc.team3838.core.meta.API;



@Immutable
@API
public class EncoderConfig
{
    private final int channelA;
    private final int channelB;
    private final double distancePerPulse;
    private final boolean inverse;


    /**
     *
     * @param channelA Digital Input Channel A
     * @param channelB Digital Input Channel B
     * @param distancePerPulse inches per pulse
     * @param inverse if the reading should be inverted
     */
    public EncoderConfig(int channelA, int channelB, double distancePerPulse, boolean inverse)
    {
        this.channelA = channelA;
        this.channelB = channelB;
        this.distancePerPulse = distancePerPulse;
        this.inverse = inverse;
    }

    @API
    public int getChannelA()
    {
        return channelA;
    }


    @API
    public int getChannelB()
    {
        return channelB;
    }


    @API
    public boolean isInverted()
    {
        return inverse;
    }


    /**
     * The inches per pulse.
     * @return the inches per pulse
     */
    @API
    public double getDistancePerPulse()
    {
        return distancePerPulse;
    }
}
