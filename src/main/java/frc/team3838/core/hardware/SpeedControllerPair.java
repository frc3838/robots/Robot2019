package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;

import edu.wpi.first.wpilibj.SpeedController;



public interface SpeedControllerPair extends SpeedController 
{
    void setInverted(boolean invertMaster, @Nonnull InvertType followerInvertType);
}
