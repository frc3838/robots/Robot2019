package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



@API
public class NoOpLoggingSpeedController implements SpeedController
{
    private static final Logger logger = LoggerFactory.getLogger(NoOpLoggingSpeedController.class);

    double previousSpeed = 0;

    boolean inverted = false;

    @Override
    public double get() { return 0; }


    @Override
    public void set(double speed)
    {
        if (speed != previousSpeed)
        {
            logger.info("Changing speed to {} from {}. isInverted = {}", speed, previousSpeed, inverted);
        }
    }


    @Override
    public void setInverted(boolean isInverted) { this.inverted = isInverted; }


    @Override
    public boolean getInverted() { return inverted; }


    @Override
    public void disable() { }


    @Override
    public void stopMotor() { set(0); }


    @Override
    public void pidWrite(double output) { set(output); }
}
