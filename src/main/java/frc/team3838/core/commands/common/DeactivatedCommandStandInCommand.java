package frc.team3838.core.commands.common;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



/**
 * A command that is used when a non null command is needed, but the actual command needed
 * cannot be instantiated.
 */
public class DeactivatedCommandStandInCommand extends Abstract3838NoSubsystemsCommand
{
    private final String deactivatedCommandName;
    private final String logMsg;
    private boolean messageLogged = false;
    
    
    public DeactivatedCommandStandInCommand(Command command)
    {
        this(command.getClass());
    }
    
    public DeactivatedCommandStandInCommand(Class<? extends Command> commandClass)
    {
        this(commandClass.getSimpleName());    
    }
    
    public DeactivatedCommandStandInCommand(String deactivatedCommandName)
    {
        this.deactivatedCommandName = deactivatedCommandName;
        this.logMsg = String.format("The \"No Action\" Stand In command execution has been called in place of the %s. "
                                      + "This is likely because the because subsystem the command typically assigned to this action uses has been disabled.",
                                      deactivatedCommandName);
    }
    
   

    @Override
    protected void initializeImpl() throws Exception
    {
        logger.warn("The \"No Action\" Stand In command is being assigned in place of the {}. "
                    + "This is likely because the because subsystem the command typically " 
                    + "assigned to this action uses has been disabled.",
                    deactivatedCommandName);
    }


    @Override
    protected void executeImpl() throws Exception
    {
        if (!messageLogged)
        {
            logger.warn(logMsg);
            messageLogged = true;
        }
        else
        {
            logger.trace(logMsg);
        }
    }
}
