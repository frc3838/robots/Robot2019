package frc.team3838.core.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.CommandGroup;



public abstract class Abstract3838CommandGroup extends CommandGroup
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected boolean isFirstCall = true;
    
    protected boolean hasHadException = false;
    
    
    protected Abstract3838CommandGroup()
    {
        super();
    }


    protected Abstract3838CommandGroup(String name)
    {
        super(name);
    }


    @Override
    protected void initialize()
    {
        hasHadException = false;
        try
        {
            super.initialize();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred when initializing {}. Cause Summary: {}", getName(), e.toString(), e);
            //If something has gone wrong, we do not want the command to execute any longer.
            hasHadException = true;
        }
    }


    @Override
    protected void execute()
    {
        if (isFirstCall)
        {
            logger.info("Executing {}", getClass().getSimpleName());
            isFirstCall = false;
        }
        try
        {
            super.execute();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred when executing {}. Cause Summary: {}", getName(), e.toString(), e);
            //If something has gone wrong, we do not want the command to execute any longer.
            hasHadException = true;
        }
    }


    @Override
    protected boolean isFinished()
    {
        return hasHadException || super.isFinished();
    }
}
