//package frc.team3838.core.commands
//
//import com.google.common.collect.ImmutableSet
//import edu.wpi.first.wpilibj.command.Command
//import edu.wpi.first.wpilibj.command.Subsystem
//import frc.team3838.core.logging.PeriodicLogger
//import frc.team3838.core.meta.DoesNotThrowExceptions
//import frc.team3838.core.subsystems.I3838Subsystem
//import org.slf4j.LoggerFactory
//import java.util.concurrent.TimeUnit
//
//
//abstract class KtAbstract3838Command() : Command {
//    protected val logger = LoggerFactory.getLogger(javaClass)
//
//    protected var hasHadException = false
//
//    protected var initializedSuccessfully = true
//
//    private var disableMessageLogged = false
//    private val disabledPeriodicLogger = PeriodicLogger(logger, 15, TimeUnit.SECONDS)
//
//    private var disabledMessage = String.format("COMMAND NOT AVAILABLE: Not all required subsystems for the command '%s' are enabled, or "
//            + "there was a problem when initializing the command. Therefore the command can not be "
//            + "and will not be executed.",
//            javaClass.simpleName)
//
//
//    protected abstract val requiredSubsystems: Set<I3838Subsystem>
//
//    protected abstract val isFinishedImpl: Boolean
//
//    init {
//        registerRequiredSubsystems()
//    }
//
//    protected constructor() : super() {
//
//    }
//
//
//    protected constructor(name: String) : super(name) {
//        registerRequiredSubsystems()
//    }
//
//
//    protected constructor(timeout: Double) : super(timeout) {
//        registerRequiredSubsystems()
//    }
//
//
//    protected constructor(name: String, timeout: Double) : super(name, timeout) {
//        registerRequiredSubsystems()
//    }
//
//    @Throws(Exception::class)
//    protected abstract fun initializeImpl()
//
//    @Throws(Exception::class)
//    protected abstract fun executeImpl()
//
//    @Throws(Exception::class)
//    protected abstract fun endImpl()
//
//    @Throws(Exception::class)
//    protected fun interruptedImpl() {
//        end()
//    }
//
//
//    @DoesNotThrowExceptions
//    override fun initialize() {
//        hasHadException = false
//        initializedSuccessfully = false
//        disabledMessage = String.format("Not all required subsystems for the command '%s' are enabled, or there was a problem when " + "initializing the command. Therefore the command can not be and will not be executed.",
//                name)
//
//        if (areAllSubsystemsAreEnabled()) {
//            logger.debug("{}.initialize() called", name)
//            try {
//                initializeImpl()
//                initializedSuccessfully = true
//            } catch (e: Exception) {
//                hasHadException = true
//                logger.error("An exception occurred when initializing the command '{}'.", name, e)
//            }
//
//        } else {
//            logger.warn("Not all required subsystems for the command '{}' are enabled. Therefore the command can not be and will not be initialized or executed.",
//                    name)
//        }
//    }
//
//
//    @DoesNotThrowExceptions
//    override fun execute() {
//        super.execute()
//        if (areAllSubsystemsAreEnabled()) {
//            try {
//                executeImpl()
//            } catch (e: Exception) {
//                logger.error("An exception occurred in {}.execute()", name, e)
//                hasHadException = true
//            }
//
//        } else {
//            if (!disableMessageLogged) {
//                disabledPeriodicLogger.info("In execute: {}", disabledMessage)
//                disableMessageLogged = true
//            }
//            disabledPeriodicLogger.debug("In execute: {}", disabledMessage)
//        }
//    }
//
//
//    @DoesNotThrowExceptions
//    override fun isFinished(): Boolean {
//        try {
//            return !initializedSuccessfully || hasHadException || isFinishedImpl
//        } catch (e: Exception) {
//            logger.error("An exception occurred when calling {}.isFinished(). Cause Summary: {}", name, e.toString(), e)
//            hasHadException = true
//            return true
//        }
//
//    }
//
//
//    @DoesNotThrowExceptions
//    override fun end() {
//        // Called once after isFinished returns true
//        // do any clean up or post command work here
//        try {
//            endImpl()
//        } catch (e: Exception) {
//            if (areAllSubsystemsAreEnabled()) {
//                logger.error("An exception occurred when calling {}.end(). Cause Summary: {}", name, e.toString(), e)
//            }
//        }
//
//    }
//
//
//    @DoesNotThrowExceptions
//    override fun interrupted() {
//        try {
//            logger.debug("interrupted() called for command {}", name)
//            interruptedImpl()
//        } catch (e: Exception) {
//            if (areAllSubsystemsAreEnabled()) {
//                logger.error("An exception occurred when calling {}.end(). Cause Summary: {}", name, e.toString(), e)
//                hasHadException = true
//            }
//        }
//
//    }
//
//
//    private fun registerRequiredSubsystems() {
//        for (subsystem in requiredSubsystems) {
//            if (subsystem != null) {
//                requires(subsystem as Subsystem)
//            }
//        }
//    }
//
//
//    protected fun areAllSubsystemsAreEnabled(): Boolean {
//        for (subsystem in requiredSubsystems) {
//            if (!subsystem.isEnabled) {
//                return false
//            }
//        }
//        //All subsystems are enabled. So as long as we are initialized properly, we are good to go.
//
//        //TODO - Need to troubleshot the original code - initializedSuccessfully was always false
//        return true // initializedSuccessfully && !hasHadException;
//    }
//
//    companion object {
//
//        protected val NO_SUBSYSTEMS: Set<I3838Subsystem> = ImmutableSet.of()
//    }
//
//
//}
