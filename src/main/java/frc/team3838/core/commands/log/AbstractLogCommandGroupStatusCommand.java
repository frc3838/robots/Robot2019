package frc.team3838.core.commands.log;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



public abstract class AbstractLogCommandGroupStatusCommand extends Abstract3838NoSubsystemsCommand
{

    private final Command commandToLog;

    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    private LocalTime statusTime;


    protected AbstractLogCommandGroupStatusCommand(Command commandToLog)
    {
        this.commandToLog = commandToLog;
    }


    protected abstract String getStatus();


    @Override
    protected void initializeImpl() throws Exception { statusTime = LocalTime.now(); }


    @Override
    protected void executeImpl() throws Exception
    {
        logger.info("~~~~~ {} {} command at {}{}",
                    getStatus(),
                    commandToLog.getName(),
                    timeFormatter.format(statusTime),
                    getAdditionalMessage());
    }


    protected String getAdditionalMessage()
    {
        return "";
    }


    public LocalTime getStatusTime()
    {
        return statusTime;
    }
}
