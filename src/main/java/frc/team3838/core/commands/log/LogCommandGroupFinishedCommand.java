package frc.team3838.core.commands.log;

import java.time.Duration;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import edu.wpi.first.wpilibj.command.Command;



public class LogCommandGroupFinishedCommand extends AbstractLogCommandGroupStatusCommand
{

    @Nullable
    final AbstractLogCommandGroupStatusCommand startStatusLogger;


    @SuppressWarnings("unused")
    public LogCommandGroupFinishedCommand(Command commandToLog)
    {
        super(commandToLog);
        this.startStatusLogger = null;
    }


    @SuppressWarnings("unused")
    public LogCommandGroupFinishedCommand(@Nonnull Command commandToLog, @Nonnull AbstractLogCommandGroupStatusCommand startStatusLogger)
    {
        super(commandToLog);
        this.startStatusLogger = startStatusLogger;
    }


    @Override
    protected String getStatus() { return "Finished"; }


    @Override
    protected String getAdditionalMessage()
    {
        if (startStatusLogger != null)
        {
            final Duration duration = Duration.between(startStatusLogger.getStatusTime(), getStatusTime());
            return " a duration of " + duration;
        }
        else
        {
            return super.getAdditionalMessage();
        }
    }
}
