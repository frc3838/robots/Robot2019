package frc.team3838.core.commands

import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem

/**
 * A command that simply calls the provided action on each loop, and never returns `true` for isFinished.
 */
@API
class PollingCommand(private val theRequiredSubsystems: Set<I3838Subsystem>, val action: () -> Unit) : Abstract3838Command()
{
    override fun getRequiredSubsystems(): Set<I3838Subsystem> = theRequiredSubsystems

    override fun initializeImpl() = action.invoke()

    override fun executeImpl() = action.invoke()
    
    override fun isFinishedImpl(): Boolean = false

    override fun endImpl() { /* No Op */ }

}
