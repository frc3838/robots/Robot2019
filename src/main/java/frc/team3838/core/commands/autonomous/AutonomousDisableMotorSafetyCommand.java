package frc.team3838.core.commands.autonomous;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.logging.PeriodicLogger;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem;



public class AutonomousDisableMotorSafetyCommand extends Abstract3838Command
{
    private static final Logger logger = LoggerFactory.getLogger(AutonomousDisableMotorSafetyCommand.class);
    private static final PeriodicLogger periodicLogger = new PeriodicLogger(logger, new TimeSetting(500, TimeUnit.SECONDS));

    @Nullable
    private final I3838DriveTrainSubsystem driveTrainSubsystem;


    public AutonomousDisableMotorSafetyCommand()
    {
        driveTrainSubsystem = Subsystems.getDriveTrainSubsystem();
    }



    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        if (driveTrainSubsystem != null)
        {
            return ImmutableSet.of(driveTrainSubsystem);
        }
        else
        {
            if (Subsystems.haveSubsystemsBeenInitialized())
            {
                periodicLogger.warn("No DriveTrain subsystem found. AutonomousDisableMotorSafetyCommand will not do anything.");
            }
            return NO_SUBSYSTEMS;
        }
    }


    @Override
    protected void initializeImpl() throws Exception { /* No op */ }


    @Override
    protected void executeImpl() throws Exception
    {
        if (driveTrainSubsystem != null)
        {
            driveTrainSubsystem.disableMotorSafetyForAutonomousMode();
        }
    }


    @Override
    protected boolean isFinishedImpl() throws Exception { return true; }


    @Override
    protected void endImpl() throws Exception { /* No op */ }
}
