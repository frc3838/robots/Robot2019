package frc.team3838.core.commands.autonomous;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.commands.common.NoOpCommand;
import frc.team3838.core.meta.API;



@API
public class AllianceAwareAutonomousCommandConfiguration implements AutonomousCommandConfiguration
{
    private static final Logger logger = LoggerFactory.getLogger(AllianceAwareAutonomousCommandConfiguration.class);


    @Nonnull
    private final Command blueCommand;
    @Nonnull
    private final Command redCommand;
    @Nonnull
    private final String description;


    public AllianceAwareAutonomousCommandConfiguration(@Nonnull String description, @Nonnull Command blueCommand, @Nonnull Command redCommand)
    {
        this.description = description;
        this.blueCommand = blueCommand;
        this.redCommand = redCommand;
    }


    @Nonnull
    @Override
    public Command getCommand()
    {
        final Alliance alliance = DriverStation.getInstance().getAlliance();
        switch (alliance)
        {
            case Blue:
                return blueCommand;
            case Red:
                return redCommand;
            case Invalid:
                String msg = "DriverStation is reporting current alliance as 'Invalid'. Cannot return proper command. Will return NoOpCommand";
                logger.error(msg);
                DriverStation.reportError(msg, false);
                return NoOpCommand.getInstance();
            default:
                msg = "Unhandled alliance value of '" + alliance + "'. Cannot return proper command. Will return NoOpCommand";
                logger.error(msg);
                DriverStation.reportError(msg, false);
                return NoOpCommand.getInstance();
        }
    }


    @Nonnull
    @Override
    public String getDescription()
    {
        return description;
    }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("description", description)
            .toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AllianceAwareAutonomousCommandConfiguration that = (AllianceAwareAutonomousCommandConfiguration) o;

        return new EqualsBuilder()
            .append(blueCommand, that.blueCommand)
            .append(redCommand, that.redCommand)
            .append(description, that.description)
            .isEquals();
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
            .append(blueCommand)
            .append(redCommand)
            .append(description)
            .toHashCode();
    }
}
