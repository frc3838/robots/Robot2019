package frc.team3838.core.commands.autonomous;

import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



public class AutonomousDoNothingCommand extends Abstract3838NoSubsystemsCommand
{

    private static AutonomousDoNothingCommand instance = new AutonomousDoNothingCommand(true);

    public static AutonomousDoNothingCommand getInstance() {return instance; }


    private AutonomousDoNothingCommand(boolean haveLoggedMessage)
    {
        this.haveLoggedMessage = haveLoggedMessage;
    }


    private boolean haveLoggedMessage = false;


    @Override
    protected void initializeImpl() throws Exception { haveLoggedMessage = false; }


    @Override
    protected void executeImpl() throws Exception
    {
        if (!haveLoggedMessage)
        {
            logger.info("DO NOTHING AUTONOMOUS COMMAND RUNNING");
            haveLoggedMessage = true;
        }
    }

    @Override
    protected void endImpl() throws Exception { haveLoggedMessage = false; }
}
