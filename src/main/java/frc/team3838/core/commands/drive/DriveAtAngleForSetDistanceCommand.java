package frc.team3838.core.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.game.subsystems.NavxSubsystem;



public class DriveAtAngleForSetDistanceCommand extends AbstractDriveForSetDistanceCommand
{
    public DriveAtAngleForSetDistanceCommand(double targetAngle, double targetDistanceInInches)
    {
        super(targetAngle, targetDistanceInInches);
    }


    public DriveAtAngleForSetDistanceCommand(RobotDirection robotDirection,
                                             double targetAngle,
                                             double targetDistanceInInches)
    {
        super(robotDirection, targetAngle, targetDistanceInInches);
    }

    public DriveAtAngleForSetDistanceCommand(String name,
                                             RobotDirection robotDirection,
                                             double targetAngle,
                                             double targetDistanceInInches)
    {
        super(name, robotDirection, targetAngle, targetDistanceInInches);
    }


    public DriveAtAngleForSetDistanceCommand(double targetAngle, double targetDistanceInInches, boolean enableTuning)
    {
        super(targetAngle, targetDistanceInInches, enableTuning);
    }


    public DriveAtAngleForSetDistanceCommand(RobotDirection robotDirection,
                                             double targetAngle,
                                             double targetDistanceInInches,
                                             boolean enableTuning)
    {
        super(robotDirection, targetAngle, targetDistanceInInches, enableTuning);
    }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }



}
