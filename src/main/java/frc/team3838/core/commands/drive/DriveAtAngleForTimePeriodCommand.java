package frc.team3838.core.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.commands.drive.AbstractDriveForTimePeriodCommand;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



public class DriveAtAngleForTimePeriodCommand extends AbstractDriveForTimePeriodCommand
{

    public DriveAtAngleForTimePeriodCommand(double targetAngle, double runTimeInMs)
    {
        super(targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(String name, double targetAngle, double runTimeInMs)
    {
        super(name, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(double timeout, double targetAngle, double runTimeInMs)
    {
        super(timeout, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(String name, double timeout, double targetAngle, double runTimeInMs)
    {
        super(name, timeout, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(RobotDirection robotDirection,
                                            double speed,
                                            double targetAngle, double runTimeInMs)
    {
        super(robotDirection, speed, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(String name,
                                            RobotDirection robotDirection,
                                            double speed, double targetAngle, double runTimeInMs)
    {
        super(name, robotDirection, speed, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(double timeout,
                                            RobotDirection robotDirection,
                                            double speed, double targetAngle, double runTimeInMs)
    {
        super(timeout, robotDirection, speed, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(String name,
                                            double timeout,
                                            RobotDirection robotDirection,
                                            double speed,
                                            double targetAngle, double runTimeInMs)
    {
        super(name, timeout, robotDirection, speed, targetAngle, runTimeInMs);
    }


    public DriveAtAngleForTimePeriodCommand(double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(String name, double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(name, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(double timeout, double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(timeout, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(String name, double timeout, double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(name, timeout, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(RobotDirection robotDirection,
                                            double speed,
                                            double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(robotDirection, speed, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(String name,
                                            RobotDirection robotDirection,
                                            double speed, double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(name, robotDirection, speed, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(double timeout,
                                            RobotDirection robotDirection,
                                            double speed, double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(timeout, robotDirection, speed, targetAngle, runTimeInMs, enableTuning);
    }


    public DriveAtAngleForTimePeriodCommand(String name,
                                            double timeout,
                                            RobotDirection robotDirection,
                                            double speed,
                                            double targetAngle, double runTimeInMs, boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, targetAngle, runTimeInMs, enableTuning);
    }


    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return DriveTrainSubsystem.getInstance(); }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }
}
