package frc.team3838.core.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.commands.drive.AbstractDriveStraightForTimePeriodCommand;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



@API
public class DriveStraightForTimePeriodCommand extends AbstractDriveStraightForTimePeriodCommand
{
    public DriveStraightForTimePeriodCommand(double runTimeInMs)
    {
        super(runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             double runTimeInMs)
    {
        super(name, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(double timeout, 
                                             double runTimeInMs)
    {
        super(timeout, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             double timeout, 
                                             double runTimeInMs)
    {
        super(name, timeout, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs)
    {
        super(robotDirection, speed, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs)
    {
        super(name, robotDirection, speed, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(double timeout, 
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs)
    {
        super(timeout, robotDirection, speed, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(String name,
                                             double timeout,
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs)
    {
        super(name, timeout, robotDirection, speed, runTimeInMs);
    }


    public DriveStraightForTimePeriodCommand(double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(name, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(double timeout, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(timeout, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             double timeout, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(name, timeout, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(robotDirection, speed, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(String name, 
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(name, robotDirection, speed, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(double timeout, 
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(timeout, robotDirection, speed, runTimeInMs, enableTuning);
    }


    public DriveStraightForTimePeriodCommand(String name,
                                             double timeout,
                                             RobotDirection robotDirection, 
                                             double speed, 
                                             double runTimeInMs, 
                                             boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, runTimeInMs, enableTuning);
    }


    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return DriveTrainSubsystem.getInstance(); }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }
}
