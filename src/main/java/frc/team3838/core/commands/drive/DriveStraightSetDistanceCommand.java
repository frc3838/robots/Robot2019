package frc.team3838.core.commands.drive;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



public class DriveStraightSetDistanceCommand extends AbstractDriveStraightForSetDistanceCommand
{
    public DriveStraightSetDistanceCommand(double targetDistanceInInches)
    {
        super(targetDistanceInInches);
    }
    public DriveStraightSetDistanceCommand(RobotDirection robotDirection, double targetDistanceInInches)
    {
        super(robotDirection, targetDistanceInInches);
    }

    public DriveStraightSetDistanceCommand(double targetDistanceInInches, TimeSetting timeoutInSeconds)
    {
        super(TimeUnit.MILLISECONDS.toSeconds(timeoutInSeconds.toMillis()), targetDistanceInInches);
    }



    public DriveStraightSetDistanceCommand(double targetDistanceInInches, double speed)
    {
        super(RobotDirection.Forward, speed, targetDistanceInInches);
    }


    public DriveStraightSetDistanceCommand(double targetDistanceInInches, boolean enableTuning)
    {
        super(targetDistanceInInches, enableTuning);
    }

    public DriveStraightSetDistanceCommand(double targetDistanceInInches, double speed, boolean enableTuning)
    {
        super(RobotDirection.Forward, speed, targetDistanceInInches, enableTuning);
    }



    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return DriveTrainSubsystem.getInstance(); }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }

}
