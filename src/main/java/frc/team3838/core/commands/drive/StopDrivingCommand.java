package frc.team3838.core.commands.drive;

import frc.team3838.core.commands.AbstractBasicDriveCommand;
import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem;



public class StopDrivingCommand extends AbstractBasicDriveCommand
{

    public StopDrivingCommand() { }


    public StopDrivingCommand(I3838DriveTrainSubsystem driveTrainSubsystem)
    {
        super(driveTrainSubsystem);
    }


    @Override
    protected void initializeImpl() throws Exception { /* no op */ }


    @Override
    protected void executeImpl() throws Exception
    {
        if (driveTrainSubsystem != null)
        {
            driveTrainSubsystem.stop();
        }
    }


    @Override
    protected boolean isFinishedImpl() throws Exception { return true; }


    @Override
    protected void endImpl() throws Exception
    {

    }
}
