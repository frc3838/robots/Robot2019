package frc.team3838.core.commands.drive;

import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;



public abstract class AbstractDriveStraightForSetDistanceCommand extends AbstractDriveForSetDistanceCommand
{

    public AbstractDriveStraightForSetDistanceCommand(double targetDistanceInInches)
    {
        super(0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double targetDistanceInInches)
    {
        super(name, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(double timeout,
                                                      double targetDistanceInInches)
    {
        super(timeout, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double timeout,
                                                      double targetDistanceInInches)
    {
        super(name, timeout, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches)
    {
        super(robotDirection, speed, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches)
    {
        super(name, robotDirection, speed, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(double timeout,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches)
    {
        super(timeout, robotDirection, speed, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double timeout,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches)
    {
        super(name, timeout, robotDirection, speed, 0.0, targetDistanceInInches);
    }


    public AbstractDriveStraightForSetDistanceCommand(double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(name, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(double timeout,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(timeout, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double timeout,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(name, timeout, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(RobotDirection robotDirection,
                                                      double targetDistanceInInches
                                                      )
    {
        super(robotDirection, 0.0, targetDistanceInInches);
    }

    public AbstractDriveStraightForSetDistanceCommand(RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(robotDirection, speed, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(name, robotDirection, speed, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(double timeout,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(timeout, robotDirection, speed, 0.0, targetDistanceInInches, enableTuning);
    }


    public AbstractDriveStraightForSetDistanceCommand(String name,
                                                      double timeout,
                                                      RobotDirection robotDirection,
                                                      double speed,
                                                      double targetDistanceInInches,
                                                      boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, 0.0, targetDistanceInInches, enableTuning);
    }
}
