package frc.team3838.core.commands.drive;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.logging.PeriodicLogger;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



public class DrivePidRotateCommand extends Abstract3838Command implements PIDOutput
{

    private static final TimeSetting DEFAULT_TIMEOUT = new TimeSetting(4, TimeUnit.SECONDS);
    private static final long MIN_RUN_TIME = 100; // milliseconds

    private static final String P_SDB_KEY = "R-p";
    private static final String I_SDB_KEY = "R-i";
    private static final String D_SDB_KEY = "R-d";

    private final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    private final NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();

    private static final boolean ENABLE_PID_TUNING = false;

    /* The following PID Controller coefficients will need to be tuned */
    /* to match the dynamics of your drive system.  Note that the      */
    /* SmartDashboard in Test mode has support for helping you tune    */
    /* controllers by displaying a form where you can enter new P, I,  */
    /* and D constants and test the mechanism.                         */

    static final double DEFAULT_P = 0.028;
    static final double DEFAULT_I = 0.006;
    static final double DEFAULT_D = 0.07;
    static final double DEFAULT_F = 0.00;

    static double TOLERANCE_DEGREES = 0.4f;

    PIDController turnController;

    private static final double MAX_TURN_SPEED = 0.5;

    /**
     * The angle to rotate, relative to zero at NavxReset time. In other words,
     * this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     * rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     * and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     * start position or last Navx Reset), and you pass in 180° it will rotate 90°
     * (or possibly 270°) to the 180° point. Value must be in the range of
     * -180° to 180
     */
    protected double targetAngle;
    protected double rotateToAngleRate;


    protected long startTime;
    protected long delayMs;
    protected long diff;
    protected PeriodicLogger executeTracePeriodicLogger = new PeriodicLogger(logger, 200, TimeUnit.MILLISECONDS);
    protected PeriodicLogger finishedTracePeriodicLogger = new PeriodicLogger(logger, 200, TimeUnit.MILLISECONDS);



    /**
     * Any subclasses using this constructor must somehow initialize the {@link #targetAngle}.
     */
    @API
    public DrivePidRotateCommand(String name)
    {
        super(name);
        delayMs = DEFAULT_TIMEOUT.toMillis();
    }

    /**
     * Any subclasses using this constructor must somehow initialize the {@link #targetAngle}.
     */
    @API
    public DrivePidRotateCommand()
    {
        super(DrivePidRotateCommand.class.getSimpleName());
        delayMs = DEFAULT_TIMEOUT.toMillis();
    }

    @API
    public DrivePidRotateCommand(int targetAngle)
    {
        this((double) targetAngle, DEFAULT_TIMEOUT);
    }

    @API
    public DrivePidRotateCommand(int targetAngle, TimeSetting timeOut)
    {
        this((double) targetAngle, timeOut);
    }
    @API
    public DrivePidRotateCommand(double targetAngle)
    {
        this(targetAngle, DEFAULT_TIMEOUT);
    }

    @API
    public DrivePidRotateCommand(double targetAngle, TimeSetting timeOut)
    {
        super(DrivePidRotateCommand.class.getSimpleName() + '_' + targetAngle);
        delayMs = timeOut.toMillis();

        if (ENABLE_PID_TUNING)
        {
            boolean isSet = false;

            try
            {
                final double p = SmartDashboard.getNumber(P_SDB_KEY, -1);
                isSet = p > 0;
            }
            catch (Exception ignore) { }
            if (!isSet)
            {

                logger.info("Writing PID valuers to SDB");
                SmartDashboard.putNumber(P_SDB_KEY, DEFAULT_P);
                SmartDashboard.putNumber(I_SDB_KEY, DEFAULT_I);
                SmartDashboard.putNumber("R-d", DEFAULT_D);
            }
        }

        if ((targetAngle > 180) || (targetAngle < -180))
        {
            throw new IllegalArgumentException("'targetAngle' must be between -180.0 and 180.0");
        }

        if (targetAngle == 180)
        {
            targetAngle = 179.9;
        }
        else if (targetAngle == -180)
        {
            targetAngle = -179.9;
        }
        this.targetAngle = targetAngle;
    }


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return ImmutableSet.of(DriveTrainSubsystem.getInstance(), NavxSubsystem.getInstance());
    }


    /**
     * The initialize method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute
     * command is called repeated until isFinished returns true,
     * or interrupted is called by the command scheduler/runner.
     * After isFinished returns true, end is called one time
     * in order to do any cleanup or set and values.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    protected void initializeImpl() throws Exception
    {
        final AHRS navx = navxSubsystem.getNavx();
        if (navx != null)
        {

            logger.info("Initializing {} with target angle of {}.", getName(), targetAngle);
            if (targetAngle == 0)
            {
                logger.warn("{} called with an target angle of zero. NO ROTATION WILL OCCUR.", getName());
            }


            logger.info("shouldResetNavxReturned was '{}' for command {}", shouldResetNavX(), getName());
            if (shouldResetNavX())
            {

                navx.reset();
            }

            logger.info("For command {}, current angle is {} for target angle of {} (with angle Adjustment of {}). shouldReset =  {}",
                        getName(), navx.getAngle(), targetAngle, navx.getAngleAdjustment(), shouldResetNavX());

            final double p = ENABLE_PID_TUNING ? SmartDashboard.getNumber(P_SDB_KEY, 0.03) : DEFAULT_P;
            final double i = ENABLE_PID_TUNING ? SmartDashboard.getNumber(I_SDB_KEY, 0.00) : DEFAULT_I;
            final double d = ENABLE_PID_TUNING ? SmartDashboard.getNumber(D_SDB_KEY, 0.00) : DEFAULT_D;

            turnController = new PIDController(p, i, d, DEFAULT_F, navx, this);
            turnController.setInputRange(-180.0f, 180.0f);
            turnController.setOutputRange(-1.0, 1.0);
            turnController.setAbsoluteTolerance(TOLERANCE_DEGREES);
            turnController.setContinuous(true);
            turnController.enable();
            driveTrainSubsystem.enableMotorSafetyPostAutonomousMode();
            driveTrainSubsystem.setExpiration(0.1);
            Timer.delay(0.1);
            logger.info("{} initialization completed. Target angle = {}. Current Angle = {}; p = {}; i = {}; d = {}",
                        getName(),
                        targetAngle,
                        navx.getAngle(),
                        MathUtils.formatNumber(p, 4),
                        MathUtils.formatNumber(i, 4),
                        MathUtils.formatNumber(d, 4));
        }
        startTime = System.currentTimeMillis();
    }

    protected boolean shouldResetNavX() { return true;}

    /**
     * The execute method is called repeatedly when this Command is
     * scheduled to run until. It is called repeatedly until
     * the either isFinish returns true, interrupted is called,
     * or the command is canceled. Note that the initialize
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @Override
    protected void executeImpl() throws Exception
    {
        if (isFinished())
        {
            logger.debug("In execute, isFinished return true");
            end();
        }
        else
        {
            turnController.setSetpoint(targetAngle);

            final boolean wasNegative = MathUtils.isNegative(rotateToAngleRate);
            // outputRangeMin is the approximate stall speed when rotating
            double scaledAngle = MathUtils.scaleRange(Math.abs(rotateToAngleRate), 0, 1, 0.05, 1);
            scaledAngle = wasNegative ? -scaledAngle : scaledAngle;

            driveTrainSubsystem.driveRobotViaArcadeControlRaw(scaledAngle, 0);

            Timer.delay(0.005); // wait for a motor update time

            // Timeout code
            final long currentTime = System.currentTimeMillis();
            diff = currentTime - startTime;


            if (executeTracePeriodicLogger.isTraceEnabled() && (navxSubsystem.getNavx() != null))
            {
                executeTracePeriodicLogger.trace("    In execute for {}, diff = {}, target angle = {}, current angle = {}, rotateToAngleRate = {}",
                                                 getName(), diff, targetAngle, navxSubsystem.getNavx().getAngle(), rotateToAngleRate);
            }
        }
    }

    protected boolean isInternalTimedOut()
    {
        return diff >= delayMs;
    }

    /**
     * <p>
     * Returns whether this command is finished. If it is, then the command will be removed and
     * {@link #end()} will be called.
     * </p><p>
     * It may be useful for a team to reference the {@link #isTimedOut()}
     * method for time-sensitive commands.
     * </p><p>
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning true will result in the
     * command executing once and finishing immediately. It is recommended to use
     * {@link edu.wpi.first.wpilibj.command.InstantCommand} (added in 2017) for this.
     * </p>
     *
     * @return whether this command is finished.
     *
     * @see #isTimedOut() isTimedOut()
     */
    @SuppressWarnings("RedundantThrows")
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        if ((turnController == null) || (navxSubsystem.getNavx() == null))
        {
            return true;
        }
        else
        {
            final long runTime = System.currentTimeMillis() - startTime;
            final boolean hasRunMinTime = runTime > MIN_RUN_TIME;

            boolean onTarget = turnController.onTarget();

            if (onTarget)
            {
                //Make sure we are on target and staying there over a couple of data points
                try {TimeUnit.MILLISECONDS.sleep(50);} catch (InterruptedException ignore) {}
                onTarget = turnController.onTarget();
            }

            final boolean timedOut = isInternalTimedOut();

            final boolean done = hasRunMinTime && (onTarget || timedOut);

            finishedTracePeriodicLogger.trace("in isFinish: done = {}   based on: hasRunMinTime = {} && ( onTarget = {} || timedOut = {} )", done, hasRunMinTime, onTarget, timedOut);

            if (done)
            {
                driveTrainSubsystem.stop();
            }
            return done;
        }
    }


    /**
     * Called once when the command ended peacefully; that is it is called once
     * after {@link #isFinished()} returns true. This is where you may want to
     * wrap up loose ends, like shutting off a motor that was being used in the
     * command.
     */
    @SuppressWarnings("RedundantThrows")
    protected void endImpl() throws Exception
    {
        final AHRS navx = navxSubsystem.getNavx();
        double angle = (navx != null) ? navx.getAngle() : -9999;

        logger.info("At command completion {} angle = {}, timedOut = {};   Used: p: {}; i: {}; d: {}",
                    getName(),
                    angle,
                    isInternalTimedOut(),
                    MathUtils.formatNumber(turnController.getP(), 4),
                    MathUtils.formatNumber(turnController.getI(), 4),
                    MathUtils.formatNumber(turnController.getD(), 4));

        driveTrainSubsystem.stop();
        driveTrainSubsystem.disableMotorSafetyForAutonomousMode();
        driveTrainSubsystem.setExpiration(10);
        if (turnController != null)
        {
            turnController.disable();
        }
    }


    /**
     * <p>
     * Called when the command ends because somebody called {@link #cancel()} or
     * another command shared the same requirements as this one, and booted it out. For example,
     * it is called when another command which requires one or more of the same
     * subsystems is scheduled to run.
     * </p><p>
     * This is where you may want to wrap up loose ends, like shutting off a motor that was being
     * used in the command.
     * </p><p>
     * Generally, it is useful to simply call the {@link #end()} method within this
     * method, as done here.
     * </p>
     */
    @SuppressWarnings("RedundantMethodOverride")
    protected void interruptedImpl() throws Exception
    {
        //Super calls end() which works for this command
        super.interruptedImpl();
    }


    @Override
    public void pidWrite(double output)
    {
        /* This method is invoked periodically by the PID Controller, */
        /* based upon navX MXP yaw angle input and PID Coefficients.    */

        final boolean isNegative = MathUtils.isNegative(output);
        if (Math.abs(output) > MAX_TURN_SPEED)
        {
            output = MAX_TURN_SPEED;
            if (isNegative)
            {
                output = -output;
            }
        }
        this.rotateToAngleRate = output;
    }
}
