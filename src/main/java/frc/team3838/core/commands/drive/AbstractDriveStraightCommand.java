package frc.team3838.core.commands.drive;

import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;



public abstract class AbstractDriveStraightCommand extends AbstractDriveCommand
{
    @API
    protected AbstractDriveStraightCommand()
    {
        super(0.0);
    }


    @API
    protected AbstractDriveStraightCommand(String name)
    {
        super(name, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(double timeout)
    {
        super(timeout, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout)
    {
        super(name, timeout, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(RobotDirection robotDirection, double speed)
    {
        super(robotDirection, speed, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(String name, RobotDirection robotDirection, double speed)
    {
        super(name, robotDirection, speed, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, RobotDirection robotDirection, double speed)
    {
        super(timeout, robotDirection, speed, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, RobotDirection robotDirection, double speed)
    {
        super(name, timeout, robotDirection, speed, 0.0);
    }


    @API
    protected AbstractDriveStraightCommand(boolean enableTuning)
    {
        super(0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(String name, boolean enableTuning)
    {
        super(name, 0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, boolean enableTuning)
    {
        super(timeout, 0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, boolean enableTuning)
    {
        super(name, timeout, 0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(robotDirection, speed,0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(String name, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(name, robotDirection, speed, 0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(timeout, robotDirection, speed, 0.0, enableTuning);
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, 0.0, enableTuning);
    }
}
