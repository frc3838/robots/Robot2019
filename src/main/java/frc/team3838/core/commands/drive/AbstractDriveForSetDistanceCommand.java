package frc.team3838.core.commands.drive;


import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.core.utils.MathUtils;



public abstract class AbstractDriveForSetDistanceCommand extends AbstractDriveCommand
{
    protected final double targetDistanceInInches;
    protected double endDistanceInInches;

    protected double lastLogSpeed = -1;


    protected AbstractDriveForSetDistanceCommand(double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }

    protected AbstractDriveForSetDistanceCommand(RobotDirection robotDirection,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(robotDirection, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }

    protected AbstractDriveForSetDistanceCommand(String name,
                                                 RobotDirection robotDirection,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(robotDirection, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(name, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(double timeout,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(timeout, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double timeout,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(name, timeout, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(robotDirection, speed, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(name, robotDirection, speed, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(double timeout,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(timeout, robotDirection, speed, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double timeout,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches)
    {
        super(name, timeout, robotDirection, speed, targetAngle);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(RobotDirection robotDirection,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(robotDirection, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }

    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(name, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(double timeout,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(timeout, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double timeout,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(name, timeout, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(robotDirection, speed, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(name, robotDirection, speed, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(double timeout,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(timeout, robotDirection, speed, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    protected AbstractDriveForSetDistanceCommand(String name,
                                                 double timeout,
                                                 RobotDirection robotDirection,
                                                 double speed,
                                                 double targetAngle,
                                                 double targetDistanceInInches,
                                                 boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, targetAngle, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    /**
     * The initialize method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute
     * command is called repeated until isFinished returns true,
     * or interrupted is called by the command scheduler/runner.
     * After isFinished returns true, end is called one time
     * in order to do any cleanup or set and values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        getDriveTrainSubsystem().resetEncoders();
        super.initializeImpl();
        // We initially set this here, but reset it in executePreFirstCall
        endDistanceInInches = calculateEndDistanceInInches();
        logger.info("    {} with a target distance of  {}", getName(), targetDistanceInInches);
        logger.info("    {} with an *INITIAL* end distance of {}", getName(), endDistanceInInches);
        logger.info("    {} with an target angle of {}", getName(), targetAngle);
        logger.info("    {} Encoder readings at Start: Left = {}; Right = {}",
                    getName(),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getLeftDistanceInInches(), 3),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getRightDistanceInInches(), 3));
    }


    protected double calculateEndDistanceInInches()
    {
        double distance = getDriveTrainSubsystem().getAverageDistanceInInches() + targetDistanceInInches;
        if (robotDirection == RobotDirection.Reverse)
        {
            distance = -distance;
        }
        return distance;
    }

//
//    @SuppressWarnings("RedundantThrows")
//    @Override
//    protected void executePreFirstCall() throws Exception
//    {
//        super.executePreFirstCall();
//        endDistanceInInches = calculateEndDistanceInInches();
//        logger.info("{} in executePreFirstCall() set end distance to {}", getName(), endDistanceInInches);
//    }


    /**
     * The execute method is called repeatedly when this Command is
     * scheduled to run until. It is called repeatedly until
     * the either isFinish returns true, interrupted is called,
     * or the command is canceled. Note that the initialize
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected void executeImpl() throws Exception
    {
        final double remainingDistance = calculateRemainingDistance();

        //SmartDashboard.putString("Remaining Distance", MathUtils.formatNumber(remainingDistance, 3));

        double theSpeed = speed;

        if (remainingDistance > 24)
        {
            theSpeed = speed;
        }
        else if (remainingDistance > 18)
        {
            theSpeed = speed * 0.75;
        }
        else if (remainingDistance > 12)
        {
            theSpeed = speed * 0.50;
        }
        else if (remainingDistance > 8)
        {
            theSpeed = speed * 0.25;
        }
        else if (remainingDistance < 3)
        {
            theSpeed = speed * 0.10;
        }
        else if (remainingDistance <= 0)
        {
            theSpeed = 0;
        }

        if (theSpeed > 0 && theSpeed < getMinSpeed())
        {
            theSpeed = getMinSpeed();
        }

        if (logger.isInfoEnabled() && (lastLogSpeed != theSpeed))
        {
            logger.debug("Speed set to {}%", MathUtils.formatNumber(theSpeed, 3));
            lastLogSpeed = theSpeed;
        }
        driveStraight();
    }


    private double calculateRemainingDistance()
    {
        return endDistanceInInches - getDriveTrainSubsystem().getAverageDistanceInInches();
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected boolean shouldApplyCorrection()
    {
        final double remainingDistance = calculateRemainingDistance();
        if (robotDirection == RobotDirection.Reverse)
        {
            return !(-remainingDistance < 0.5);
        }
        else
        {

            // Don't correct for the last 1/2 inch
            return !(remainingDistance < 0.5);
        }

    }


    @SuppressWarnings({"MethodDoesntCallSuperMethod", "RedundantThrows"})
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return (robotDirection == RobotDirection.Forward)
               ? (getDriveTrainSubsystem().getAverageDistanceInInches() >= endDistanceInInches)
               : (getDriveTrainSubsystem().getAverageDistanceInInches() <= endDistanceInInches);
    }


    protected double getMaxSpeed()
    {
        return (Subsystems.getDriveTrainSubsystem() != null) ? Subsystems.getDriveTrainSubsystem().getMaxSpeedForAutonomous() : 0.0;
    }


    protected double getMinSpeed()
    {
        return (Subsystems.getDriveTrainSubsystem() != null) ? Subsystems.getDriveTrainSubsystem().getMinSpeedForAutonomous() : 0.0;
    }

}
