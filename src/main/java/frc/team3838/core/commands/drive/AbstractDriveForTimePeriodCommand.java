package frc.team3838.core.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;


@API
public abstract class AbstractDriveForTimePeriodCommand extends AbstractDriveCommand
{
    private final double runTimeInMs;
    private double startTime;


    @API
    protected AbstractDriveForTimePeriodCommand(double targetAngle,
                                                double runTimeInMs)
    {
        super(targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(name, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(double timeout,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(timeout, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double timeout,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(name, timeout, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(robotDirection, speed, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(name, robotDirection, speed, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(double timeout,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(timeout, robotDirection, speed, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double timeout,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs)
    {
        super(name, timeout, robotDirection, speed, targetAngle);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(name, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(double timeout,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(timeout, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double timeout,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(name, timeout, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(robotDirection, speed, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(name, robotDirection, speed, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(double timeout,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(timeout, robotDirection, speed, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @API
    protected AbstractDriveForTimePeriodCommand(String name,
                                                double timeout,
                                                RobotDirection robotDirection,
                                                double speed,
                                                double targetAngle,
                                                double runTimeInMs,
                                                boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, targetAngle, enableTuning);
        this.runTimeInMs = runTimeInMs;
    }


    @Override
    protected void initializeImpl() throws Exception
    {
        startTime = System.currentTimeMillis();
        super.initializeImpl();
        logger.info("    {} with a run time of  {}ms", getName(), runTimeInMs);
    }


    @SuppressWarnings({"MethodDoesntCallSuperMethod", "RedundantThrows"})
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return System.currentTimeMillis() > (startTime + runTimeInMs);
    }


    @Nonnull
    @Override
    protected abstract Abstract3838DriveTrainSubsystem getDriveTrainSubsystem();

    @Nonnull
    @Override
    protected abstract Abstract3838NavxSubsystem getNavxSubsystem();

}
