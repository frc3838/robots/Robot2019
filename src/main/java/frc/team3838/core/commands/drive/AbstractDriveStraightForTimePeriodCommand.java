package frc.team3838.core.commands.drive;

import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;



public abstract class AbstractDriveStraightForTimePeriodCommand extends AbstractDriveForTimePeriodCommand
{
    public AbstractDriveStraightForTimePeriodCommand( double runTimeInMs)
    {
        super(0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name,  
                                                     double runTimeInMs)
    {
        super(name, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(double timeout,  
                                                     double runTimeInMs)
    {
        super(timeout, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name, 
                                                     double timeout,  
                                                     double runTimeInMs)
    {
        super(name, timeout, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(RobotDirection robotDirection, 
                                                     double speed,  
                                                     double runTimeInMs)
    {
        super(robotDirection, speed, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name, 
                                                     RobotDirection robotDirection, 
                                                     double speed,  
                                                     double runTimeInMs)
    {
        super(name, robotDirection, speed, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(double timeout,
                                                     RobotDirection robotDirection, 
                                                     double speed,  
                                                     double runTimeInMs)
    {
        super(timeout, robotDirection, speed, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name,
                                                     double timeout,
                                                     RobotDirection robotDirection, 
                                                     double speed,  
                                                     double runTimeInMs)
    {
        super(name, timeout, robotDirection, speed, 0.0, runTimeInMs);
    }


    public AbstractDriveStraightForTimePeriodCommand(double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(name, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(double timeout, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(timeout, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name, 
                                                     double timeout, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(name, timeout, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(RobotDirection robotDirection, 
                                                     double speed, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(robotDirection, speed, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name,
                                                     RobotDirection robotDirection,
                                                     double speed,
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(name, robotDirection, speed, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(double timeout,
                                                     RobotDirection robotDirection, 
                                                     double speed, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(timeout, robotDirection, speed, 0.0, runTimeInMs, enableTuning);
    }


    public AbstractDriveStraightForTimePeriodCommand(String name,
                                                     double timeout,
                                                     RobotDirection robotDirection, 
                                                     double speed, 
                                                     double runTimeInMs, 
                                                     boolean enableTuning)
    {
        super(name, timeout, robotDirection, speed, 0.0, runTimeInMs, enableTuning);
    }
}
