package frc.team3838.core.commands.drive;

import frc.team3838.core.config.time.TimeSetting;



public class DrivePidRotateNoResetCommand extends DrivePidRotateCommand
{
    public DrivePidRotateNoResetCommand(int targetAngle)
    {
        super(targetAngle);
    }


    public DrivePidRotateNoResetCommand(double targetAngle)
    {
        super(targetAngle);
    }


    public DrivePidRotateNoResetCommand(int targetAngle, TimeSetting timeOut)
    {
        super(targetAngle, timeOut);
    }


    public DrivePidRotateNoResetCommand(double targetAngle, TimeSetting timeOut)
    {
        super(targetAngle, timeOut);
    }


    @Override
    protected boolean shouldResetNavX()
    {
        return false;
    }
}
