package frc.team3838.core.controls;

public class NoOpAxisPair implements AxisPair
{
    @Override
    public double getXorLeft() { return 0; }


    @Override
    public double getYorRight() { return 0; }
}
