package frc.team3838.core.controls;

/**
 * <p>
 *     A simple interface to wrap a Human Interface Device (HID) axis for use as a throttle. It 
 *     allows the actual HID, such as a Joystick or GamePad, to be abstracted away from its use 
 *     as a throttle. This allows for easier configuration changes, especially is the Throttle is 
 *     used in multiple locations within the code since only the definition need to change to change 
 *     which HID axis is being used.
 * </p>
 * <p>
 *     This interface does not have nay concrete implementations. Instead, use the {@link ThrottleBuilder}
 *     to build an instance. For example:
 * </p>
 */
public interface Throttle 
{
    double get();
}
