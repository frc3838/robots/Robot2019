package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.SendableBase;
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder;



public abstract class AbstractAxisPairBase extends SendableBase implements AxisPair,
                                                                           Sendable
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractAxisPairBase.class);


    /**
     * Creates an instance of the sensor base.
     */
    protected AbstractAxisPairBase()
    {
        super();
        setName(getClass().getSimpleName());
    }


    /**
     * Creates an instance of the sensor base.
     *
     * @param addLiveWindow if true, add this Sendable to LiveWindow
     */
    protected AbstractAxisPairBase(boolean addLiveWindow)
    {
        super(addLiveWindow);
        setName(getClass().getSimpleName());
    }


    /**
     * Initializes this {@link Sendable} object.
     *
     * @param builder sendable builder
     */
    @Override
    public void initSendable(SendableBuilder builder)
    {


    }
}
