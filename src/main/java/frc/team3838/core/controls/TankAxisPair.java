package frc.team3838.core.controls;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



/**
 * Also see {@link GamepadTankAxisPair}.
 *
 * @see GamepadTankAxisPair
 */
@API
public class TankAxisPair implements AxisPair
{
    public static final int USE_Y_AXIS = -1;

    @Nonnull
    private final GenericHID leftHid;
    @Nonnull
    private final GenericHID rightHid;

    private final int leftRawAxis;
    private final int rightRawAxis;


    /**
     * Creates a {@code TankAxisPair} from two joysticks, using the 'Y' axis
     * (via {@link GenericHID#getY()}) for each Joystick. For creating an TankAxisPair
     * from a single gamepad, see  {@link GamepadTankAxisPair}.
     *
     * @param leftHid  the left Human Interface Device (HID), i.e. joystick
     * @param rightHid the right Human Interface Device (HID), i.e. joystick
     *
     * @see GamepadTankAxisPair
     */
    @API
    public TankAxisPair(@Nonnull GenericHID leftHid, @Nonnull GenericHID rightHid)
    {
        this.leftHid = leftHid;
        this.rightHid = rightHid;
        //noinspection SuspiciousNameCombination
        this.leftRawAxis = USE_Y_AXIS;
        //noinspection SuspiciousNameCombination
        this.rightRawAxis = USE_Y_AXIS;
    }


    /**
     * Creates a {@code TankAxisPair} from two joysticks, using the specified axes
     * for each Joystick. For creating an TankAxisPair
     * from a single gamepad, see  {@link GamepadTankAxisPair}.
     *
     * @param leftHid      the left Human Interface Device (HID), i.e. joystick
     * @param leftRawAxis  the raw axis to use from the left Joystick
     * @param rightHid     the right Human Interface Device (HID), i.e. joystick
     * @param rightRawAxis the raw axis to use from the right Joystick
     *
     * @see GamepadTankAxisPair
     */
    @API
    public TankAxisPair(@Nonnull GenericHID leftHid, int leftRawAxis, @Nonnull GenericHID rightHid, int rightRawAxis)
    {
        this.leftHid = leftHid;
        this.rightHid = rightHid;
        this.leftRawAxis = leftRawAxis;
        this.rightRawAxis = rightRawAxis;
    }


    @Override
    public double getXorLeft()
    {
        return (leftRawAxis == USE_Y_AXIS) ? leftHid.getY() : leftHid.getRawAxis(leftRawAxis);
    }


    @Override
    public double getYorRight()
    {
        return (rightRawAxis == USE_Y_AXIS) ? rightHid.getY() : rightHid.getRawAxis(rightRawAxis);
    }
}
