package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public class GamePadDualTriggerThrottle implements Throttle
{
    private final GenericHID hid;
    private final int leftAxis;
    private final int rightAxis;
    private final GamepadSide negativeGamepadSide;


    public GamePadDualTriggerThrottle(GenericHID genericHID, GamepadSide negativeGamepadSide)
    {
        this(genericHID, 2, 3, negativeGamepadSide);
    }



    public GamePadDualTriggerThrottle(GenericHID hid, int leftAxis, int rightAxis, GamepadSide negativeGamepadSide)
    {
        this.hid = hid;
        this.leftAxis = leftAxis;
        this.rightAxis = rightAxis;
        this.negativeGamepadSide = negativeGamepadSide;
    }


    @Override
    public double get()
    {
        final double leftValue = hid.getRawAxis(leftAxis);
        final double rightValue = hid.getRawAxis(rightAxis);

        if (leftValue > 0.01 && rightValue > 0.01)
        {
            // both are active
            return 0;
        }
        else if (leftValue > 0.01)
        {
            return (negativeGamepadSide == GamepadSide.Left) ? -leftValue : leftValue;
        }
        else if (rightValue > 0.01)
        {
            return (negativeGamepadSide == GamepadSide.Right) ? -rightValue : rightValue;
        }
        else
        {
            return 0;
        }
    }
}
