package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.buttons.Button;



public class DualComboButton extends Button
{
    private static final Logger logger = LoggerFactory.getLogger(DualComboButton.class);


    private final Button buttonA;
    private final Button buttonB;


    public DualComboButton(Button buttonA, Button buttonB)
    {
        this.buttonA = buttonA;
        this.buttonB = buttonB;
    }


    @Override
    public boolean get()
    {
        return buttonA.get() && buttonB.get();
    }
}
