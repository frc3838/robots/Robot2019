package frc.team3838.core.controls;

public interface AxisPair //extends Sendable
{
    double getXorLeft();
    double getYorRight();
}
