package frc.team3838.core.controls;

import frc.team3838.core.meta.API;



@API
public class NoOpThrottle implements Throttle
{

    private static Throttle instance = new NoOpThrottle();

    private NoOpThrottle() { }


    public static Throttle getInstance() { return instance; }


    @Override
    public double get() { return 0; }
}
