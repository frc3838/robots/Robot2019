package frc.team3838.core.controls;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Button;



public class MultiPovButton extends Button
{
    private static final Logger logger = LoggerFactory.getLogger(MultiPovButton.class);

    private final GenericHID hid;

    private final Set<Integer> activationAngles;


    public MultiPovButton(GenericHID hid, int activationAngle)
    {
        this.hid = hid;
        this.activationAngles = ImmutableSet.of(activationAngle);
    }

    public MultiPovButton(GenericHID hid, int activationAngle, int... others)
    {
        this.hid = hid;
        final Builder<Integer> builder = ImmutableSet.builder();
        builder.add(activationAngle);
        for (int other : others)
        {
            builder.add(other);
        }

        this.activationAngles = builder.build();
    }

    public MultiPovButton(GenericHID hid, PovDirection povDirection)
    {
        this.hid = hid;
        this.activationAngles = ImmutableSet.of(povDirection.getAngle());
    }

    public MultiPovButton(GenericHID hid, PovDirection povDirection, PovDirection... others)
    {
        this.hid = hid;
        final Builder<Integer> builder = ImmutableSet.builder();
        builder.add(povDirection.getAngle());
        for (PovDirection other : others)
        {
            builder.add(other.getAngle());
        }

        this.activationAngles = builder.build();
    }


    @Override
    public boolean get()
    {
        return activationAngles.contains(hid.getPOV());
    }

}
