package frc.team3838.core.utils;

import javax.annotation.Nonnull;

import edu.wpi.first.hal.HAL;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public final class HidUtils
{

    private HidUtils() { }


    @API
    public static boolean isHidConnected(int hidPort)
    {
        return DriverStation.getInstance().getJoystickAxisType(hidPort, 0) != -1;
    }


    @API
    public static boolean isGamepad(@Nonnull GenericHID hid)
    {
        //noinspection ConstantConditions
        return hid != null && isGamepad(hid.getPort());
    }


    @API
    public static boolean isGamepad(int hidPort)
    {
        // The driver station reports both the Logitech F310 and the XBox Controller as 'XBox'
        return isHidConnected(hidPort) &&  DriverStation.getInstance().getJoystickIsXbox(hidPort);
    }

    @API
    public static String getHidName(@Nonnull GenericHID hid)
    {
        return getHidName(hid.getPort());
    }


    @API
    public static String getHidName(int hidPort)
    {
        return HAL.getJoystickName((byte) hidPort);
    }
}
