package frc.team3838.core.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.meta.API;


@API
public class PIDTuner
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(PIDTuner.class);

    private final double default_P;
    private final double default_I;
    private final double default_D;

    private final String namePrefix;


    @API
    public PIDTuner(@Nonnull String namePrefix, double initialDefault_P, double initialDefault_I, double initialDefault_D)
    {
        this.default_P = initialDefault_P;
        this.default_I = initialDefault_I;
        this.default_D = initialDefault_D;
        this.namePrefix = normalizePrefix(namePrefix);
        initSmartDashboard();
    }



    @API
    public PIDTuner(@Nonnull String namePrefix, PIDSetting initialDefaultValues)
    {
        this(namePrefix, initialDefaultValues.getP(), initialDefaultValues.getI(), initialDefaultValues.getD());
    }



    @API
    protected PIDSetting initSmartDashboard()
    {
        PIDSetting pidSetting = readSmartdashboard(-100, -100, -100);
        if (pidSetting.getP() == -100)
        {
            logger.info("Initializing '{}PIDTuner' SmartDashboard with default values P:{}, I:{}, D:{}", namePrefix, default_P, default_I, default_D);
            SmartDashboard.putNumber(namePrefix + 'P', default_P);
            SmartDashboard.putNumber(namePrefix + 'I', default_I);
            SmartDashboard.putNumber(namePrefix + 'D', default_D);
            pidSetting = readSmartdashboard(default_P, default_I, default_D);
        }
        else
        {
            SmartDashboard.putNumber(namePrefix + 'P', pidSetting.getP());
            SmartDashboard.putNumber(namePrefix + 'I', pidSetting.getI());
            SmartDashboard.putNumber(namePrefix + 'D', pidSetting.getD());
            logger.info("Initializing '{}PIDTuner' SmartDashboard with exiting values {}", namePrefix,  pidSetting);
        }

        return pidSetting;
    }


    @API
    public PIDSetting getCurrentPidSettings()
    {
        return readSmartdashboard();
    }



    protected PIDSetting readSmartdashboard(double defaultP, double defaultI, double defaultD)
    {

        final double p = SmartDashboard.getNumber(namePrefix + 'P', defaultP);
        final double i = SmartDashboard.getNumber(namePrefix + 'I', defaultI);
        final double d = SmartDashboard.getNumber(namePrefix + 'D', defaultD);
        return new PIDSetting(p, i, d);
    }


    protected PIDSetting readSmartdashboard()
    {
        return readSmartdashboard(default_P, default_I, default_D);
    }


    protected static String normalizePrefix(@Nullable String prefix)
    {
        if (prefix == null) {prefix = ""; }
        if (StringUtils.isNotBlank(prefix)) { prefix += "-"; }
        return prefix;
    }
}
