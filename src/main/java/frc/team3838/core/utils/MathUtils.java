package frc.team3838.core.utils;


import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@SuppressWarnings({"UnusedDeclaration", "IfMayBeConditional", "WeakerAccess", "SameParameterValue", "Duplicates", "BooleanMethodNameMustStartWithQuestion"})
public class MathUtils
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(MathUtils.class);


    public static final DecimalFormat DECIMAL_FORMATTER_0_PLACES = new DecimalFormat("#,##0");
    public static final DecimalFormat DECIMAL_FORMATTER_1_PLACES = new DecimalFormat("#,##0.0");
    public static final DecimalFormat DECIMAL_FORMATTER_2_PLACES = new DecimalFormat("#,##0.00");
    public static final DecimalFormat DECIMAL_FORMATTER_3_PLACES = new DecimalFormat("#,##0.000");
    public static final DecimalFormat DECIMAL_FORMATTER_4_PLACES = new DecimalFormat("#,##0.0000");
    public static final DecimalFormat DECIMAL_FORMATTER_5_PLACES = new DecimalFormat("#,##0.00000");
    public static final DecimalFormat DECIMAL_FORMATTER_6_PLACES = new DecimalFormat("#,##0.000000");


    private MathUtils() { }


    /**
     * Scales one range of numbers to another range of numbers. For example, to
     * scale a joystick axis input of {@code -1.0 to 1.0} to a PWM raw value
     * range of {@code 0 to 255} usage would be:
     * <pre>double out = scaleRange(joystick.getY(), -1, 1, 0, 255);</pre>
     * Also see various the various {@code scaleXyz(double)} methods that do common scaling conversions.
     *
     * @param inputValue     the input value to scale to new range
     * @param inputRangeMin  the minimum value of the input range
     * @param inputRangeMax  the maximum value of the input range
     * @param outputRangeMin the minimum value of the output range
     * @param outputRangeMax the maximum value of the output range
     *
     * @return the scaled value
     */
    public static double scaleRange(final double inputValue,
                                    final double inputRangeMin, final double inputRangeMax,
                                    final double outputRangeMin, final double outputRangeMax)
    {
        //http://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
        return (((outputRangeMax - outputRangeMin) * (inputValue - inputRangeMin)) / (inputRangeMax - inputRangeMin)) + outputRangeMin;
    }


    /**
     * Scales a -1 to 1 speed range to remove stall speeds. For example, if the robot does not start moving
     * until the power speed is set to 0.3, we have a stall speed of 0.3. This method will scale the
     * the speed from 0.3 to 1 and -0.3 to -1 (with a speed od 0 returning zero).
     *
     * @param speedInputValue the speed value read from the joystick or otherwise calculated.
     * @param stallSpeed      the absolute (i.e. positive) value of the stall speed
     *
     * @return the scaled speed in the range {@code stallSpeed} to 1, {@code -stallSpeed} to -1, or 0
     */
    public static double scaleP1toN1SpeedToNonStallRange(double speedInputValue, double stallSpeed)
    {
        if (speedInputValue == 0)
        {
            return 0;
        }
        final boolean isNeg = isNegative(speedInputValue);
        final double scaledSpeed = scaleRange(Math.abs(speedInputValue), 0, 1, Math.abs(stallSpeed), 1);
        return isNeg ? -scaledSpeed : scaledSpeed;
    }


    /**
     * Scales a -1 to 1 speed range to remove stall speeds, and compensate for joystick drift.
     * For example, if the robot does not start moving
     * until the power speed is set to 0.3, we have a stall speed of 0.3. This method will scale the
     * the speed from 0.3 to 1 and -0.3 to -1. If the input value is in the range of
     * {@code -joystickDriftCutOffValue} to {@code joystickDriftCutOffValue} a value of zero is returned.
     *
     * @param speedInputValue          the speed value read from the joystick or otherwise calculated.
     * @param stallSpeed               the absolute (i.e. positive) value of the stall speed'
     * @param joystickDriftCutOffValue the absolute (i.e. positive) joystick drift value.
     *
     * @return the scaled speed in the range {@code stallSpeed} to 1, {@code -stallSpeed} to -1, or 0
     */
    public static double scaleP1toN1SpeedToNonStallRange(double speedInputValue, double stallSpeed, double joystickDriftCutOffValue)
    {
        if ((Math.abs(speedInputValue) <= Math.abs(joystickDriftCutOffValue)))
        {
            return 0;
        }
        final boolean isNeg = isNegative(speedInputValue);
        final double scaledSpeed = scaleRange(Math.abs(speedInputValue), 0, 1, Math.abs(stallSpeed), 1);
        return isNeg ? -scaledSpeed : scaledSpeed;
    }


    /**
     * Scales one range of numbers to another range of numbers with the final result being an int. For example, to
     * scale a joystick axis input of {@code -1.0 to 1.0} to a PWM raw value
     * range of {@code 0 to 255} usage would be:
     * <pre>double out = scaleRange(joystick.getY(), -1, 1, 0, 255);</pre>
     * Also see various the various {@code scaleXyz(double)} methods that do common scaling conversions.
     *
     * @param inputValue     the input value to scale to new range
     * @param inputRangeMin  the minimum value of the input range
     * @param inputRangeMax  the maximum value of the input range
     * @param outputRangeMin the minimum value of the output range
     * @param outputRangeMax the maximum value of the output range
     *
     * @return the scaled value
     */
    public static int scaleRangeToInt(final double inputValue,
                                      final double inputRangeMin, final double inputRangeMax,
                                      final int outputRangeMin, final int outputRangeMax)
    {
        final double result = scaleRange(inputValue, inputRangeMin, inputRangeMax, outputRangeMin, outputRangeMax);
        final BigDecimal bigDecimal = new BigDecimal(result, MathContext.DECIMAL128).setScale(0, BigDecimal.ROUND_HALF_EVEN);
        return bigDecimal.toBigInteger().intValue();
    }


    /**
     * Scales an input value of {@code -1 to 1} to an int value of {@code 0 to 255}.
     *
     * @param value the value to scale
     *
     * @return the scaled value
     */
    public static int scaleN1TP1_to_0T255(double value)
    {
        return scaleRangeToInt(value, -1, 1, 0, 255);
    }


    /**
     * Scales an input value of {@code -1 to 1} to an int value of {@code -255 to 255}.
     *
     * @param value the value to scale
     *
     * @return the scaled value
     */
    public static int scaleN1TP1_to_N255T255(double value)
    {
        return scaleRangeToInt(value, -1, 1, -255, 255);
    }


    /**
     * Scales an input value of {@code 0 to 255} to a value of {@code -1 to 1}.
     *
     * @param value the value to scale
     *
     * @return the scaled value
     */
    public static double scale0T255_to_N1TP1(double value)
    {
        return scaleRange(value, 0, 255, -1, 1);
    }


    /**
     * Scales an input value of {@code -255 to 255} to a value of {@code -1 to 1}.
     *
     * @param value the value to scale
     *
     * @return the scaled value
     */
    public static double scaleN255T255_to_N1TP1(double value)
    {
        return scaleRange(value, -255, 255, -1, 1);
    }


    /**
     * Converts a decimal to a percentage String (including the '%' sign) with no decimal places and no rounding.
     *
     * @param value the value to convert to a percentage
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value)
    {
        return toPercentage(value, 0, false);
    }


    /**
     * Converts a decimal number to a percentage String (including the '%' sign).
     *
     * @param value     the value to convert to a percentage
     * @param precision the number of decimal places to have in the percentage value; must be between 0 and 5 inclusive
     * @param round     if true, the value will be rounded up, otherwise irt will be floored
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value, int precision, boolean round)
    {

        double roundFactor = round ? 0.5d : 0.0;
        if (precision <= 0)
        {
            return Long.toString((long) Math.floor((value * 100) + roundFactor)) + '%';
        }
        else
        {
            //technically we should make sure that the value * the shiftFactor does not exceed
            // Double.MAX_VALUE.  But that is an unlikely use case, so we favor performance over safety
            precision = Math.min(5, precision);
            double shiftFactor = Math.pow(10, precision);
            long shifted = (long) Math.floor((value * 100 * shiftFactor) + roundFactor);
            double result = shifted / (shiftFactor);
            return Double.toString(result) + '%';
        }
    }


    public static double makeNegative(double d)
    {
        return -Math.abs(d);
    }


    public static double makePositive(double d)
    {
        return Math.abs(d);
    }


    public static boolean isPositive(double d) { return d == Math.abs(d); }


    public static boolean isPositive(int i) { return i == Math.abs(i); }


    public static boolean isNegative(double d) { return d == -Math.abs(d); }


    public static boolean isNegative(int i) { return i == -Math.abs(i); }


    public static double constrainBetweenZeroAndOne(double d)
    {
        if (d > 1)
        {
            return 1;
        }
        else if (d < 0)
        {
            return 0;
        }
        else
        {
            return d;
        }
    }


    public static double constrainBetweenNegOneAndZero(double d)
    {
        if (d > 0)
        {
            return 0;
        }
        else if (d < -1)
        {
            return -1;
        }
        else
        {
            return d;
        }
    }


    public static double constrainBetweenNegOneAndOne(double d)
    {
        if (d > 1)
        {
            return 1;
        }
        else if (d < -1)
        {
            return -1;
        }
        else
        {
            return d;
        }
    }


    public static int constrainBetweenZeroAnd360(int value)
    {
        if (value > 360)
        {
            return 360;
        }
        else if (value < 0)
        {
            return 0;
        }
        else
        {
            return value;
        }
    }


    public static double constrainBetweenZeroAnd360(double value)
    {
        if (value > 360)
        {
            return 360;
        }
        else if (value < 0)
        {
            return 0;
        }
        else
        {
            return value;
        }
    }


    public static int constrainValue(int value, int min, int max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    public static double constrainValue(double value, int min, int max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    public static double constrainValue(double value, double min, double max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range of 0 to 1, retaining the sign when returned. For example, if a value
     * of -2 is received with, a value of -1 is returned.
     *
     * @param value The value to constrain
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteValueToOne(double value)
    {
        if (Math.abs(value) > 1)
        {
            return matchSign(1, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range of 0 to 1, retaining the sign when returned. For example, if a value
     * of -2 is received with, a value of -1 is returned.
     *
     * @param value The value to constrain
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValueToOne(float value)
    {
        if (Math.abs(value) > 1)
        {
            return matchSign(1, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(double value, int max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(float value, float max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(float value, int max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(double value, double max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    public static double matchSign(double value, double valueToMatch)
    {
        if (valueToMatch <= 0.0D)
        {
            return -Math.abs(value);
        }
        else
        {
            return Math.abs(value);
        }
    }


    public static double matchSign(float value, float valueToMatch)
    {
        if (valueToMatch <= 0.0D)
        {
            return -Math.abs(value);
        }
        else
        {
            return Math.abs(value);
        }
    }


    /**
     * Determines if two values have the same sign or not. If one of the values is zero, the method will always return true,
     *
     * @param first  first value
     * @param second second value
     *
     * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
     */
    public static boolean haveSameSign(double first, double second)
    {
        //noinspection OverlyComplexBooleanExpression
        return (first == 0) || (second == 0) || ((first > 0) && (second > 0)) || ((first < 0) && (second < 0));
    }


    /**
     * Determines if two values have the same sign or not. If one of the values is zero, the method will always return true,
     *
     * @param first  first value
     * @param second second value
     *
     * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
     */
    public static boolean haveSameSign(int first, int second)
    {
        //noinspection OverlyComplexBooleanExpression
        return (first == 0) || (second == 0) || ((first > 0) && (second > 0)) || ((first < 0) && (second < 0));
    }


    /**
     * Floors a double value to the nearest X percent. For example, for x = 5: <ul> <li>0.025 = 0.0</li> <li>0.144 = 0.1</li> <li>0.151 = 0.15</li> <li>0.049 =
     * 0.0</li> <li>0.394 =
     * 0.35</li> </ul>
     *
     * @param d the number to floor
     * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
     *
     * @return the floored value
     */
    public static double floorToXPercent(double d, int x)
    {
        double percent = d * 100;
        final double v = percent - (percent % x);
        return v / 100;
    }


    /**
     * Rounds a double value up or down to the nearest X percent. For example, for x = 5: <ul> <li>0.024 = 0.0</li> <li>0.025 = 0.05</li> <li>0.144 = 0.15</li>
     * <li>0.151 =
     * 0.15</li> <li>0.049 = 0.05</li> <li>0.394 = 0.4</li> </ul>
     *
     * @param d the number to round
     * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
     *
     * @return the rounded value
     */
    public static double roundToXPercent(double d, int x)
    {
        double percent = d * 100;
        int q = (int) (percent / x);
        double r = percent % x;
        int roundFactor = (r < ((double) x / 2)) ? 0 : x;
        int rounded = (q * x) + roundFactor;
        return (double) rounded / 100;
    }


    public static double roundToNPlaces(double value, int n)
    {
        final BigDecimal bigDecimal = new BigDecimal(value, MathContext.DECIMAL128).setScale(n, BigDecimal.ROUND_HALF_EVEN);
        return bigDecimal.doubleValue();

    }


    public static float roundToNPlaces(float value, int n)
    {
        final BigDecimal bigDecimal = new BigDecimal(value, MathContext.DECIMAL128).setScale(n, BigDecimal.ROUND_HALF_EVEN);
        return bigDecimal.floatValue();
    }
    
    
    /**
     * Formats a floating point number to a 2 decimal places String.
     * @param value the number to format.
     * @return a 2 decimal places String
     */
    public static String format(double value)
    {
        return DECIMAL_FORMATTER_2_PLACES.format(value);
    }
    
    
    /**
     * Formats a floating point number to a 2 decimal places String.
     *
     * @param value the number to format.
     *
     * @return a 2 decimal places String
     */
    public static String format(float value)
    {
        return DECIMAL_FORMATTER_2_PLACES.format(value);
    }
    
    
    /**
     * Formats a floating point number to a n decimal places String.
     *
     * @param value the number to format.
     * @param n the number of decimal places to format the number to, from 0 to 6.
     *
     * @return a n decimal places String
     */
    public static String formatNumber(float value, final int n)
    {
        return formatNumber((double) value, n);
    }
    
    
    /**
     * Formats a floating point number to a n decimal places String.
     *
     * @param value the number to format.
     * @param n     the number of decimal places to format the number to, from 0 to 6.
     *
     * @return a n decimal places String
     */
    public static String formatNumber(double value, final int n)
    {
        DecimalFormat formatter;
        switch (n)
        {
            case 0:
                formatter = DECIMAL_FORMATTER_0_PLACES;
                break;
            case 1:
                formatter = DECIMAL_FORMATTER_1_PLACES;
                break;
            case 2:
                formatter = DECIMAL_FORMATTER_2_PLACES;
                break;
            case 3:
                formatter = DECIMAL_FORMATTER_3_PLACES;
                break;
            case 4:
                formatter = DECIMAL_FORMATTER_4_PLACES;
                break;
            case 5:
                formatter = DECIMAL_FORMATTER_5_PLACES;
                break;
            case 6:
            default:
                formatter = DECIMAL_FORMATTER_6_PLACES;
        }
        return formatter.format(value);
    }


    public static boolean isBetween(double value, double lowerLimitInclusive, double upperLimitInclusive)
    {
        // if (angle >= 90 && angle <= 180)
        // if(10 < x < 20)
        // if(10<x && x<20)
        final boolean result = (lowerLimitInclusive <= value) && (value <= upperLimitInclusive);
        logger.debug("Checking if {} isBetween {} and {}; result = {}", value, lowerLimitInclusive, upperLimitInclusive, result);
        return result;
    }


    public static boolean isBetween(int value, int lowerLimitInclusive, int upperLimitInclusive)
    {
        return (isBetween((double) value, lowerLimitInclusive, upperLimitInclusive));
    }


    public static boolean isNear(double value, double targetValue, double deltaInclusive)
    {
        return isBetween(value, targetValue - deltaInclusive, targetValue + deltaInclusive);
    }


    public static boolean isNear(int value, int targetValue, int deltaInclusive)
    {
        return isBetween(value, targetValue - deltaInclusive, targetValue + deltaInclusive);
    }

    //TODO: Write a isNear that handles wrapping - for example:
    // angleValue = 358;
    // targetValue  = 0
    // delta = 0
    // return is "between" 355 and 5, thus 358 is true.


    public static boolean isNearWithZeroToMaxWrapping(int value, int targetValue, int deltaInclusive, int maxValueForWrapping)
    {
        int lowerLimit = targetValue - deltaInclusive;
        if (lowerLimit < 0)
        {
            lowerLimit += maxValueForWrapping;
        }

        int upperLimit = targetValue + deltaInclusive;
        if (upperLimit > maxValueForWrapping)
        {
            upperLimit -= maxValueForWrapping;
        }

        return isBetween(value, lowerLimit, upperLimit);

    }


    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args)
    {
        doIt(45);
        doIt(0);
        doIt(359);
        doIt(360);
        doIt(1);

    }


    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void doIt(int targetValue)
    {

        for (int i = 0; i < 7; i++)
        {
            int value = targetValue + i;
            boolean result = isNearWithZeroToMaxWrapping(value, targetValue, 5, 360);
            System.out.println(value + " is near  " + targetValue + " :: " + result);

            value = targetValue - i;
            result = isNearWithZeroToMaxWrapping(value, targetValue, 5, 360);
            System.out.println(value + " is near  " + targetValue + " :: " + result);

        }

    }

}
