package frc.team3838.core.config.time;


import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;



/**
 * A simple POJO-like class for use with time settings (such as timeouts).
 * It provides a convenient single object that can be used for configuring
 * times. A TimeSetting can easily be created in a number of manners, including
 * by using natural language (String) phrases such a &quot;2 hours&quot; or
 * &quot;2 hour and 15 minutes&quot;. See {@link #parse(String)} and
 * {@link #TimeSetting(String)} for more information.
 *
 * @author Mark Vedder [mark.vedder@thomsonreuters.com]
 * @since 1.0
 */
public class TimeSetting
{
    private static final List<TimeUnit> sortedTimeUnits = createSortedTimeUnitsList();
    private static final TimeUnit finest = sortedTimeUnits.get(0);

    private static final Pattern AND_REGEX = Pattern.compile("(?i),|AND|&");
    private static final Pattern SIGN_REGEX = Pattern.compile("(?i)([\\-+]?)(.*)");
    private static final Pattern SPLITTING_REGEX = Pattern.compile("(?i)[\\s]+(?=(([\\d]+)([\\s]*)([a-z_$][\\w]+)))");
    private static final Pattern SINGLE_TIME_SETTING_PARSING_REGEX = Pattern.compile("([\\d]+)([\\s]*)([a-z_$][\\w]+)",
                                                                                     Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

    protected long duration;
    protected TimeUnit timeUnit;


    protected TimeSetting() { }


    /**
     * Creates a TimeSetting from a case insensitive String representation. See the {@link #parse(String)}
     * method for more details on the formatting of the String representation.
     *
     * @param s a string representing a time setting
     *
     * @throws NullPointerException      if the supplied string is null, or
     * @throws TimeSettingParseException if the supplied string cannot be parsed
     */
    public TimeSetting(String s) throws NullPointerException, TimeSettingParseException
    {
        TimeSetting timeSetting = normalizeToCoarsestTimeUnit(parse(s));
        this.duration = timeSetting.getDuration();
        this.timeUnit = timeSetting.getTimeUnit();
    }


    /**
     * A cloning constructor that creates a TimeSetting from another TimeSetting.
     *
     * @param timeSetting the TimeSetting to clone.
     */
    public TimeSetting(@Nonnull TimeSetting timeSetting)
    {
        final TimeSetting normalizedTimeSetting = normalizeToCoarsestTimeUnit(timeSetting);
        this.duration = normalizedTimeSetting.getDuration();
        this.timeUnit = normalizedTimeSetting.getTimeUnit();
    }


    /**
     * Alternate constructor for creating a {@code TimeSetting} with a TimeUnit of {@code TimeUnit.MILLISECONDS}.
     *
     * @param milliseconds number of milliseconds in the duration;
     */
    public TimeSetting(long milliseconds)
    {
        this(milliseconds, TimeUnit.MILLISECONDS);
    }


    /**
     * Creates a {@code TimeSetting} using the specified values.
     *
     * @param duration the duration (i.e. the number of {@code TimeUnit}s) of the {@code TimeSetting}
     * @param timeUnit the {@code TimeUnit} the {@code TimeSetting}
     */
    public TimeSetting(long duration, TimeUnit timeUnit)
    {
        final TimeSetting temp = normalizeToCoarsestTimeUnit(duration, timeUnit);
        this.duration = temp.getDuration();
        this.timeUnit = temp.getTimeUnit();
    }


    protected static TimeSetting createNonNormalizedTimeSetting(long duration, @Nonnull TimeUnit timeUnit)
    {
        final TimeSetting temp = new TimeSetting();
        temp.duration = duration;
        temp.timeUnit = timeUnit;
        return temp;
    }


    /**
     * Creates a {@code TimeSetting} as a sum of other time settings. The TimeSettings will be added together and represented in the coarsest {@code TimeUnit}
     * possible.
     *
     * @param timeSettings the {@code TimeSetting}s to sum up to create the new TimeSetting
     */
    public TimeSetting(TimeSetting... timeSettings)
    {
        this(Arrays.asList(timeSettings));
    }


    /**
     * Creates a {@code TimeSetting} as a sum of other time settings. The TimeSettings will be added together and represented in the coarsest {@code TimeUnit}
     *
     * @param timeSettings the list of time settings to sum and create a TimeSetting from
     */
    public TimeSetting(List<TimeSetting> timeSettings)
    {
        TimeSetting sum = addTimeSettings(timeSettings);
        this.duration = sum.getDuration();
        this.timeUnit = sum.getTimeUnit();
    }


    /**
     * Creates a TimeSetting from a case insensitive String describing a time setting or list of time settings. The time settings can be delimited by a comma,
     * the
     * case insensitive word 'and' (optionally preceded by a comma and a space), or an ampersand (optionally preceded by a comma and a space). The name of any
     * valid {@link TimeUnit} can be used
     * either in the singular or plural format.
     *
     * Some examples:
     * <ul>
     * <li>1 Day</li>
     * <li>2 DAYS</li>
     * <li>2 DAYS, 4 hours, 1 Minute</li>
     * <li>2 days 4 hours 1 Minute</li>
     * <li>2 Days 4 hours and 1 Minute</li>
     * <li>2 Days, 4 hours, and 1 Minute</li>
     * <li>9 Days, 4 hours, & 1 Minute</li>
     * <li>5 MINUTES & 30 SECONDS</li>
     * <li>250 Milliseconds</li>
     * </ul>
     *
     * @param s the string to parse
     *
     * @return a TimeSetting with the coarsest possible TimeUnit without truncation
     *
     * @throws NullPointerException      if the supplied string is null, or
     * @throws TimeSettingParseException if the supplied string cannot be parsed
     */
    public static TimeSetting parse(String s) throws NullPointerException, TimeSettingParseException
    {
        if (s == null)
        {
            throw new NullPointerException("String representing Time Setting cannot be null");
        }

        try
        {
            final Matcher signMatcher = SIGN_REGEX.matcher(s);
            //noinspection ResultOfMethodCallIgnored
            signMatcher.matches();
            @Nullable
            final String signString = signMatcher.group(1);
            boolean isNegative = "-".equals(signString);

            String normalized = AND_REGEX.matcher(signMatcher.group(2).trim()).replaceAll(" ");
            String[] split = SPLITTING_REGEX.split(normalized);

            List<TimeSetting> timeSettings = new ArrayList<>(split.length);
            for (String timeSettingString : split)
            {
                TimeSetting ts = parseSingleTimeSettingString(timeSettingString);
                timeSettings.add(ts);
            }

            final TimeSetting baseTimeSetting = addTimeSettings(timeSettings);
            return isNegative ? new TimeSetting(-baseTimeSetting.duration, baseTimeSetting.timeUnit) : baseTimeSetting;
        }
        catch (TimeSettingParseException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new TimeSettingParseException("Could not parse the supplied time string '" + s + "' due to an exception. Cause Summary: " + e.toString(), e);
        }
    }


    @Nonnull
    private static TimeSetting parseSingleTimeSettingString(@Nonnull String s) throws TimeSettingParseException
    {
        s = s.trim();
        Matcher matcher = SINGLE_TIME_SETTING_PARSING_REGEX.matcher(s);
        if (!matcher.matches())
        {
            throw new TimeSettingParseException("Supplied time setting string does not represent a valid time setting [" + s + "]");
        }


        String durationString = matcher.group(1);
        long duration = Long.parseLong(durationString);

        TimeUnit timeUnit;
        String timeUnitString = matcher.group(3).toUpperCase();
        try
        {
            timeUnit = (TimeUnit.valueOf(timeUnitString));
        }
        catch (IllegalArgumentException e)
        {
            try
            {
                if (!timeUnitString.endsWith("S"))
                {
                    timeUnitString += "S";
                    timeUnit = TimeUnit.valueOf(timeUnitString);
                }
                else
                {
                    throw e;
                }
            }
            catch (IllegalArgumentException ignore)
            {
                throw new IllegalArgumentException("Supplied time setting string does not represent a valid time setting [" + s + ']');
            }
        }
        return new TimeSetting(duration, timeUnit);
    }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param days the number of days to convert
     *
     * @return TimeSetting representing the number of specified days
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromDays(long days) { return fromValues(days, TimeUnit.DAYS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param hours the number of hours to convert
     *
     * @return TimeSetting representing the number of specified hours
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromHours(long hours) { return fromValues(hours, TimeUnit.HOURS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param minutes the number of minutes to convert
     *
     * @return TimeSetting representing the number of specified minutes
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromMinutes(long minutes) { return fromValues(minutes, TimeUnit.MINUTES); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param seconds the number of seconds to convert
     *
     * @return TimeSetting representing the number of specified seconds
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromSeconds(long seconds) { return fromValues(seconds, TimeUnit.SECONDS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param milliseconds the number of milliseconds to convert
     *
     * @return TimeSetting representing the number of specified milliseconds
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromMills(long milliseconds) { return fromValues(milliseconds, TimeUnit.MILLISECONDS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param microseconds the number of microseconds to convert
     *
     * @return TimeSetting representing the number of specified microseconds
     *
     * @see #fromValues(long, TimeUnit)
     */
    @Nonnull
    public static TimeSetting fromMicros(long microseconds) { return fromValues(microseconds, TimeUnit.MICROSECONDS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param nanoseconds the number of nanoseconds to convert
     *
     * @return TimeSetting representing the number of specified nanoseconds
     */
    @Nonnull
    public static TimeSetting fromNanos(long nanoseconds) { return fromValues(nanoseconds, TimeUnit.NANOSECONDS); }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     * For example:
     * <ul>
     * <li>{@code fromValues(60, TimeUnit.SECONDS)} would return a {@code TimeSetting}
     * with a duration of {@code 1} and a TimeUnit of {@code TimeUnit.MINUTES}</li>
     * <li>{@code fromValues(59, TimeUnit.SECONDS)} would return a {@code TimeSetting}
     * with a duration of {@code 59} and a TimeUnit of {@code TimeUnit.SECONDS}</li>
     * </ul>
     */
    @Nonnull
    public static TimeSetting fromValues(long duration, TimeUnit timeUnit)
    {

        final TimeSetting temp = new TimeSetting();
        temp.duration = duration;
        temp.timeUnit = timeUnit;
        return normalizeToCoarsestTimeUnit(temp);
    }


    @Nonnull
    public static TimeSetting fromJavaDuration(java.time.Duration javaDuration)
    {
        return fromNanos(javaDuration.toNanos());
    }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param duration the Duration to convert
     *
     * @return TimeSetting equivalent to the Duration
     */
    @Nonnull
    public static TimeSetting fromJodaTimeDuration(org.joda.time.Duration duration)
    {
        return fromMills(duration.getMillis());
    }


    /**
     * Returns a {@code TimeSetting} with the coarsest time unit possible without a loss of value.
     *
     * @param interval the interval to convert
     *
     * @return TimeSetting equivalent to the time represented by the supplied interval
     */
    @Nonnull
    public static TimeSetting fromJodaTimeInterval(org.joda.time.Interval interval)
    {
        return fromJodaTimeDuration(interval.toDuration());
    }


    /**
     * Adds a group go time settings and normalizes/reduces the TimeUnit to the coarsest unit possible without truncation. In the event that the supplied array
     * is null or empty, a TimeSetting of 0 Milliseconds is
     * returned.
     *
     * @param timeSettings collection of time settings to add
     *
     * @return a sum of the time settings
     */
    public static TimeSetting addTimeSettings(TimeSetting... timeSettings)
    {
        return addTimeSettings(Arrays.asList(timeSettings));
    }


    /**
     * Adds a group go time settings and normalizes the TimeUnit to the coarsest unit possible without truncation. In the event that the supplied Collection is
     * null or empty, a TimeSetting of 0 Milliseconds is
     * returned.
     *
     * @param timeSettings collection of time settings to add
     *
     * @return a sum of the time settings
     */
    public static TimeSetting addTimeSettings(Collection<TimeSetting> timeSettings)
    {
        if (timeSettings == null || timeSettings.isEmpty())
        {
            return new TimeSetting(0);
        }
        else if (timeSettings.size() == 1)
        {
            TimeSetting copy = new TimeSetting(timeSettings.iterator().next());
            return normalizeToCoarsestTimeUnit(copy);
        }

        //TODO: convert to using BigInteger to allow for cases where the duration exceeds Long.MAX_VALUE when expressed in nanoseconds
        long durationTotal = 0;

        for (TimeSetting timeSetting : timeSettings)
        {

            durationTotal += finest.convert(timeSetting.getDuration(), timeSetting.getTimeUnit());
        }
        return normalizeToCoarsestTimeUnit(new TimeSetting(durationTotal, finest));
    }


    /**
     * Normalizes the supplied TimeSetting by reducing the TimeUnit of the TimeSetting to the coarsest TimeUnit without truncation.
     * For example, given a TimeSetting of 24 Hours, 1 Day is returned; given 25 Hours, 25 Hours is returned.
     *
     * @param timeSetting the TimeSetting to normalize
     */
    public static TimeSetting normalizeToCoarsestTimeUnit(TimeSetting timeSetting)
    {
        if (timeSetting == null)
        {
            return null;
        }
        final long duration = timeSetting.getDuration();
        final TimeUnit timeUnit = timeSetting.getTimeUnit();

        return normalizeToCoarsestTimeUnit(duration, timeUnit);
    }


    public static TimeSetting normalizeToCoarsestTimeUnit(long duration, TimeUnit timeUnit)
    {
        TimeSetting retVal = createNonNormalizedTimeSetting(duration, timeUnit);
        int startIndex = sortedTimeUnits.indexOf(timeUnit);
        if (startIndex != sortedTimeUnits.size() - 1)
        {

            for (int i = startIndex + 1; i < sortedTimeUnits.size(); i++)
            {
                TimeUnit nextUnit = sortedTimeUnits.get(i);

                long converted = nextUnit.convert(duration, timeUnit);
                long roundTripped = timeUnit.convert(converted, nextUnit);

                if ((roundTripped == duration))
                {
                    retVal = createNonNormalizedTimeSetting(converted, nextUnit);
                }
            }
        }
        return retVal;
    }


    /**
     * Gets the duration, which is the number of {@code TimeUnit}s. For example, if the {@code TimeSetting} is created
     * with a value of 1 SECOND, this method returns 1, not 1000. As such, it's value is 'meaningless' without
     * also getting the {@link #getTimeUnit() TimeUnit} of this Time Setting. If milliseconds is need, use the {@link #toMillis()}
     * method. Also see the {@link #toJodaTimeDuration()} method which returns this time setting as a Joda Time {@code Duration} object.
     *
     * @return the duration (or number of) {@code TimeUnit}s of this {@code TimeSetting}
     *
     * @see #toMillis()
     * @see #toJodaTimeDuration()
     */
    public long getDuration()
    {
        return duration;
    }


    /**
     * Returns a JodaTime {@code Duration} object representing the duration of this {@code TimeSetting}. Should not be confused with
     * the {@code duration} property or the {@link #getDuration()} which returns the numerical value half of the setting (with {@link #getTimeUnit()}
     * supplying the unit.)
     *
     * @return the time setting as a Joda Time {@code Duration}
     */
    public org.joda.time.Duration toJodaTimeDuration()
    {
        return new org.joda.time.Duration(toMillis());
    }


    public java.time.Duration toJavaDuration()
    {
        return Duration.ofNanos(getTimeUnit().toNanos(getDuration()));
    }


    public TimeUnit getTimeUnit()
    {
        return timeUnit;
    }


    /**
     * Returns the time setting in millisecond representation. Equivalent to calling:<br> &nbsp; &nbsp; &nbsp; &nbsp;{@code
     * timeSetting.getTimeUnit().toMillis(timeSetting.getDuration);}<br> Note that in the
     * event the configured {@code TimeUnit} is finer grained than Milliseconds (such as Nanoseconds), truncation and thus a loss of precision will occur.
     *
     * @return the time setting in milliseconds
     *
     * @see TimeUnit#convert(long, TimeUnit)
     */
    public long toMillis()
    {
        return timeUnit.toMillis(duration);
    }


    /**
     * Returns the time setting in nanosecond representation. Equivalent to calling:<br> &nbsp; &nbsp; &nbsp; &nbsp;{@code
     * timeSetting.getTimeUnit().toNanos(timeSetting.getDuration);}<br>
     *
     * @return the time setting in milliseconds
     *
     * @see TimeUnit#convert(long, TimeUnit)
     */
    public long toNanos()
    {
        return timeUnit.toNanos(duration);
    }


    private static List<TimeUnit> createSortedTimeUnitsList()
    {
        List<TimeUnit> timeUnits = Arrays.asList(TimeUnit.values());
        timeUnits.sort((a, b) ->
                       {
                           // convert 1 B to x A
                           // 1 DAY converts to 24 Hours;
                           // 1 Hour converts to 0 Hours;
                           // 1 hour converts to 1 Hour;
                           long x = b.convert(1, a);
                           //Logic from Long.compare(long, long), but we use 1 for 'y'
                           return (x < 1) ? -1 : ((x == 1) ? 0 : 1);
                       });
        return timeUnits;
    }


    @Override
    public String toString()
    {
        return String.valueOf(duration) + " " + timeUnit;
    }


    /**
     * Standard equals method. Note that two TimeSettings are equal if and only if both the duration and TimeUnit are equal. As such, 2 DAYS does not equal 48
     * HOURS.
     * For an equal method that would evaluate 2 DAYS as equal to 48 HOURS, see {@link #equalsIgnoreTimeUnit(Object)}.
     *
     * @param obj the reference object with which to compare.
     *
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     *
     * @see #equalsIgnoreTimeUnit(Object)
     * @see Object#equals(Object)
     */
    @Override
    @SuppressWarnings({"MethodWithMultipleReturnPoints", "RedundantIfStatement", "OverlyComplexMethod", "QuestionableName"})
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        TimeSetting that = (TimeSetting) obj;

        if (duration != that.duration) return false;
        if (timeUnit != that.timeUnit) return false;

        return true;
    }


    /**
     * Compares two TimeSettings for equality (or equivalence) ignoring the TimeUnits such that 2 DAYS would be equal to 48 HOURS.
     *
     * @param obj the reference object with which to compare.
     *
     * @return {@code true} if this object is represents the same period of time as the obj argument; {@code false} otherwise.
     */
    public boolean equalsIgnoreTimeUnit(Object obj)
    {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (obj instanceof TimeSetting)
        {
            @SuppressWarnings("CastToConcreteClass")
            TimeSetting that = (TimeSetting) obj;
            return this.toNanos() == that.toNanos();
        }
        else
        {
            return false;
        }
    }


    @Override
    public int hashCode()
    {
        int result = (int) (duration ^ (duration >>> 32));
        result = 31 * result + timeUnit.hashCode();
        return result;
    }


    /**
     * Standard clone method provided for object completeness. Note that a {@link #TimeSetting(TimeSetting) cloning constructor} is also provided,
     * which may be easier to use in order to avoid having to handle the CloneNotSupportedException and necessary casting.
     *
     * @return A cloned TimeSetting
     *
     * @throws CloneNotSupportedException will not be thrown
     */
    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new TimeSetting(this.getDuration(), this.getTimeUnit());
    }
}