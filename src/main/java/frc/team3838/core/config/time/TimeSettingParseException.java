package frc.team3838.core.config.time;

import javax.annotation.Nonnull;



public class TimeSettingParseException extends RuntimeException
{
    private static final long serialVersionUID = -2434943932112081705L;


    public TimeSettingParseException(@Nonnull String message)
    {
        super(message);
    }


    public TimeSettingParseException(String message, Throwable cause)
    {
        super(message, cause);
    }


    public TimeSettingParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}