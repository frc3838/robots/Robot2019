package frc.team3838.core.config;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import frc.team3838.core.logging.TextTable;
import frc.team3838.core.utils.ReflectionUtils3838;



/**
 * <p>
 * A class designed to be to be extended for each IO or port type. The {@link #validateAllPortAndChannelAssignments()}  should then
 * be called (once) on this base class, typically in the {@code Robot#robotInit()} method prior to initializing any hardware or
 * subsystems. This will run a check against all implementations/extending-classes to ensure that there are no duplicate port
 * assignments. In the event there are, a meaningful error message is printed out.
 * </p>
 * <p>
 * Using &quot;global constants&quot; contained in a single class for port/IO assignments, makes keeping track of them
 * significantly easier, reduces errors, makes making changes (due to hardware changes) easier, and prevents the use 
 * of <a href="https://en.wikipedia.org/wiki/Magic_number_(programming)">magic numbers</a> in the code base.
 * </p>
 * <p>
 * Example usage:
 * <pre>
 * public class DIOs extends AbstractIOAssignments
 * {
 *     public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_A = 0;
 *     public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_B = 1;
 *
 *     public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_A = 2;
 *     public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_B = 3;

 *     public static final int ELEVATOR_LIMIT_SWITCH = 6;
 *
 *     public static final int BALL_SENSOR = 9;
 *     
 *     &#47;** Static use only. *&#47;
 *     private DIOs(){}
 * }
 * 
 * public class Robot extends TimedRobot
 * {
 *     . . .
 *     
 *     &#64;Override
 *     public void robotInit()
 *     {
 *         . . .
 *         AbstractIOAssignments.validateAllPortAndChannelAssignments();
 *         
 *         // init hardware, controllers, joysticks, subsystems, etc.
 *         . . .
 *     }
 *     
 *     . . .
 * }
 * </pre>
 * 
 * </p>
 * <p>
 * For even more convenience, the various port/IO type classes can be static inner classes in a wrapping classes.
 * For example:
 * <pre>
 * public class RobotMap
 * {
 *     public class DIOs extends AbstractIOAssignments
 *     {
 *         public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_A = 0;
 *         public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_B = 1;
 *         public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_A = 2;
 *         public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_B = 3;
 *         public static final int ELEVATOR_LIMIT_SWITCH = 6;
 *         public static final int BALL_SENSOR = 9;
 *    
 *         &#47;** Static use only. *&#47;
 *         private DIOs(){}
 *     }
 *     
 *     public static class PWMs extends AbstractIOAssignments
 *     {
 *         public static final int ELEVATOR_MOTOR = 0;
 *         
 *         &#47;** Static use only. *&#47;
 *         private PWMs(){}
 *     }
 *     
 *     public static class CANs extends AbstractIOAssignments
 *     {
 *          public static final int DRIVE_LEFT_SIDE_REAR = 1;  
 *          public static final int DRIVE_LEFT_SIDE_FRONT = 2;
 *          public static final int DRIVE_RIGHT_SIDE_REAR = 3; 
 *          public static final int DRIVE_RIGHT_SIDE_FRONT = 4;
 *          public static final int BALL_COLLECTOR_MOTOR = 5;
 *          
 *          &#64;SuppressWarnings("unused")
 *          public static final int PDP = 9;
 *          &#64;SuppressWarnings("unused")
 *          public static final int PCM = 10;
 *          
 *          &#47;** Static use only. *&#47;
 *          private CANs(){}
 *     }
 *     
 *     &#47;** Static use only. *&#47;
 *     private RobotMap(){}
 * }
 * </pre> 
 * Code then subsequently refer to the desired port/IO as such:
 * <pre>
 * SpeedController ballMotor = new WPI_TalonSRX(RobotMap.CANs.BALL_COLLECTOR_MOTOR);    
 * </pre>
 * If desired, the <tt>CANs</tt> class could be statically imported:
 * <pre>
 * import frc.team3838.game.RobotMap.CANs;
 * . . .
 * SpeedController ballMotor = new WPI_TalonSRX(CANs.BALL_COLLECTOR_MOTOR); 
 * </pre>
 * or you can static import all CAN assignments:
 * <pre>
 * import static frc.team3838.game.RobotMap.CANs.*;
 * . . . 
 * SpeedController ballMotor = new WPI_TalonSRX(BALL_COLLECTOR_MOTOR);
 * </pre>
 * It should be remembered that static imports is an intermediate programming concept, and my not be
 * familiar to novice developers. And thus their use should be avoided in code to be developed by
 * less experienced students/developers.
 * </p>
 */
@SuppressWarnings("AbstractClassWithoutAbstractMethods")
public abstract class AbstractIOAssignments // TODO: lets rename this to something like PortsAndChannelAssignmentsBase
{
    /** Static logger for use in this Abstract class. */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractIOAssignments.class);

    /** Logger for use in this class in non-static contexts and by implementing classes. */
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private static AtomicBoolean initializationCompleted = new AtomicBoolean(false);
    
    /*
        Although we do the validation check in the static initializer block, in most cases it will NOT be called unless
        the validateAllPortAndChannelAssignments() method is called. It is NOT called when a public static final field
        (such as a port assigment) is accessed. This is because the static initializer block is executed the first time
        the class is loaded by a ClassLoader. That only happens when one of the following occurs:
            • A static member variable is SET by the application ("by the application" means code outside the class containing the static block).
            • A non-final static member variable is accessed by the application. (in general, we make our assignments final)
            • A static method is called by the application.
            • Class.forName(“..”) is used on the class
            • Creating an instance using Class.newInstance() or through the new keyword.
            • An instance of the class is created/constructed.
       But it does not happen with a public static FINAL field is accessed
       See Java Specification:
            § 8.7. Static Initializers    https://docs.oracle.com/javase/specs/jls/se11/html/jls-8.html#jls-8.7
            § 12.4. Initialization of Classes and Interfaces    https://docs.oracle.com/javase/specs/jls/se11/html/jls-12.html#jls-12.4
     */   
    static 
    {
        if (!initializationCompleted.getAndSet(true))
        {
            final SortedMap<String, TextTable> dupMap = new TreeMap<>();
            try
            {
                final Set<Class<? extends AbstractIOAssignments>> instances = ReflectionUtils3838.findImplementations(AbstractIOAssignments.class);
    
                for (Class<? extends AbstractIOAssignments> clazz : instances)
                {
                    final Multimap<Integer, String> duplicateCheckMultiMap = ArrayListMultimap.create();
                    final String className = clazz.getSimpleName();
                    final String name = className.endsWith("s") ? className.substring(0, className.length() - 1) : className;
                    LOG.info("Checking {} assignments for duplicates", name);


                    // 1) Put all int and long fields for this class into the multimap
                    final Field[] fields = clazz.getFields();
                    for (Field field : fields)
                    {
                        final String fieldName = field.getName();

                        if (int.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(fieldName))
                        {
                            final int port = field.getInt(null);
                            duplicateCheckMultiMap.put(port, fieldName);
                        }
                        else if (long.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(fieldName))
                        {
                            final long port = field.getLong(null);
                            duplicateCheckMultiMap.put((int) port, fieldName);
                        }
                    }

                    // 2) Check for any duplicates...
                    List<Integer> ports = new ArrayList<>(duplicateCheckMultiMap.keySet());
                    Collections.sort(ports);

                    boolean hasDuplicate = false;
                    for (Integer port : ports)
                    {
                        if (duplicateCheckMultiMap.get(port).size() != 1)
                        {
                            hasDuplicate = true;
                            break; // from ports loop;
                        }
                    }
                    
                    if (hasDuplicate)
                    {
                        final List<String> headers = ImmutableList.of("#", "Name", "Duplicate?");
                        List<List<String>> data = new ArrayList<>();
                        
                        for (Integer port : ports)
                        {
                            final Collection<String> assignments = duplicateCheckMultiMap.get(port);
                            String dupString = (assignments.size() == 1) ? "" : "*DUPLICATE*";
                            for (String assignment : assignments)
                            {
                                data.add(ImmutableList.of(port.toString(), assignment, dupString));
                            }
                        }
                        final TextTable table = TextTable.Companion.of(headers, data);
                        dupMap.putIfAbsent(className, table);
                    }
                }

                // If we found any duplicates for any of the assignment subclasses, it is a fatal condition, so we throw an error
                if (!dupMap.isEmpty())
                {
                    final StringBuilder msg = new StringBuilder();
                    final Set<String> keySet = dupMap.keySet();
                    msg.append(">>>>>FATAL ERROR: Duplicate port/channel assignments found in: ").append(keySet).append("<<<<<").append(System.lineSeparator());
                    for (Entry<String, TextTable> entry : dupMap.entrySet())
                    {
                        msg.append(entry.getKey()).append(System.lineSeparator());
                        for (String line : entry.getValue().toLines())
                        {
                            msg.append(line).append(System.lineSeparator());
                        }
                    }
                    msg.append(">>>>>FATAL ERROR: Duplicate port/channel assignments found in: ").append(keySet).append("<<<<<").append(System.lineSeparator());
    
    
    
    
                    LOG.error(">>>>>FATAL ERROR<<<<<");
                    LOG.error("\n{}", msg);
                    LOG.error(">>>>>FATAL ERROR<<<<<");
                    LOG.error("");
                    // Give the logging framework time to flush
                    try {TimeUnit.MILLISECONDS.sleep(250);} catch (InterruptedException ignore) {}
                    throw new ExceptionInInitializerError("Duplicate port/channel assignments were found.\n" + msg);
                }
            }
            catch (Exception e)
            {
                final String msg = String.format("Could not validate port/channel assignments due to an Exception. Cause Details: %s", e.toString());
                LOG.warn(msg, e);
                throw new RuntimeException(msg, e);
            }
        }
    }
    
    /**
     * <p>
     * An initialization method that should be called early in the Robot's initialization sequence, such as early in the
     * Robot#robotInit() method. This will check all implementations, verify that none have any duplicate ports assigned. 
     * This method can/should be called just on this Abstract Class: {@code AbstractIOAssignments.validateAllPortAndChannelAssignments();} 
     * It will programmatically -- via reflection -- find all extending classes/implementations and check them for duplicates. 
     * It is safe to call this method multiple times, as it is guarded to ensure that the the validation for duplicates only occurs once.
     * <p>
     * <p>
     * Failure to call this method will result no check for duplicates (in almost all cases).     
     * </p>    
     */
    public static void validateAllPortAndChannelAssignments()
    {
        /* a no op convenience method to call in order to cause the static initializer to be invoked, and thus running the duplication checks */
    }
}
