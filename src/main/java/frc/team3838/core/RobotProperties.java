package frc.team3838.core;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;



public class RobotProperties
{
    private static final Logger logger = LoggerFactory.getLogger(RobotProperties.class);


    public  static final String UNDETERMINED_ROBOT_NAME = "<undetermined robot name>";
    public static final String NAVX_ADJUSTMENT_ANGLE_PROP_KEY = "navxAdjustmentAngle";
    public static final double DEFAULT_NAVX_ADJUSTMENT_ANGLE = -0.825;
    public static final String DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING = Double.toString(DEFAULT_NAVX_ADJUSTMENT_ANGLE);
    public static final String ENCODER_DISTANCE_PER_PULSE_KEY = "encoderDistancePerPulse";
    public static final double DEFAULT_ENCODER_DISTANCE_PER_PULSE = 0.054917;
    public static final String DEFAULT_ENCODER_DISTANCE_PER_PULSE_STRING = Double.toString(DEFAULT_ENCODER_DISTANCE_PER_PULSE);

    public static final String DRIVE_STRAIGHT_P_PROP_KEY = "driveStraightP";
    // original value was 0.027
    public static final double DEFAULT_P = 0.175;

    // W x H
    //   4:3 ==>   160x120   256x192   320x240
    public static final String CAMERA_ResolutionWidthSetting = "cameraResolutionWidthSetting";
    public static final int CAMERA_ResolutionWidthSetting_DEFAULT = 160;
    public static final String CAMERA_ResolutionHeightSetting = "cameraResolutionHeightSetting";
    public static final int CAMERA_ResolutionHeightSetting_DEFAULT = 120;
    public static final String CAMERA_FpsSetting = "cameraFpsSetting";
    public static final int CAMERA_FpsSetting_DEFAULT = 15;

    private static Properties robotProperties;


    private RobotProperties() {}


    public static void initProperties()
    {
        robotProperties =loadRobotProperties();
    }

    public static double getNavxAdjustmentAngle()
    {
        try
        {
            final String adjAngleString = getRobotProperties().getProperty(NAVX_ADJUSTMENT_ANGLE_PROP_KEY, DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING);
            final Double adjAngle = Double.valueOf(adjAngleString);
            logger.info("NavX Adjustment Angle of {} read in from properties file.", adjAngle);
            return adjAngle;
        }
        catch (Exception e)
        {
            logger.warn("Could not read NavX Adjustment Angle from robot properties. Using default angle of {}", DEFAULT_NAVX_ADJUSTMENT_ANGLE);
            return DEFAULT_NAVX_ADJUSTMENT_ANGLE;
        }
    }


    @API
    public static Properties getRobotProperties()
    {
        return robotProperties;
    }

    public static String getRobotName()
    {
        return robotProperties.getProperty(getRobotNamePropertiesKey(), UNDETERMINED_ROBOT_NAME);
    }

    private static String getRobotNamePropertiesKey()
    {
        return "robotName";
    }



    public static double getEncoderDistancePerPulse()
    {
        // Note 02/21/2017 - Bag & Tag night -  Robot 2 is dead on when running the drive straight 140"
        //                                      Robot 1 is off a little (~1 inch). So we still need to calibrate the encoder distance per pulse value a little

        try
        {
            final String valueString = robotProperties.getProperty(ENCODER_DISTANCE_PER_PULSE_KEY, DEFAULT_ENCODER_DISTANCE_PER_PULSE_STRING);
            final Double value = Double.valueOf(valueString);
            logger.info("Encoder Distance per Pulse setting of {} read in from properties file.", value);
            return value;
        }
        catch (Exception e)
        {
            logger.warn("Could not read Encoder Distance per Pulse setting from robot properties. Using default value of {}", DEFAULT_ENCODER_DISTANCE_PER_PULSE);
            return DEFAULT_ENCODER_DISTANCE_PER_PULSE;
        }
    }


    public static Properties getDefaultProperties()
    {
        Properties properties = new Properties();
        properties.setProperty(getRobotNamePropertiesKey(), UNDETERMINED_ROBOT_NAME);
        properties.setProperty(NAVX_ADJUSTMENT_ANGLE_PROP_KEY, DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING);
        return properties;
    }


    public static String getRobotPropertiesFileName()
    {
        return "Robot3838.properties";
    }

    @Nonnull
    private static Properties loadRobotProperties()
    {
        /*
        user.home = '/home/lvuser'
        user.dir  = '/'
        user.name = 'lvuser'
        /home/lvuser/Team3838/Robot3838.properties
        */
        final Properties properties = new Properties(getDefaultProperties());
        try
        {


            final Path nameFile = Paths.get(System.getProperty("user.home")).resolve("Team3838").resolve(getRobotPropertiesFileName());
            try
            {
                if (Files.isReadable(nameFile))
                {
                    try (InputStream inputStream = Files.newInputStream(nameFile))
                    {
                        properties.load(inputStream);
                    }
                }
                else
                {
                    logger.warn("Robot properties file does not exist at '{}'.", nameFile);
                }
            }
            catch (Exception e)
            {
                logger.warn("Could not read Robot name file from '{}'. Cause Summary: {}", nameFile, e.toString());
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not read Robot name file. Cause Summary: {}", e.toString());
        }

        return properties;
    }



    public static Double getDriveStraightP()
    {
        try
        {
            final String valueString = robotProperties.getProperty(DRIVE_STRAIGHT_P_PROP_KEY, Double.toString(DEFAULT_P));
            final double value = Double.valueOf(valueString);
            logger.info("DriveStraightP setting of {} read in from properties file.", MathUtils.formatNumber(value, 5));
            return value;
        }
        catch (Exception e)
        {
            logger.warn("Could not load DriveStraightP value from properties file due to an exception: {}", e.toString(), e);
            return DEFAULT_P;
        }
    }
}
